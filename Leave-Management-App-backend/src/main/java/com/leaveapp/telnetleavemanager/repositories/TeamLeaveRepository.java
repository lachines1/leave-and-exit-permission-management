package com.leaveapp.telnetleavemanager.repositories;

import com.leaveapp.telnetleavemanager.entities.Status;
import com.leaveapp.telnetleavemanager.entities.Team;
import com.leaveapp.telnetleavemanager.entities.TeamLeave;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TeamLeaveRepository extends JpaRepository<TeamLeave,Long> {
    List<TeamLeave> findByTeamId(Long teamId);

    List<TeamLeave> findByTeamAndStatus(Team team, Status status);
}
