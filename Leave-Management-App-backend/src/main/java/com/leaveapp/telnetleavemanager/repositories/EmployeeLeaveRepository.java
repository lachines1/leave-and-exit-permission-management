package com.leaveapp.telnetleavemanager.repositories;

import com.leaveapp.telnetleavemanager.entities.EmployeeLeave;
import com.leaveapp.telnetleavemanager.entities.Status;
import com.leaveapp.telnetleavemanager.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeLeaveRepository extends JpaRepository<EmployeeLeave, Long> {
    List<EmployeeLeave> findAllByUser_Id(Integer id);

    List<EmployeeLeave> findByUserAndStatus(User userRequestingLeave, Status status);
}
