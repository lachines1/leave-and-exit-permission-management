package com.leaveapp.telnetleavemanager.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ExternalActivitiesStatisticsDto {

        private int totalExternalActivities;
        private int usedExternalActivities;
        private int remainingExternalActivities;
}
