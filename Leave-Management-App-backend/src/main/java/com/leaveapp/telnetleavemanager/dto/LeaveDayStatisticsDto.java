package com.leaveapp.telnetleavemanager.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class LeaveDayStatisticsDto {

        private float totalLeaveDays;
        private float usedLeaveDays;
        private float remainingLeaveDays;
}
