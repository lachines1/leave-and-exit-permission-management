package com.leaveapp.telnetleavemanager.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class LeaveStatistics {

    private int totalEmployees;
    private int totalEmployeesOnLeave;

}
