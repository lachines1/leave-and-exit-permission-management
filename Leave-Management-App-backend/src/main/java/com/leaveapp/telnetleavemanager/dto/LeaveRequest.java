package com.leaveapp.telnetleavemanager.dto;

import com.leaveapp.telnetleavemanager.entities.ExceptionalLeaveType;
import com.leaveapp.telnetleavemanager.entities.LeaveType;
import com.leaveapp.telnetleavemanager.entities.TimeOfDay;
import jakarta.annotation.Nullable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LeaveRequest {

    private String userEmail;
    private LeaveType leaveType;
    @Builder.Default
    private ExceptionalLeaveType exceptionalLeaveType = ExceptionalLeaveType.NONE;
    @Builder.Default
    private TimeOfDay timeOfDay = TimeOfDay.INAPPLICABLE;
    @Builder.Default
    private LocalDateTime startDate = LocalDateTime.now();
    @Nullable
    @Builder.Default
    private LocalDateTime endDate = LocalDateTime.now();
    private String reason;
}
