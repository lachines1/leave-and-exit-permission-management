package com.leaveapp.telnetleavemanager.services;

import com.leaveapp.telnetleavemanager.entities.*;
import com.leaveapp.telnetleavemanager.exceptions.InsufficientLeaveBalanceException;
import com.leaveapp.telnetleavemanager.exceptions.UnauthorizedActionException;
import com.leaveapp.telnetleavemanager.repositories.EmployeeLeaveRepository;
import com.leaveapp.telnetleavemanager.dto.LeaveRequest;
import com.leaveapp.telnetleavemanager.user.Role;
import com.leaveapp.telnetleavemanager.user.User;
import com.leaveapp.telnetleavemanager.user.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
@RequiredArgsConstructor
public class EmployeeLeaveService {

    private final EmployeeLeaveRepository employeeLeaveRepository;
    private final UserRepository userRepository;
    private final MailingService mailingService;

    public void createLeaveRequest(String currentUserEmail,LeaveRequest leaveRequest) {
        validateLeaveRequest(leaveRequest);
        User currentUser = userRepository.findByEmail(currentUserEmail)
                .orElseThrow(() -> new RuntimeException("User not found"));
        User user = userRepository.findByEmail(leaveRequest.getUserEmail())
                .orElseThrow(() -> new RuntimeException("User not found"));

        EmployeeLeave leave = EmployeeLeave.builder()
                .user(user)
                .leaveType(leaveRequest.getLeaveType())
                .exceptionalLeaveType(leaveRequest.getExceptionalLeaveType())
                .startDate(leaveRequest.getStartDate())
                .status(Status.PENDING)
                .reason(leaveRequest.getReason())
                .createdAt(LocalDateTime.now())
                .timeOfDay(leaveRequest.getTimeOfDay())
                .build();

        if (leaveRequest.getLeaveType() == LeaveType.PERSONAL_LEAVE || leaveRequest.getLeaveType() == LeaveType.SICK_LEAVE) {
            leave.setExceptionalLeaveType(ExceptionalLeaveType.NONE);
            if (leaveRequest.getLeaveType() == LeaveType.PERSONAL_LEAVE && user.getLeaveDays() <= 0 || leaveRequest.getLeaveType() == LeaveType.HALF_DAY && user.getLeaveDays() <= 0) {
                throw new InsufficientLeaveBalanceException("You don't have enough leave days");
            }
            if (leaveRequest.getLeaveType() == LeaveType.SICK_LEAVE && currentUser.getRole()!= Role.ADMIN) {
                throw new UnauthorizedActionException("You are not authorized to create sick leave requests");
            }
            // For personal leave, set the end date directly from the DTO
            leave.setEndDate(leaveRequest.getEndDate());
        } else {
            // Calculate duration based on the leave type and set the end date
            calculateDurationAndSetEndDate(leave);
        }

        if (leaveRequest.getLeaveType() == LeaveType.PERSONAL_LEAVE && Duration.between(leave.getStartDate(), leave.getEndDate()).toDays() > user.getLeaveDays()) {
            throw new InsufficientLeaveBalanceException("You don't have enough leave days");
        }

        employeeLeaveRepository.save(leave);
        this.mailingService.sendMail(user.getEmail(),"Leave request created", "Your leave request has been created");
        this.mailingService.sendMail(currentUser.getEmail(),"Leave request created", "Leave request for " + user.getEmail() + " has been created");
        if (user.getTeam() != null && user.getTeam().getManager() != null) {
            this.mailingService.sendMail(user.getTeam().getManager().getEmail(),"Leave request created", "Leave request for " + user.getEmail() + " has been created");
        }
    }

    private void calculateDurationAndSetEndDate(EmployeeLeave leave) {
        LeaveType leaveType = leave.getLeaveType();

        if (leaveType == LeaveType.EXCEPTIONAL_LEAVE) {
            ExceptionalLeaveType exceptionalLeaveType = leave.getExceptionalLeaveType();
            int duration = exceptionalLeaveType.getDuration();
            leave.setEndDate(leave.getStartDate().plusDays(duration));
        } else if (leaveType == LeaveType.HALF_DAY) {
            leave.setEndDate(leave.getStartDate().plusDays(1));
        }
    }

    public EmployeeLeave treatLeaveRequest(String currentUserEmail, Long leaveRequestId, String status) {

        EmployeeLeave leaveRequest = employeeLeaveRepository.findById(leaveRequestId)
                .orElseThrow(() -> new RuntimeException("Leave Request not Found"));

        User currentUser = userRepository.findByEmail(currentUserEmail)
                .orElseThrow(() -> new RuntimeException("Current user not found"));

        User userRequestingLeave = leaveRequest.getUser();

        if ( currentUser.getRole() == Role.USER ) {
            throw new IllegalStateException("You are not authorized to treat this leave request.");
        }

        if (leaveRequest.getLeaveType() == LeaveType.PERSONAL_LEAVE && userRequestingLeave.getLeaveDays() < Duration.between(leaveRequest.getStartDate(), leaveRequest.getEndDate()).toDays()) {
            leaveRequest.setStatus(Status.REJECTED);
            throw new IllegalStateException("User does not have enough leave balance.");
        }

        // Consider additional conditions or business rules for setting the status
        if ("ACCEPTED".equalsIgnoreCase(status)) {
            List<EmployeeLeave> pendingLeaveRequests = employeeLeaveRepository
                    .findByUserAndStatus(userRequestingLeave, Status.PENDING);

            for (EmployeeLeave pendingLeave : pendingLeaveRequests) {
                if (!pendingLeave.getId().equals(leaveRequestId)) {
                    // Erase or update status for other pending leave requests
                    // Here, assuming you want to erase them, you can use your own logic
                    employeeLeaveRepository.delete(pendingLeave);
                }
            }
            leaveRequest.setStatus(Status.ACCEPTED);
            if (leaveRequest.getStartDate().isBefore(LocalDateTime.now())) {
                userRequestingLeave.setOnLeave(true);
            }
            userRequestingLeave.setReturnDate(leaveRequest.getEndDate());
            userRepository.save(userRequestingLeave);
            updateLeaveBalance(leaveRequest, userRequestingLeave);
        } else if ("REJECTED".equalsIgnoreCase(status)) {
            leaveRequest.setStatus(Status.REJECTED);
            // You may want to handle additional actions for a rejected leave request
        } else {
            throw new IllegalArgumentException("Invalid status provided.");
        }
        this.mailingService.sendMail(userRequestingLeave.getEmail(),"Leave request treated", "Your leave request with id: " + leaveRequestId + " has been " + status);
        if(userRequestingLeave.getRole() == Role.MANAGER) {
            if (userRequestingLeave.getTeam() != null && userRequestingLeave.getTeam().getManager() != null) {
                this.mailingService.sendMail(userRequestingLeave.getTeam().getManager().getEmail(), "Leave request treated", "Leave request with id: " + leaveRequestId + " has been " + status);
            }
        }
        if (userRequestingLeave.getRole() == Role.USER){
            if (userRequestingLeave.getTeam() != null && userRequestingLeave.getTeam().getManager() != null) {
                this.mailingService.sendMail(userRequestingLeave.getTeam().getManager().getEmail(), "Leave request treated", "Leave request with id: " + leaveRequestId + " has been " + status);
            }
        }
        return employeeLeaveRepository.save(leaveRequest);
    }

    private void updateLeaveBalance(EmployeeLeave leaveRequest, User userRequestingLeave) {

        double currentBalance = userRequestingLeave.getLeaveDays();
        double requestedDays = Duration.between(leaveRequest.getStartDate(), leaveRequest.getEndDate()).toDays();
        if(leaveRequest.getLeaveType() == LeaveType.HALF_DAY) requestedDays = 0.5f;
        if (leaveRequest.getLeaveType() == LeaveType.PERSONAL_LEAVE) {
            userRequestingLeave.setLeaveDays(currentBalance - requestedDays);
        }
        userRepository.saveAndFlush(userRequestingLeave);
    }

    public void deleteLeaveRequestEmployee(Long id) {
        EmployeeLeave currentLeaveRequest = employeeLeaveRepository.findById(id)
                .orElseThrow(()-> new RuntimeException("Leave request not found!"));
        if (currentLeaveRequest.getStatus() != Status.PENDING) {
            throw new UnauthorizedActionException("This Leave Request has already been processed!");
        }
        employeeLeaveRepository.delete(currentLeaveRequest);
    }

    public EmployeeLeave updateLeaveRequestEmployee(String currentUserEmail, Long leaveRequestId, LeaveRequest leaveRequest) {

        User currentUser = userRepository.findByEmail(currentUserEmail)
                .orElseThrow(() -> new RuntimeException("Current user not found"));
        EmployeeLeave existingLeaveRequest = employeeLeaveRepository.findById(leaveRequestId)
                .orElseThrow(() -> new IllegalArgumentException("Leave request not found"));
        if (existingLeaveRequest.getStatus() != Status.PENDING || currentUser!=existingLeaveRequest.getUser()) {
            throw new UnauthorizedActionException("Unauthorized action");
        }
        return updateFields(leaveRequest, existingLeaveRequest);
    }

    private EmployeeLeave updateFields(LeaveRequest leaveRequest, EmployeeLeave existingLeaveRequest) {
        existingLeaveRequest.setReason(leaveRequest.getReason() == null ? existingLeaveRequest.getReason() : leaveRequest.getReason());
        existingLeaveRequest.setLeaveType(leaveRequest.getLeaveType() == null ? existingLeaveRequest.getLeaveType() : leaveRequest.getLeaveType());
        existingLeaveRequest.setExceptionalLeaveType(leaveRequest.getExceptionalLeaveType() == null ? existingLeaveRequest.getExceptionalLeaveType() : leaveRequest.getExceptionalLeaveType());
        existingLeaveRequest.setLeaveType(leaveRequest.getLeaveType() == null ? existingLeaveRequest.getLeaveType() : leaveRequest.getLeaveType());
        existingLeaveRequest.setStartDate(leaveRequest.getStartDate() == null ? existingLeaveRequest.getStartDate() : leaveRequest.getStartDate());
        existingLeaveRequest.setEndDate(leaveRequest.getEndDate() == null ? existingLeaveRequest.getEndDate() : leaveRequest.getEndDate());
        this.mailingService.sendMail(existingLeaveRequest.getUser().getEmail(),"Leave request updated", "Your leave request has been updated");
        return employeeLeaveRepository.save(existingLeaveRequest);
    }

    public EmployeeLeave updateLeaveRequest(Long leaveRequestId, LeaveRequest leaveRequest) {
        EmployeeLeave existingLeaveRequest = employeeLeaveRepository.findById(leaveRequestId)
                .orElseThrow(() -> new IllegalArgumentException("Leave request not found"));
        if (existingLeaveRequest.getStatus() != Status.PENDING) {
            throw new UnauthorizedActionException("The Leave Request has already been processed");
        }
        return updateFields(leaveRequest, existingLeaveRequest);
    }

    public void deleteLeaveRequest(Integer leaveRequestId) {
        employeeLeaveRepository.deleteById(leaveRequestId.longValue());
        try {
            String email = employeeLeaveRepository.findById(leaveRequestId.longValue()).orElseThrow(()-> new RuntimeException("User not found")).getUser().getEmail();
            this.mailingService.sendMail(email,"Leave request deleted", "Your leave request has been deleted");
        } catch (Exception e) {
            log.error("Error sending email: " + e.getMessage());
        }
    }

    public EmployeeLeave getLeaveRequestById(Long leaveRequestId) {
        return employeeLeaveRepository.findById(leaveRequestId).orElse(null);
    }

    public List<EmployeeLeave> getAllLeaveRequests() {
        return employeeLeaveRepository.findAll();
    }

    public List<EmployeeLeave> getLeaveRequestsByUserId(Integer userId) {
        return employeeLeaveRepository.findAllByUser_Id(userId);
    }


    private void validateLeaveRequest(LeaveRequest leaveRequest) {
        if (leaveRequest.getUserEmail() == null) {
            throw new IllegalArgumentException("User id is required");
        }
        if (leaveRequest.getLeaveType() == null) {
            throw new IllegalArgumentException("Leave type is required");
        }
        if (leaveRequest.getStartDate() == null) {
            throw new IllegalArgumentException("Start date is required");
        }
        if (leaveRequest.getEndDate() == null) {
            throw new IllegalArgumentException("End date is required");
        }
    }

    public List<EmployeeLeave> getLeaveRequestsByManager(String managerEmail) {
        List<EmployeeLeave> leaveRequests = new ArrayList<>();
        User manager = userRepository.findByEmail(managerEmail)
                .orElseThrow(() -> new RuntimeException("User not found"));
        Team team = manager.getTeam();
        for (User user : team.getMembers()) {
            leaveRequests.addAll(employeeLeaveRepository.findAllByUser_Id(user.getId()));
        }
        return leaveRequests;
    }

    public List<Integer> countLeaveRequestsByStatus() {
        List<Integer> countMap = new ArrayList<>();
        // Get all leave requests
        List<EmployeeLeave> allLeaveRequests = employeeLeaveRepository.findAll();
        // Count leave requests by status
        long pendingCount = allLeaveRequests.stream()
                .filter(leaveRequest -> leaveRequest.getStatus() == Status.PENDING)
                .count();
        long acceptedCount = allLeaveRequests.stream()
                .filter(leaveRequest -> leaveRequest.getStatus() == Status.ACCEPTED)
                .count();
        long rejectedCount = allLeaveRequests.stream()
                .filter(leaveRequest -> leaveRequest.getStatus() == Status.REJECTED)
                .count();
        // Populate the count map
        countMap.add((int) pendingCount);
        countMap.add((int) acceptedCount);
        countMap.add((int) rejectedCount);

        return countMap;
    }

    public List<Integer> countLeaveRequestsByType() {
        List<Integer> countMap = new ArrayList<>();

        // Get all leave requests
        List<EmployeeLeave> allLeaveRequests = employeeLeaveRepository.findAll();

        // Count leave requests by status
        long personalLeaveCount = allLeaveRequests.stream()
                .filter(leaveRequest -> leaveRequest.getLeaveType() == LeaveType.PERSONAL_LEAVE)
                .count();
        long exceptionalLeaveCount = allLeaveRequests.stream()
                .filter(leaveRequest -> leaveRequest.getLeaveType() == LeaveType.EXCEPTIONAL_LEAVE)
                .count();
        long sickLeaveCount = allLeaveRequests.stream()
                .filter(leaveRequest -> leaveRequest.getLeaveType() == LeaveType.SICK_LEAVE)
                .count();
        long halfDayCount = allLeaveRequests.stream()
                .filter(leaveRequest -> leaveRequest.getLeaveType() == LeaveType.HALF_DAY)
                .count();

        // Populate the count map
        countMap.add((int) personalLeaveCount);
        countMap.add((int) exceptionalLeaveCount);
        countMap.add((int) sickLeaveCount);
        countMap.add((int) halfDayCount);

        return countMap;
    }
    public List<Integer> getLeaveCountsByMonth() {
        List<EmployeeLeave> allLeaveRequests = employeeLeaveRepository.findAll();
        List<Integer> leaveCountsByMonth = new ArrayList<>(12);
        for (int i = 0; i < 12; i++) {
            leaveCountsByMonth.add(0);
        }

        for (EmployeeLeave leave : allLeaveRequests) {
            Month month = leave.getStartDate().getMonth();
            int index = month.getValue() - 1;
            leaveCountsByMonth.set(index, leaveCountsByMonth.get(index) + 1);
        }

        return leaveCountsByMonth;
    }


    public Map<String, Map<String, Double>> countPermissionRequestsByType() {
        List<EmployeeLeave> allLeaveRequests = employeeLeaveRepository.findAll();
        Map<String, Map<String, Double>> permissionRequestData = new HashMap<>();

        // Initialiser les compteurs pour chaque type de demande de permission
        Map<String, Double> permissionCounts = new HashMap<>();
        for (ExceptionalLeaveType type : ExceptionalLeaveType.values()) {
            permissionCounts.put(type.toString(), 0.0);
        }

        // Compter les demandes de permission par type
        for (EmployeeLeave leave : allLeaveRequests) {
            ExceptionalLeaveType leaveType = leave.getExceptionalLeaveType();
            if (leaveType != null) {
                String permissionType = leaveType.toString();
                permissionCounts.put(permissionType, permissionCounts.getOrDefault(permissionType, 0.0) + 1);
            }
        }

        // Calculer les pourcentages pour chaque type de demande de permission
        int totalPermissionRequests = allLeaveRequests.size();
        Map<String, Double> permissionPercentages = new HashMap<>();
        for (Map.Entry<String, Double> entry : permissionCounts.entrySet()) {
            String permissionType = entry.getKey();
            Double count = entry.getValue();
            Double percentage = (count / totalPermissionRequests) * 100;
            permissionPercentages.put(permissionType, percentage);
        }

        // Ajouter les pourcentages au résultat final
        permissionRequestData.put("permission_request", permissionPercentages);

        return permissionRequestData;
    }

    @Scheduled(fixedDelay = 1000 * 60 * 60 )
    private void updateOnLeaveStatus() {
        List<EmployeeLeave> leaveRequests = employeeLeaveRepository.findAll();
        for (EmployeeLeave leaveRequest : leaveRequests) {
            if (leaveRequest.getEndDate().isBefore(LocalDateTime.now())) {
                leaveRequest.getUser().setOnLeave(false);
                userRepository.saveAndFlush(leaveRequest.getUser());
            }
            if (leaveRequest.getStartDate().isBefore(LocalDateTime.now()) && leaveRequest.getEndDate().isAfter(LocalDateTime.now()) && leaveRequest.getStatus() == Status.ACCEPTED) {
                leaveRequest.getUser().setOnLeave(true);
                userRepository.saveAndFlush(leaveRequest.getUser());
            }
        }
    }
}
