package com.leaveapp.telnetleavemanager.services;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.leaveapp.telnetleavemanager.dto.LeaveRequest;
import com.leaveapp.telnetleavemanager.entities.EmployeeLeave;
import com.leaveapp.telnetleavemanager.entities.ExceptionalLeaveType;
import com.leaveapp.telnetleavemanager.entities.LeaveType;
import com.leaveapp.telnetleavemanager.entities.OrganizationalUnit;
import com.leaveapp.telnetleavemanager.entities.Status;
import com.leaveapp.telnetleavemanager.entities.Team;
import com.leaveapp.telnetleavemanager.entities.TimeOfDay;
import com.leaveapp.telnetleavemanager.exceptions.InsufficientLeaveBalanceException;
import com.leaveapp.telnetleavemanager.repositories.EmployeeLeaveRepository;
import com.leaveapp.telnetleavemanager.user.Gender;
import com.leaveapp.telnetleavemanager.user.Role;
import com.leaveapp.telnetleavemanager.user.User;
import com.leaveapp.telnetleavemanager.user.UserRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {EmployeeLeaveService.class})
@ExtendWith(SpringExtension.class)
@PropertySource("classpath:application-test.properties")
@EnableConfigurationProperties
class EmployeeLeaveServiceTest {
    @MockBean
    private EmployeeLeaveRepository employeeLeaveRepository;

    @Autowired
    private EmployeeLeaveService employeeLeaveService;

    @MockBean
    private MailingService mailingService;

    @MockBean
    private UserRepository userRepository;

    /**
     * Method under test:
     * {@link EmployeeLeaveService#createLeaveRequest(String, LeaveRequest)}
     */
    @Test
    void testCreateLeaveRequest() {
        // Arrange, Act and Assert
        assertThrows(IllegalArgumentException.class,
                () -> employeeLeaveService.createLeaveRequest("jane.doe@example.org", new LeaveRequest()));
    }

    /**
     * Method under test:
     * {@link EmployeeLeaveService#createLeaveRequest(String, LeaveRequest)}
     */
    @Test
    void testCreateLeaveRequest2() {
        // Arrange
        when(employeeLeaveRepository.save(Mockito.<EmployeeLeave>any()))
                .thenThrow(new InsufficientLeaveBalanceException("An error occurred"));

        OrganizationalUnit organizationalUnit = new OrganizationalUnit();
        organizationalUnit.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit.setId(1L);
        organizationalUnit.setManager(new User());
        organizationalUnit.setMembers(new ArrayList<>());
        organizationalUnit.setName("Name");
        organizationalUnit.setTeams(new HashSet<>());

        Team team = new Team();
        team.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        team.setDescription("The characteristics of someone or something");
        team.setId(1L);
        team.setManager(new User());
        team.setMembers(new ArrayList<>());
        team.setMinimumAttendance(3);
        team.setName("Name");
        team.setOrganizationalUnit(new OrganizationalUnit());

        User manager = new User();
        manager.setEmail("jane.doe@example.org");
        manager.setExternalActivitiesLimit(1);
        manager.setFirstName("Jane");
        manager.setGender(Gender.MALE);
        manager.setId(1);
        manager.setLastName("Doe");
        manager.setLeaveDays(10.0d);
        manager.setLeaves(new ArrayList<>());
        manager.setMfaEnabled(true);
        manager.setOnLeave(true);
        manager.setOrganizationalUnit(organizationalUnit);
        manager.setPassword("iloveyou");
        manager.setPhone("6625550144");
        manager.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        manager.setRole(Role.USER);
        manager.setSecret("Secret");
        manager.setTeam(team);
        manager.setTokens(new ArrayList<>());

        OrganizationalUnit organizationalUnit2 = new OrganizationalUnit();
        organizationalUnit2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit2.setId(1L);
        organizationalUnit2.setManager(manager);
        organizationalUnit2.setMembers(new ArrayList<>());
        organizationalUnit2.setName("Name");
        organizationalUnit2.setTeams(new HashSet<>());

        OrganizationalUnit organizationalUnit3 = new OrganizationalUnit();
        organizationalUnit3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit3.setId(1L);
        organizationalUnit3.setManager(new User());
        organizationalUnit3.setMembers(new ArrayList<>());
        organizationalUnit3.setName("Name");
        organizationalUnit3.setTeams(new HashSet<>());

        Team team2 = new Team();
        team2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        team2.setDescription("The characteristics of someone or something");
        team2.setId(1L);
        team2.setManager(new User());
        team2.setMembers(new ArrayList<>());
        team2.setMinimumAttendance(3);
        team2.setName("Name");
        team2.setOrganizationalUnit(new OrganizationalUnit());

        User manager2 = new User();
        manager2.setEmail("jane.doe@example.org");
        manager2.setExternalActivitiesLimit(1);
        manager2.setFirstName("Jane");
        manager2.setGender(Gender.MALE);
        manager2.setId(1);
        manager2.setLastName("Doe");
        manager2.setLeaveDays(10.0d);
        manager2.setLeaves(new ArrayList<>());
        manager2.setMfaEnabled(true);
        manager2.setOnLeave(true);
        manager2.setOrganizationalUnit(organizationalUnit3);
        manager2.setPassword("iloveyou");
        manager2.setPhone("6625550144");
        manager2.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        manager2.setRole(Role.USER);
        manager2.setSecret("Secret");
        manager2.setTeam(team2);
        manager2.setTokens(new ArrayList<>());

        User manager3 = new User();
        manager3.setEmail("jane.doe@example.org");
        manager3.setExternalActivitiesLimit(1);
        manager3.setFirstName("Jane");
        manager3.setGender(Gender.MALE);
        manager3.setId(1);
        manager3.setLastName("Doe");
        manager3.setLeaveDays(10.0d);
        manager3.setLeaves(new ArrayList<>());
        manager3.setMfaEnabled(true);
        manager3.setOnLeave(true);
        manager3.setOrganizationalUnit(new OrganizationalUnit());
        manager3.setPassword("iloveyou");
        manager3.setPhone("6625550144");
        manager3.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        manager3.setRole(Role.USER);
        manager3.setSecret("Secret");
        manager3.setTeam(new Team());
        manager3.setTokens(new ArrayList<>());

        OrganizationalUnit organizationalUnit4 = new OrganizationalUnit();
        organizationalUnit4.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit4.setId(1L);
        organizationalUnit4.setManager(manager3);
        organizationalUnit4.setMembers(new ArrayList<>());
        organizationalUnit4.setName("Name");
        organizationalUnit4.setTeams(new HashSet<>());

        Team team3 = new Team();
        team3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        team3.setDescription("The characteristics of someone or something");
        team3.setId(1L);
        team3.setManager(manager2);
        team3.setMembers(new ArrayList<>());
        team3.setMinimumAttendance(3);
        team3.setName("Name");
        team3.setOrganizationalUnit(organizationalUnit4);

        User user = new User();
        user.setEmail("jane.doe@example.org");
        user.setExternalActivitiesLimit(1);
        user.setFirstName("Jane");
        user.setGender(Gender.MALE);
        user.setId(1);
        user.setLastName("Doe");
        user.setLeaveDays(10.0d);
        user.setLeaves(new ArrayList<>());
        user.setMfaEnabled(true);
        user.setOnLeave(true);
        user.setOrganizationalUnit(organizationalUnit2);
        user.setPassword("iloveyou");
        user.setPhone("6625550144");
        user.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        user.setRole(Role.USER);
        user.setSecret("Secret");
        user.setTeam(team3);
        user.setTokens(new ArrayList<>());
        Optional<User> ofResult = Optional.of(user);
        when(userRepository.findByEmail(Mockito.<String>any())).thenReturn(ofResult);
        LeaveRequest leaveRequest = LeaveRequest.builder()
                .leaveType(LeaveType.PERSONAL_LEAVE)
                .reason("Just cause")
                .userEmail("jane.doe@example.org")
                .build();

        // Act and Assert
        assertThrows(InsufficientLeaveBalanceException.class,
                () -> employeeLeaveService.createLeaveRequest("jane.doe@example.org", leaveRequest));
        verify(userRepository, atLeast(1)).findByEmail(eq("jane.doe@example.org"));
        verify(employeeLeaveRepository).save(isA(EmployeeLeave.class));
    }

    /**
     * Method under test:
     * {@link EmployeeLeaveService#createLeaveRequest(String, LeaveRequest)}
     */
    @Test
    void testCreateLeaveRequest3() {
        // Arrange
        OrganizationalUnit organizationalUnit = new OrganizationalUnit();
        organizationalUnit.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit.setId(1L);
        organizationalUnit.setManager(new User());
        organizationalUnit.setMembers(new ArrayList<>());
        organizationalUnit.setName("Name");
        organizationalUnit.setTeams(new HashSet<>());

        Team team = new Team();
        team.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        team.setDescription("The characteristics of someone or something");
        team.setId(1L);
        team.setManager(new User());
        team.setMembers(new ArrayList<>());
        team.setMinimumAttendance(3);
        team.setName("Name");
        team.setOrganizationalUnit(new OrganizationalUnit());

        User manager = new User();
        manager.setEmail("jane.doe@example.org");
        manager.setExternalActivitiesLimit(1);
        manager.setFirstName("Jane");
        manager.setGender(Gender.MALE);
        manager.setId(1);
        manager.setLastName("Doe");
        manager.setLeaveDays(10.0d);
        manager.setLeaves(new ArrayList<>());
        manager.setMfaEnabled(true);
        manager.setOnLeave(true);
        manager.setOrganizationalUnit(organizationalUnit);
        manager.setPassword("iloveyou");
        manager.setPhone("6625550144");
        manager.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        manager.setRole(Role.USER);
        manager.setSecret("Secret");
        manager.setTeam(team);
        manager.setTokens(new ArrayList<>());

        OrganizationalUnit organizationalUnit2 = new OrganizationalUnit();
        organizationalUnit2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit2.setId(1L);
        organizationalUnit2.setManager(manager);
        organizationalUnit2.setMembers(new ArrayList<>());
        organizationalUnit2.setName("Name");
        organizationalUnit2.setTeams(new HashSet<>());

        OrganizationalUnit organizationalUnit3 = new OrganizationalUnit();
        organizationalUnit3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit3.setId(1L);
        organizationalUnit3.setManager(new User());
        organizationalUnit3.setMembers(new ArrayList<>());
        organizationalUnit3.setName("Name");
        organizationalUnit3.setTeams(new HashSet<>());

        Team team2 = new Team();
        team2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        team2.setDescription("The characteristics of someone or something");
        team2.setId(1L);
        team2.setManager(new User());
        team2.setMembers(new ArrayList<>());
        team2.setMinimumAttendance(3);
        team2.setName("Name");
        team2.setOrganizationalUnit(new OrganizationalUnit());

        User manager2 = new User();
        manager2.setEmail("jane.doe@example.org");
        manager2.setExternalActivitiesLimit(1);
        manager2.setFirstName("Jane");
        manager2.setGender(Gender.MALE);
        manager2.setId(1);
        manager2.setLastName("Doe");
        manager2.setLeaveDays(10.0d);
        manager2.setLeaves(new ArrayList<>());
        manager2.setMfaEnabled(true);
        manager2.setOnLeave(true);
        manager2.setOrganizationalUnit(organizationalUnit3);
        manager2.setPassword("iloveyou");
        manager2.setPhone("6625550144");
        manager2.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        manager2.setRole(Role.USER);
        manager2.setSecret("Secret");
        manager2.setTeam(team2);
        manager2.setTokens(new ArrayList<>());

        User manager3 = new User();
        manager3.setEmail("jane.doe@example.org");
        manager3.setExternalActivitiesLimit(1);
        manager3.setFirstName("Jane");
        manager3.setGender(Gender.MALE);
        manager3.setId(1);
        manager3.setLastName("Doe");
        manager3.setLeaveDays(10.0d);
        manager3.setLeaves(new ArrayList<>());
        manager3.setMfaEnabled(true);
        manager3.setOnLeave(true);
        manager3.setOrganizationalUnit(new OrganizationalUnit());
        manager3.setPassword("iloveyou");
        manager3.setPhone("6625550144");
        manager3.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        manager3.setRole(Role.USER);
        manager3.setSecret("Secret");
        manager3.setTeam(new Team());
        manager3.setTokens(new ArrayList<>());

        OrganizationalUnit organizationalUnit4 = new OrganizationalUnit();
        organizationalUnit4.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit4.setId(1L);
        organizationalUnit4.setManager(manager3);
        organizationalUnit4.setMembers(new ArrayList<>());
        organizationalUnit4.setName("Name");
        organizationalUnit4.setTeams(new HashSet<>());

        Team team3 = new Team();
        team3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        team3.setDescription("The characteristics of someone or something");
        team3.setId(1L);
        team3.setManager(manager2);
        team3.setMembers(new ArrayList<>());
        team3.setMinimumAttendance(3);
        team3.setName("Name");
        team3.setOrganizationalUnit(organizationalUnit4);

        User user = new User();
        user.setEmail("jane.doe@example.org");
        user.setExternalActivitiesLimit(1);
        user.setFirstName("Jane");
        user.setGender(Gender.MALE);
        user.setId(1);
        user.setLastName("Doe");
        user.setLeaveDays(10.0d);
        user.setLeaves(new ArrayList<>());
        user.setMfaEnabled(true);
        user.setOnLeave(true);
        user.setOrganizationalUnit(organizationalUnit2);
        user.setPassword("iloveyou");
        user.setPhone("6625550144");
        user.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        user.setRole(Role.USER);
        user.setSecret("Secret");
        user.setTeam(team3);
        user.setTokens(new ArrayList<>());

        EmployeeLeave employeeLeave = new EmployeeLeave();
        employeeLeave.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        employeeLeave.setEndDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        employeeLeave.setExceptionalLeaveType(ExceptionalLeaveType.MATERNITY_LEAVE);
        employeeLeave.setId(1L);
        employeeLeave.setLeaveType(LeaveType.PERSONAL_LEAVE);
        employeeLeave.setReason("Just cause");
        employeeLeave.setStartDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        employeeLeave.setStatus(Status.PENDING);
        employeeLeave.setTimeOfDay(TimeOfDay.MORNING);
        employeeLeave.setUser(user);
        when(employeeLeaveRepository.save(Mockito.<EmployeeLeave>any())).thenReturn(employeeLeave);

        OrganizationalUnit organizationalUnit5 = new OrganizationalUnit();
        organizationalUnit5.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit5.setId(1L);
        organizationalUnit5.setManager(new User());
        organizationalUnit5.setMembers(new ArrayList<>());
        organizationalUnit5.setName("Name");
        organizationalUnit5.setTeams(new HashSet<>());

        Team team4 = new Team();
        team4.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        team4.setDescription("The characteristics of someone or something");
        team4.setId(1L);
        team4.setManager(new User());
        team4.setMembers(new ArrayList<>());
        team4.setMinimumAttendance(3);
        team4.setName("Name");
        team4.setOrganizationalUnit(new OrganizationalUnit());

        User manager4 = new User();
        manager4.setEmail("jane.doe@example.org");
        manager4.setExternalActivitiesLimit(1);
        manager4.setFirstName("Jane");
        manager4.setGender(Gender.MALE);
        manager4.setId(1);
        manager4.setLastName("Doe");
        manager4.setLeaveDays(10.0d);
        manager4.setLeaves(new ArrayList<>());
        manager4.setMfaEnabled(true);
        manager4.setOnLeave(true);
        manager4.setOrganizationalUnit(organizationalUnit5);
        manager4.setPassword("iloveyou");
        manager4.setPhone("6625550144");
        manager4.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        manager4.setRole(Role.USER);
        manager4.setSecret("Secret");
        manager4.setTeam(team4);
        manager4.setTokens(new ArrayList<>());

        OrganizationalUnit organizationalUnit6 = new OrganizationalUnit();
        organizationalUnit6.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit6.setId(1L);
        organizationalUnit6.setManager(manager4);
        organizationalUnit6.setMembers(new ArrayList<>());
        organizationalUnit6.setName("Name");
        organizationalUnit6.setTeams(new HashSet<>());

        OrganizationalUnit organizationalUnit7 = new OrganizationalUnit();
        organizationalUnit7.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit7.setId(1L);
        organizationalUnit7.setManager(new User());
        organizationalUnit7.setMembers(new ArrayList<>());
        organizationalUnit7.setName("Name");
        organizationalUnit7.setTeams(new HashSet<>());

        Team team5 = new Team();
        team5.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        team5.setDescription("The characteristics of someone or something");
        team5.setId(1L);
        team5.setManager(new User());
        team5.setMembers(new ArrayList<>());
        team5.setMinimumAttendance(3);
        team5.setName("Name");
        team5.setOrganizationalUnit(new OrganizationalUnit());

        User manager5 = new User();
        manager5.setEmail("jane.doe@example.org");
        manager5.setExternalActivitiesLimit(1);
        manager5.setFirstName("Jane");
        manager5.setGender(Gender.MALE);
        manager5.setId(1);
        manager5.setLastName("Doe");
        manager5.setLeaveDays(10.0d);
        manager5.setLeaves(new ArrayList<>());
        manager5.setMfaEnabled(true);
        manager5.setOnLeave(true);
        manager5.setOrganizationalUnit(organizationalUnit7);
        manager5.setPassword("iloveyou");
        manager5.setPhone("6625550144");
        manager5.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        manager5.setRole(Role.USER);
        manager5.setSecret("Secret");
        manager5.setTeam(team5);
        manager5.setTokens(new ArrayList<>());

        User manager6 = new User();
        manager6.setEmail("jane.doe@example.org");
        manager6.setExternalActivitiesLimit(1);
        manager6.setFirstName("Jane");
        manager6.setGender(Gender.MALE);
        manager6.setId(1);
        manager6.setLastName("Doe");
        manager6.setLeaveDays(10.0d);
        manager6.setLeaves(new ArrayList<>());
        manager6.setMfaEnabled(true);
        manager6.setOnLeave(true);
        manager6.setOrganizationalUnit(new OrganizationalUnit());
        manager6.setPassword("iloveyou");
        manager6.setPhone("6625550144");
        manager6.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        manager6.setRole(Role.USER);
        manager6.setSecret("Secret");
        manager6.setTeam(new Team());
        manager6.setTokens(new ArrayList<>());

        OrganizationalUnit organizationalUnit8 = new OrganizationalUnit();
        organizationalUnit8.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit8.setId(1L);
        organizationalUnit8.setManager(manager6);
        organizationalUnit8.setMembers(new ArrayList<>());
        organizationalUnit8.setName("Name");
        organizationalUnit8.setTeams(new HashSet<>());

        Team team6 = new Team();
        team6.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        team6.setDescription("The characteristics of someone or something");
        team6.setId(1L);
        team6.setManager(manager5);
        team6.setMembers(new ArrayList<>());
        team6.setMinimumAttendance(3);
        team6.setName("Name");
        team6.setOrganizationalUnit(organizationalUnit8);

        User user2 = new User();
        user2.setEmail("jane.doe@example.org");
        user2.setExternalActivitiesLimit(1);
        user2.setFirstName("Jane");
        user2.setGender(Gender.MALE);
        user2.setId(1);
        user2.setLastName("Doe");
        user2.setLeaveDays(10.0d);
        user2.setLeaves(new ArrayList<>());
        user2.setMfaEnabled(true);
        user2.setOnLeave(true);
        user2.setOrganizationalUnit(organizationalUnit6);
        user2.setPassword("iloveyou");
        user2.setPhone("6625550144");
        user2.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        user2.setRole(Role.USER);
        user2.setSecret("Secret");
        user2.setTeam(team6);
        user2.setTokens(new ArrayList<>());
        Optional<User> ofResult = Optional.of(user2);
        when(userRepository.findByEmail(Mockito.<String>any())).thenReturn(ofResult);
        when(mailingService.sendMail(Mockito.<String>any(), Mockito.<String>any(), Mockito.<String>any()))
                .thenReturn(new CompletableFuture<>());
        LeaveRequest leaveRequest = LeaveRequest.builder()
                .leaveType(LeaveType.PERSONAL_LEAVE)
                .reason("Just cause")
                .userEmail("jane.doe@example.org")
                .build();

        // Act
        employeeLeaveService.createLeaveRequest("jane.doe@example.org", leaveRequest);

        // Assert
        verify(mailingService, atLeast(1)).sendMail(eq("jane.doe@example.org"), eq("Leave request created"),
                Mockito.<String>any());
        verify(userRepository, atLeast(1)).findByEmail(eq("jane.doe@example.org"));
        verify(employeeLeaveRepository).save(isA(EmployeeLeave.class));
    }

    /**
     * Method under test:
     * {@link EmployeeLeaveService#createLeaveRequest(String, LeaveRequest)}
     */
    @Test
    void testCreateLeaveRequest4() {
        // Arrange
        OrganizationalUnit organizationalUnit = new OrganizationalUnit();
        organizationalUnit.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit.setId(1L);
        organizationalUnit.setManager(new User());
        organizationalUnit.setMembers(new ArrayList<>());
        organizationalUnit.setName("Name");
        organizationalUnit.setTeams(new HashSet<>());

        Team team = new Team();
        team.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        team.setDescription("The characteristics of someone or something");
        team.setId(1L);
        team.setManager(new User());
        team.setMembers(new ArrayList<>());
        team.setMinimumAttendance(3);
        team.setName("Name");
        team.setOrganizationalUnit(new OrganizationalUnit());

        User manager = new User();
        manager.setEmail("jane.doe@example.org");
        manager.setExternalActivitiesLimit(1);
        manager.setFirstName("Jane");
        manager.setGender(Gender.MALE);
        manager.setId(1);
        manager.setLastName("Doe");
        manager.setLeaveDays(10.0d);
        manager.setLeaves(new ArrayList<>());
        manager.setMfaEnabled(true);
        manager.setOnLeave(true);
        manager.setOrganizationalUnit(organizationalUnit);
        manager.setPassword("iloveyou");
        manager.setPhone("6625550144");
        manager.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        manager.setRole(Role.USER);
        manager.setSecret("Secret");
        manager.setTeam(team);
        manager.setTokens(new ArrayList<>());

        OrganizationalUnit organizationalUnit2 = new OrganizationalUnit();
        organizationalUnit2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit2.setId(1L);
        organizationalUnit2.setManager(manager);
        organizationalUnit2.setMembers(new ArrayList<>());
        organizationalUnit2.setName("Name");
        organizationalUnit2.setTeams(new HashSet<>());

        OrganizationalUnit organizationalUnit3 = new OrganizationalUnit();
        organizationalUnit3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit3.setId(1L);
        organizationalUnit3.setManager(new User());
        organizationalUnit3.setMembers(new ArrayList<>());
        organizationalUnit3.setName("Name");
        organizationalUnit3.setTeams(new HashSet<>());

        Team team2 = new Team();
        team2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        team2.setDescription("The characteristics of someone or something");
        team2.setId(1L);
        team2.setManager(new User());
        team2.setMembers(new ArrayList<>());
        team2.setMinimumAttendance(3);
        team2.setName("Name");
        team2.setOrganizationalUnit(new OrganizationalUnit());

        User manager2 = new User();
        manager2.setEmail("jane.doe@example.org");
        manager2.setExternalActivitiesLimit(1);
        manager2.setFirstName("Jane");
        manager2.setGender(Gender.MALE);
        manager2.setId(1);
        manager2.setLastName("Doe");
        manager2.setLeaveDays(10.0d);
        manager2.setLeaves(new ArrayList<>());
        manager2.setMfaEnabled(true);
        manager2.setOnLeave(true);
        manager2.setOrganizationalUnit(organizationalUnit3);
        manager2.setPassword("iloveyou");
        manager2.setPhone("6625550144");
        manager2.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        manager2.setRole(Role.USER);
        manager2.setSecret("Secret");
        manager2.setTeam(team2);
        manager2.setTokens(new ArrayList<>());

        User manager3 = new User();
        manager3.setEmail("jane.doe@example.org");
        manager3.setExternalActivitiesLimit(1);
        manager3.setFirstName("Jane");
        manager3.setGender(Gender.MALE);
        manager3.setId(1);
        manager3.setLastName("Doe");
        manager3.setLeaveDays(10.0d);
        manager3.setLeaves(new ArrayList<>());
        manager3.setMfaEnabled(true);
        manager3.setOnLeave(true);
        manager3.setOrganizationalUnit(new OrganizationalUnit());
        manager3.setPassword("iloveyou");
        manager3.setPhone("6625550144");
        manager3.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        manager3.setRole(Role.USER);
        manager3.setSecret("Secret");
        manager3.setTeam(new Team());
        manager3.setTokens(new ArrayList<>());

        OrganizationalUnit organizationalUnit4 = new OrganizationalUnit();
        organizationalUnit4.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit4.setId(1L);
        organizationalUnit4.setManager(manager3);
        organizationalUnit4.setMembers(new ArrayList<>());
        organizationalUnit4.setName("Name");
        organizationalUnit4.setTeams(new HashSet<>());

        Team team3 = new Team();
        team3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        team3.setDescription("The characteristics of someone or something");
        team3.setId(1L);
        team3.setManager(manager2);
        team3.setMembers(new ArrayList<>());
        team3.setMinimumAttendance(3);
        team3.setName("Name");
        team3.setOrganizationalUnit(organizationalUnit4);

        User user = new User();
        user.setEmail("jane.doe@example.org");
        user.setExternalActivitiesLimit(1);
        user.setFirstName("Jane");
        user.setGender(Gender.MALE);
        user.setId(1);
        user.setLastName("Doe");
        user.setLeaveDays(10.0d);
        user.setLeaves(new ArrayList<>());
        user.setMfaEnabled(true);
        user.setOnLeave(true);
        user.setOrganizationalUnit(organizationalUnit2);
        user.setPassword("iloveyou");
        user.setPhone("6625550144");
        user.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        user.setRole(Role.USER);
        user.setSecret("Secret");
        user.setTeam(team3);
        user.setTokens(new ArrayList<>());

        EmployeeLeave employeeLeave = new EmployeeLeave();
        employeeLeave.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        employeeLeave.setEndDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        employeeLeave.setExceptionalLeaveType(ExceptionalLeaveType.MATERNITY_LEAVE);
        employeeLeave.setId(1L);
        employeeLeave.setLeaveType(LeaveType.PERSONAL_LEAVE);
        employeeLeave.setReason("Just cause");
        employeeLeave.setStartDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        employeeLeave.setStatus(Status.PENDING);
        employeeLeave.setTimeOfDay(TimeOfDay.MORNING);
        employeeLeave.setUser(user);
        when(employeeLeaveRepository.save(Mockito.<EmployeeLeave>any())).thenReturn(employeeLeave);

        OrganizationalUnit organizationalUnit5 = new OrganizationalUnit();
        organizationalUnit5.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit5.setId(1L);
        organizationalUnit5.setManager(new User());
        organizationalUnit5.setMembers(new ArrayList<>());
        organizationalUnit5.setName("Name");
        organizationalUnit5.setTeams(new HashSet<>());

        Team team4 = new Team();
        team4.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        team4.setDescription("The characteristics of someone or something");
        team4.setId(1L);
        team4.setManager(new User());
        team4.setMembers(new ArrayList<>());
        team4.setMinimumAttendance(3);
        team4.setName("Name");
        team4.setOrganizationalUnit(new OrganizationalUnit());

        User manager4 = new User();
        manager4.setEmail("jane.doe@example.org");
        manager4.setExternalActivitiesLimit(1);
        manager4.setFirstName("Jane");
        manager4.setGender(Gender.MALE);
        manager4.setId(1);
        manager4.setLastName("Doe");
        manager4.setLeaveDays(10.0d);
        manager4.setLeaves(new ArrayList<>());
        manager4.setMfaEnabled(true);
        manager4.setOnLeave(true);
        manager4.setOrganizationalUnit(organizationalUnit5);
        manager4.setPassword("iloveyou");
        manager4.setPhone("6625550144");
        manager4.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        manager4.setRole(Role.USER);
        manager4.setSecret("Secret");
        manager4.setTeam(team4);
        manager4.setTokens(new ArrayList<>());

        OrganizationalUnit organizationalUnit6 = new OrganizationalUnit();
        organizationalUnit6.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit6.setId(1L);
        organizationalUnit6.setManager(manager4);
        organizationalUnit6.setMembers(new ArrayList<>());
        organizationalUnit6.setName("Name");
        organizationalUnit6.setTeams(new HashSet<>());

        OrganizationalUnit organizationalUnit7 = new OrganizationalUnit();
        organizationalUnit7.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit7.setId(1L);
        organizationalUnit7.setManager(new User());
        organizationalUnit7.setMembers(new ArrayList<>());
        organizationalUnit7.setName("Name");
        organizationalUnit7.setTeams(new HashSet<>());

        Team team5 = new Team();
        team5.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        team5.setDescription("The characteristics of someone or something");
        team5.setId(1L);
        team5.setManager(new User());
        team5.setMembers(new ArrayList<>());
        team5.setMinimumAttendance(3);
        team5.setName("Name");
        team5.setOrganizationalUnit(new OrganizationalUnit());

        User manager5 = new User();
        manager5.setEmail("jane.doe@example.org");
        manager5.setExternalActivitiesLimit(1);
        manager5.setFirstName("Jane");
        manager5.setGender(Gender.MALE);
        manager5.setId(1);
        manager5.setLastName("Doe");
        manager5.setLeaveDays(10.0d);
        manager5.setLeaves(new ArrayList<>());
        manager5.setMfaEnabled(true);
        manager5.setOnLeave(true);
        manager5.setOrganizationalUnit(organizationalUnit7);
        manager5.setPassword("iloveyou");
        manager5.setPhone("6625550144");
        manager5.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        manager5.setRole(Role.USER);
        manager5.setSecret("Secret");
        manager5.setTeam(team5);
        manager5.setTokens(new ArrayList<>());

        User manager6 = new User();
        manager6.setEmail("jane.doe@example.org");
        manager6.setExternalActivitiesLimit(1);
        manager6.setFirstName("Jane");
        manager6.setGender(Gender.MALE);
        manager6.setId(1);
        manager6.setLastName("Doe");
        manager6.setLeaveDays(10.0d);
        manager6.setLeaves(new ArrayList<>());
        manager6.setMfaEnabled(true);
        manager6.setOnLeave(true);
        manager6.setOrganizationalUnit(new OrganizationalUnit());
        manager6.setPassword("iloveyou");
        manager6.setPhone("6625550144");
        manager6.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        manager6.setRole(Role.USER);
        manager6.setSecret("Secret");
        manager6.setTeam(new Team());
        manager6.setTokens(new ArrayList<>());

        OrganizationalUnit organizationalUnit8 = new OrganizationalUnit();
        organizationalUnit8.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit8.setId(1L);
        organizationalUnit8.setManager(manager6);
        organizationalUnit8.setMembers(new ArrayList<>());
        organizationalUnit8.setName("Name");
        organizationalUnit8.setTeams(new HashSet<>());

        Team team6 = new Team();
        team6.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        team6.setDescription("The characteristics of someone or something");
        team6.setId(1L);
        team6.setManager(manager5);
        team6.setMembers(new ArrayList<>());
        team6.setMinimumAttendance(3);
        team6.setName("Name");
        team6.setOrganizationalUnit(organizationalUnit8);

        User user2 = new User();
        user2.setEmail("jane.doe@example.org");
        user2.setExternalActivitiesLimit(1);
        user2.setFirstName("Jane");
        user2.setGender(Gender.MALE);
        user2.setId(1);
        user2.setLastName("Doe");
        user2.setLeaveDays(10.0d);
        user2.setLeaves(new ArrayList<>());
        user2.setMfaEnabled(true);
        user2.setOnLeave(true);
        user2.setOrganizationalUnit(organizationalUnit6);
        user2.setPassword("iloveyou");
        user2.setPhone("6625550144");
        user2.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        user2.setRole(Role.USER);
        user2.setSecret("Secret");
        user2.setTeam(team6);
        user2.setTokens(new ArrayList<>());
        Optional<User> ofResult = Optional.of(user2);
        when(userRepository.findByEmail(Mockito.<String>any())).thenReturn(ofResult);
        when(mailingService.sendMail(Mockito.<String>any(), Mockito.<String>any(), Mockito.<String>any()))
                .thenThrow(new InsufficientLeaveBalanceException("An error occurred"));
        LocalDateTime startDate = LocalDate.of(1970, 1, 1).atStartOfDay();

        // Act and Assert
        assertThrows(InsufficientLeaveBalanceException.class,
                () -> employeeLeaveService.createLeaveRequest("jane.doe@example.org",
                        new LeaveRequest("jane.doe@example.org", LeaveType.PERSONAL_LEAVE, ExceptionalLeaveType.MATERNITY_LEAVE,
                                TimeOfDay.MORNING, startDate, LocalDate.of(1970, 1, 1).atStartOfDay(), "Just cause")));
        verify(mailingService).sendMail(eq("jane.doe@example.org"), eq("Leave request created"),
                eq("Your leave request has been created"));
        verify(userRepository, atLeast(1)).findByEmail(eq("jane.doe@example.org"));
        verify(employeeLeaveRepository).save(isA(EmployeeLeave.class));
    }

    /**
     * Method under test:
     * {@link EmployeeLeaveService#treatLeaveRequest(String, Long, String)}
     */
    @Test
    void testTreatLeaveRequest() {
        // Arrange
        User manager = new User();
        manager.setEmail("jane.doe@example.org");
        manager.setExternalActivitiesLimit(1);
        manager.setFirstName("Jane");
        manager.setGender(Gender.MALE);
        manager.setId(1);
        manager.setLastName("Doe");
        manager.setLeaveDays(10.0d);
        manager.setLeaves(new ArrayList<>());
        manager.setMfaEnabled(true);
        manager.setOnLeave(true);
        manager.setOrganizationalUnit(new OrganizationalUnit());
        manager.setPassword("iloveyou");
        manager.setPhone("6625550144");
        manager.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        manager.setRole(Role.USER);
        manager.setSecret("Secret");
        manager.setTeam(new Team());
        manager.setTokens(new ArrayList<>());

        OrganizationalUnit organizationalUnit = new OrganizationalUnit();
        organizationalUnit.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit.setId(1L);
        organizationalUnit.setManager(manager);
        organizationalUnit.setMembers(new ArrayList<>());
        organizationalUnit.setName("Name");
        organizationalUnit.setTeams(new HashSet<>());

        User manager2 = new User();
        manager2.setEmail("jane.doe@example.org");
        manager2.setExternalActivitiesLimit(1);
        manager2.setFirstName("Jane");
        manager2.setGender(Gender.MALE);
        manager2.setId(1);
        manager2.setLastName("Doe");
        manager2.setLeaveDays(10.0d);
        manager2.setLeaves(new ArrayList<>());
        manager2.setMfaEnabled(true);
        manager2.setOnLeave(true);
        manager2.setOrganizationalUnit(new OrganizationalUnit());
        manager2.setPassword("iloveyou");
        manager2.setPhone("6625550144");
        manager2.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        manager2.setRole(Role.USER);
        manager2.setSecret("Secret");
        manager2.setTeam(new Team());
        manager2.setTokens(new ArrayList<>());

        OrganizationalUnit organizationalUnit2 = new OrganizationalUnit();
        organizationalUnit2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit2.setId(1L);
        organizationalUnit2.setManager(new User());
        organizationalUnit2.setMembers(new ArrayList<>());
        organizationalUnit2.setName("Name");
        organizationalUnit2.setTeams(new HashSet<>());

        Team team = new Team();
        team.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        team.setDescription("The characteristics of someone or something");
        team.setId(1L);
        team.setManager(manager2);
        team.setMembers(new ArrayList<>());
        team.setMinimumAttendance(3);
        team.setName("Name");
        team.setOrganizationalUnit(organizationalUnit2);

        User user = new User();
        user.setEmail("jane.doe@example.org");
        user.setExternalActivitiesLimit(1);
        user.setFirstName("Jane");
        user.setGender(Gender.MALE);
        user.setId(1);
        user.setLastName("Doe");
        user.setLeaveDays(10.0d);
        user.setLeaves(new ArrayList<>());
        user.setMfaEnabled(true);
        user.setOnLeave(true);
        user.setOrganizationalUnit(organizationalUnit);
        user.setPassword("iloveyou");
        user.setPhone("6625550144");
        user.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        user.setRole(Role.USER);
        user.setSecret("Secret");
        user.setTeam(team);
        user.setTokens(new ArrayList<>());

        EmployeeLeave employeeLeave = new EmployeeLeave();
        employeeLeave.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        employeeLeave.setEndDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        employeeLeave.setExceptionalLeaveType(ExceptionalLeaveType.MATERNITY_LEAVE);
        employeeLeave.setId(1L);
        employeeLeave.setLeaveType(LeaveType.PERSONAL_LEAVE);
        employeeLeave.setReason("Just cause");
        employeeLeave.setStartDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        employeeLeave.setStatus(Status.PENDING);
        employeeLeave.setTimeOfDay(TimeOfDay.MORNING);
        employeeLeave.setUser(user);
        Optional<EmployeeLeave> ofResult = Optional.of(employeeLeave);
        when(employeeLeaveRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);

        OrganizationalUnit organizationalUnit3 = new OrganizationalUnit();
        organizationalUnit3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit3.setId(1L);
        organizationalUnit3.setManager(new User());
        organizationalUnit3.setMembers(new ArrayList<>());
        organizationalUnit3.setName("Name");
        organizationalUnit3.setTeams(new HashSet<>());

        Team team2 = new Team();
        team2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        team2.setDescription("The characteristics of someone or something");
        team2.setId(1L);
        team2.setManager(new User());
        team2.setMembers(new ArrayList<>());
        team2.setMinimumAttendance(3);
        team2.setName("Name");
        team2.setOrganizationalUnit(new OrganizationalUnit());

        User manager3 = new User();
        manager3.setEmail("jane.doe@example.org");
        manager3.setExternalActivitiesLimit(1);
        manager3.setFirstName("Jane");
        manager3.setGender(Gender.MALE);
        manager3.setId(1);
        manager3.setLastName("Doe");
        manager3.setLeaveDays(10.0d);
        manager3.setLeaves(new ArrayList<>());
        manager3.setMfaEnabled(true);
        manager3.setOnLeave(true);
        manager3.setOrganizationalUnit(organizationalUnit3);
        manager3.setPassword("iloveyou");
        manager3.setPhone("6625550144");
        manager3.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        manager3.setRole(Role.USER);
        manager3.setSecret("Secret");
        manager3.setTeam(team2);
        manager3.setTokens(new ArrayList<>());

        OrganizationalUnit organizationalUnit4 = new OrganizationalUnit();
        organizationalUnit4.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit4.setId(1L);
        organizationalUnit4.setManager(manager3);
        organizationalUnit4.setMembers(new ArrayList<>());
        organizationalUnit4.setName("Name");
        organizationalUnit4.setTeams(new HashSet<>());

        OrganizationalUnit organizationalUnit5 = new OrganizationalUnit();
        organizationalUnit5.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit5.setId(1L);
        organizationalUnit5.setManager(new User());
        organizationalUnit5.setMembers(new ArrayList<>());
        organizationalUnit5.setName("Name");
        organizationalUnit5.setTeams(new HashSet<>());

        Team team3 = new Team();
        team3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        team3.setDescription("The characteristics of someone or something");
        team3.setId(1L);
        team3.setManager(new User());
        team3.setMembers(new ArrayList<>());
        team3.setMinimumAttendance(3);
        team3.setName("Name");
        team3.setOrganizationalUnit(new OrganizationalUnit());

        User manager4 = new User();
        manager4.setEmail("jane.doe@example.org");
        manager4.setExternalActivitiesLimit(1);
        manager4.setFirstName("Jane");
        manager4.setGender(Gender.MALE);
        manager4.setId(1);
        manager4.setLastName("Doe");
        manager4.setLeaveDays(10.0d);
        manager4.setLeaves(new ArrayList<>());
        manager4.setMfaEnabled(true);
        manager4.setOnLeave(true);
        manager4.setOrganizationalUnit(organizationalUnit5);
        manager4.setPassword("iloveyou");
        manager4.setPhone("6625550144");
        manager4.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        manager4.setRole(Role.USER);
        manager4.setSecret("Secret");
        manager4.setTeam(team3);
        manager4.setTokens(new ArrayList<>());

        User manager5 = new User();
        manager5.setEmail("jane.doe@example.org");
        manager5.setExternalActivitiesLimit(1);
        manager5.setFirstName("Jane");
        manager5.setGender(Gender.MALE);
        manager5.setId(1);
        manager5.setLastName("Doe");
        manager5.setLeaveDays(10.0d);
        manager5.setLeaves(new ArrayList<>());
        manager5.setMfaEnabled(true);
        manager5.setOnLeave(true);
        manager5.setOrganizationalUnit(new OrganizationalUnit());
        manager5.setPassword("iloveyou");
        manager5.setPhone("6625550144");
        manager5.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        manager5.setRole(Role.USER);
        manager5.setSecret("Secret");
        manager5.setTeam(new Team());
        manager5.setTokens(new ArrayList<>());

        OrganizationalUnit organizationalUnit6 = new OrganizationalUnit();
        organizationalUnit6.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit6.setId(1L);
        organizationalUnit6.setManager(manager5);
        organizationalUnit6.setMembers(new ArrayList<>());
        organizationalUnit6.setName("Name");
        organizationalUnit6.setTeams(new HashSet<>());

        Team team4 = new Team();
        team4.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        team4.setDescription("The characteristics of someone or something");
        team4.setId(1L);
        team4.setManager(manager4);
        team4.setMembers(new ArrayList<>());
        team4.setMinimumAttendance(3);
        team4.setName("Name");
        team4.setOrganizationalUnit(organizationalUnit6);

        User user2 = new User();
        user2.setEmail("jane.doe@example.org");
        user2.setExternalActivitiesLimit(1);
        user2.setFirstName("Jane");
        user2.setGender(Gender.MALE);
        user2.setId(1);
        user2.setLastName("Doe");
        user2.setLeaveDays(10.0d);
        user2.setLeaves(new ArrayList<>());
        user2.setMfaEnabled(true);
        user2.setOnLeave(true);
        user2.setOrganizationalUnit(organizationalUnit4);
        user2.setPassword("iloveyou");
        user2.setPhone("6625550144");
        user2.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        user2.setRole(Role.USER);
        user2.setSecret("Secret");
        user2.setTeam(team4);
        user2.setTokens(new ArrayList<>());
        Optional<User> ofResult2 = Optional.of(user2);
        when(userRepository.findByEmail(Mockito.<String>any())).thenReturn(ofResult2);

        // Act and Assert
        assertThrows(IllegalStateException.class,
                () -> employeeLeaveService.treatLeaveRequest("jane.doe@example.org", 1L, "Status"));
        verify(userRepository).findByEmail(eq("jane.doe@example.org"));
        verify(employeeLeaveRepository).findById(eq(1L));
    }

    /**
     * Method under test:
     * {@link EmployeeLeaveService#treatLeaveRequest(String, Long, String)}
     */
    @Test
    void testTreatLeaveRequest2() {
        // Arrange
        User manager = new User();
        manager.setEmail("jane.doe@example.org");
        manager.setExternalActivitiesLimit(1);
        manager.setFirstName("Jane");
        manager.setGender(Gender.MALE);
        manager.setId(1);
        manager.setLastName("Doe");
        manager.setLeaveDays(10.0d);
        manager.setLeaves(new ArrayList<>());
        manager.setMfaEnabled(true);
        manager.setOnLeave(true);
        manager.setOrganizationalUnit(new OrganizationalUnit());
        manager.setPassword("iloveyou");
        manager.setPhone("6625550144");
        manager.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        manager.setRole(Role.USER);
        manager.setSecret("Secret");
        manager.setTeam(new Team());
        manager.setTokens(new ArrayList<>());

        OrganizationalUnit organizationalUnit = new OrganizationalUnit();
        organizationalUnit.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit.setId(1L);
        organizationalUnit.setManager(manager);
        organizationalUnit.setMembers(new ArrayList<>());
        organizationalUnit.setName("Name");
        organizationalUnit.setTeams(new HashSet<>());

        User manager2 = new User();
        manager2.setEmail("jane.doe@example.org");
        manager2.setExternalActivitiesLimit(1);
        manager2.setFirstName("Jane");
        manager2.setGender(Gender.MALE);
        manager2.setId(1);
        manager2.setLastName("Doe");
        manager2.setLeaveDays(10.0d);
        manager2.setLeaves(new ArrayList<>());
        manager2.setMfaEnabled(true);
        manager2.setOnLeave(true);
        manager2.setOrganizationalUnit(new OrganizationalUnit());
        manager2.setPassword("iloveyou");
        manager2.setPhone("6625550144");
        manager2.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        manager2.setRole(Role.USER);
        manager2.setSecret("Secret");
        manager2.setTeam(new Team());
        manager2.setTokens(new ArrayList<>());

        OrganizationalUnit organizationalUnit2 = new OrganizationalUnit();
        organizationalUnit2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        organizationalUnit2.setId(1L);
        organizationalUnit2.setManager(new User());
        organizationalUnit2.setMembers(new ArrayList<>());
        organizationalUnit2.setName("Name");
        organizationalUnit2.setTeams(new HashSet<>());

        Team team = new Team();
        team.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        team.setDescription("The characteristics of someone or something");
        team.setId(1L);
        team.setManager(manager2);
        team.setMembers(new ArrayList<>());
        team.setMinimumAttendance(3);
        team.setName("Name");
        team.setOrganizationalUnit(organizationalUnit2);

        User user = new User();
        user.setEmail("jane.doe@example.org");
        user.setExternalActivitiesLimit(1);
        user.setFirstName("Jane");
        user.setGender(Gender.MALE);
        user.setId(1);
        user.setLastName("Doe");
        user.setLeaveDays(10.0d);
        user.setLeaves(new ArrayList<>());
        user.setMfaEnabled(true);
        user.setOnLeave(true);
        user.setOrganizationalUnit(organizationalUnit);
        user.setPassword("iloveyou");
        user.setPhone("6625550144");
        user.setReturnDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        user.setRole(Role.USER);
        user.setSecret("Secret");
        user.setTeam(team);
        user.setTokens(new ArrayList<>());

        EmployeeLeave employeeLeave = new EmployeeLeave();
        employeeLeave.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        employeeLeave.setEndDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        employeeLeave.setExceptionalLeaveType(ExceptionalLeaveType.MATERNITY_LEAVE);
        employeeLeave.setId(1L);
        employeeLeave.setLeaveType(LeaveType.PERSONAL_LEAVE);
        employeeLeave.setReason("Just cause");
        employeeLeave.setStartDate(LocalDate.of(1970, 1, 1).atStartOfDay());
        employeeLeave.setStatus(Status.PENDING);
        employeeLeave.setTimeOfDay(TimeOfDay.MORNING);
        employeeLeave.setUser(user);
        Optional<EmployeeLeave> ofResult = Optional.of(employeeLeave);
        when(employeeLeaveRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        when(userRepository.findByEmail(Mockito.<String>any()))
                .thenThrow(new InsufficientLeaveBalanceException("An error occurred"));

        // Act and Assert
        assertThrows(InsufficientLeaveBalanceException.class,
                () -> employeeLeaveService.treatLeaveRequest("jane.doe@example.org", 1L, "Status"));
        verify(userRepository).findByEmail(eq("jane.doe@example.org"));
        verify(employeeLeaveRepository).findById(eq(1L));
    }

    /**
     * Method under test: {@link EmployeeLeaveService#deleteLeaveRequest(Integer)}
     */
    @Test
    void testDeleteLeaveRequest() {
        // Arrange
        doNothing().when(employeeLeaveRepository).deleteById(Mockito.<Long>any());

        // Act
        employeeLeaveService.deleteLeaveRequest(1);

        // Assert that nothing has changed
        verify(employeeLeaveRepository).deleteById(eq(1L));
    }

    /**
     * Method under test: {@link EmployeeLeaveService#deleteLeaveRequest(Integer)}
     */
    @Test
    void testDeleteLeaveRequest2() {
        // Arrange
        doThrow(new InsufficientLeaveBalanceException("An error occurred")).when(employeeLeaveRepository)
                .deleteById(Mockito.<Long>any());

        // Act and Assert
        assertThrows(InsufficientLeaveBalanceException.class, () -> employeeLeaveService.deleteLeaveRequest(1));
        verify(employeeLeaveRepository).deleteById(eq(1L));
    }
}
