package com.leaveapp.telnetleavemanager.services;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import com.leaveapp.telnetleavemanager.auth.AuthenticationService;
import com.leaveapp.telnetleavemanager.config.JwtService;
import com.leaveapp.telnetleavemanager.user.User;
import com.leaveapp.telnetleavemanager.user.UserRepository;
import com.leaveapp.telnetleavemanager.user.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class UserServiceTest {

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private JwtService jwtService;

    @MockBean
    private AuthenticationService authenticationService;

    @MockBean
    private MailingService mailingService;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserService userService;

    @BeforeEach
    public void setUp() {
        // Any additional setup can be done here
    }


    @Test
    public void testGetUserByEmail() {
        User expectedUser = new User();
        expectedUser.setEmail("john.doe@example.com");
        when(userRepository.findByEmail("john.doe@example.com")).thenReturn(java.util.Optional.of(expectedUser));

        User result = userService.getUserByEmail("john.doe@example.com");

        assertThat(result.getEmail(), is("john.doe@example.com"));
    }

    @Test
    public void testGetUserByEmailNotFound() {
        when(userRepository.findByEmail("missing@example.com")).thenReturn(java.util.Optional.empty());

        Exception exception = assertThrows(RuntimeException.class, () -> {
            userService.getUserByEmail("missing@example.com");
        });

        assertEquals("User with given email not found", exception.getMessage());
    }

    @Test
    public void testDeleteUser() {
        User user = new User();
        user.setEmail("john.doe@example.com");
        when(userRepository.findByEmail("john.doe@example.com")).thenReturn(java.util.Optional.of(user));

        userService.deleteUser("john.doe@example.com");

        verify(userRepository, times(1)).delete(user);
        verify(mailingService, times(1)).sendMail(eq("john.doe@example.com"), eq("Account deleted"), anyString());
    }
}
