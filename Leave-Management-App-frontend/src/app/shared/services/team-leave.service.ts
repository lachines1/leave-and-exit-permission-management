import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { TeamLeaveRequest } from "../models/team-leave-request.model";
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class TeamLeaveService {
  private apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  createTeamLeave(email: string, request: TeamLeaveRequest): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      }),
    };

    const params = new HttpParams().set("email", email);

    return this.http.post<any>(
      `${this.apiUrl}/api/v1/team-leave/management/create`,
      request,
      {
        headers: httpOptions.headers,
        params,
      }
    );
  }

  treatTeamLeaveRequest(
    email: string,
    id: number,
    status: string
  ): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };

    const params = new HttpParams().set("email", email).set("status", status);

    return this.http.put<any>(
      `${this.apiUrl}/api/v1/team-leave/management/treat/${id}`,
      {},
      {
        headers: httpOptions.headers,
        params,
      }
    );
  }

  getAllTeamLeaves(): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/v1/team-leave/management/all`,
      httpOptions
    );
  }

  getTeamLeaveById(id: number): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/v1/team-leave/management/get/${id}`,
      httpOptions
    );
  }

  deleteTeamLeave(id: number): Observable<void> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };

    return this.http.delete<void>(
      `${this.apiUrl}/api/v1/team-leave/management/delete/${id}`,
      httpOptions
    );
  }

  getTeamLeavesForTeam(teamId: number): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/v1/team-leave/management/for-team/${teamId}`,
      httpOptions
    );
  }

  deleteTeamLeaveRequest(teamLeaveId: number): Observable<void> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };

    return this.http.delete<void>(
      `${this.apiUrl}/api/v1/team-leave/management/delete/team-lead/${teamLeaveId}`,
      httpOptions
    );
  }

  updateTeamLeaveRequest(teamLeaveId: number): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };

    return this.http.put<any>(
      `${this.apiUrl}/api/v1/team-leave/management/update/${teamLeaveId}`,
      {},
      httpOptions
    );
  }

  updateTeamLeave(teamLeaveId: number, teamLeaveRequest: TeamLeaveRequest) {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.put<any>(
      `${this.apiUrl}/api/v1/team-leave/management/update/${teamLeaveId}`,
      teamLeaveRequest,
      httpOptions
    );
  }
}
