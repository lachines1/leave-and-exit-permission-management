import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { TeamRequest } from "../models/team-request.model";
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class TeamService {
  private apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  createTeam(request: any): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      }),
    };

    return this.http.post<any>(
      `${this.apiUrl}/api/v1/teams/management/create`,
      request,
      httpOptions
    );
  }

  getAllTeams(): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/v1/teams/management/all`,
      httpOptions
    );
  }

  getTeamById(id: number): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/v1/teams/get/${id}`,
      httpOptions
    );
  }

  deleteTeamById(id: number): Observable<void> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      }),
    };

    return this.http.delete<void>(
      `${this.apiUrl}/api/v1/teams/delete/${id}`,
      httpOptions
    );
  }

  getMembersOfTeam(teamId: number): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/v1/teams/members/${teamId}`,
      httpOptions
    );
  }

  getTeamsForOrganizationalUnit(organizationalUnitId: number): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/v1/teams/management/for-organizational-unit/${organizationalUnitId}`,
      httpOptions
    );
  }

  getTeamByName(teamName: string): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/v1/teams/management/${teamName}`,
      httpOptions
    );
  }

  getTeamsByManager(currentUserEmail: any) {
    const token = localStorage.getItem("token");
    const options = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/v1/teams/management/for-manager/${currentUserEmail}`,
      options
    );
  }

  getTeamByUser(currentUserEmail: any) {
    const token = localStorage.getItem("token");
    const options = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/v1/teams/management/for-user/${currentUserEmail}`,
      options
    );
  }

  addUserToTeam(email: string, teamName: string): Observable<any> {
    const token = localStorage.getItem("token");
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    });

    const params = new HttpParams()
      .set("userEmail", email)
      .set("teamName", teamName);

    return this.http.post<any>(
      `${this.apiUrl}/api/v1/users/manager/affect-team`,
      null,
      { headers, params }
    );
  }

  removeUserFromTeam(email: string, teamName: string): Observable<any> {
    const token = localStorage.getItem("token");
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    });

    const params = new HttpParams()
      .set("userEmail", email)
      .set("teamName", teamName);

    return this.http.post<any>(
      `${this.apiUrl}/api/v1/users/manager/remove-from-team`,
      null,
      { headers, params }
    );
  }

  updateTeam(teamId: number, teamRequest: TeamRequest) {
    const token = localStorage.getItem("token");
    const options = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
    return this.http.put<any>(
      `${this.apiUrl}/api/v1/teams/management/update/${teamId}`,
      teamRequest,
      options
    );
  }
  ////####
  getLeaveRequests(): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/v1/employee-leave/count-by-status`,
      httpOptions
    );
  }

  getMonthlyLeave(): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/v1/employee-leave/leave-counts-by-month`,
      httpOptions
    );
  }

  getPermissionRequest(): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/v1/employee-leave/permission-requests-pie-chart`,
      httpOptions
    );
  }

  getLeaveType(): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/v1/employee-leave/count-by-type`,
      httpOptions
    );
  }
  getLeaveDays(): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/v1/users/statistics/leaveDays`,
      httpOptions
    );
  }
  getExternalActivities(): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/v1/users/statistics/externalAuthorization`,
      httpOptions
    );
  }
  getExitStatus(): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/external-authorization/count-exit-by-status`,
      httpOptions
    );
  }
  getEmployeeLeave(): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/v1/users/statistics/onLeave`,
      httpOptions
    );
  }

}
