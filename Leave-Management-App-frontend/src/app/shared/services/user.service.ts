import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../models/user.model";
import {RegisterRequest} from "../models/register-request.model";
import {environment} from "../../../environments/environment";

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(private http: HttpClient) {
    }


    getAllUsers(): Observable<User[]> {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}` // Add the token as a Bearer token
            })
        };
        return this.http.get<any[]>(`${environment.apiUrl}/api/v1/admin/users/all`, httpOptions);
    }

    addUser(addRequest: RegisterRequest): Observable<any> {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}` // Add the token as a Bearer token
            })
        };
        return this.http.post<any[]>(`${environment.apiUrl}/api/v1/admin/users/addUser`, addRequest, httpOptions);
    }
}
