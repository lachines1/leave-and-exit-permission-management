import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { OrganizationalUnitRequest } from "../models/organizational-unit-request.model";
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class OrganizationalUnitService {
  private apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  createOrganizationalUnit(
    email: string,
    request: OrganizationalUnitRequest
  ): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      }),
    };

    const params = new HttpParams().set("email", email);

    return this.http.post<any>(
      `${this.apiUrl}/api/v1/organizational-unit/admin/create`,
      request,
      {
        headers: httpOptions.headers,
        params,
      }
    );
  }

  getAllOrganizationalUnits(): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/v1/organizational-unit/admin/all`,
      httpOptions
    );
  }

  getOrganizationalUnitById(id: number): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      }),
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/v1/organizational-unit/admin/get/${id}`,
      httpOptions
    );
  }

  deleteOrganizationalUnit(id: number): Observable<void> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };

    return this.http.delete<void>(
      `${this.apiUrl}/api/v1/organizational-unit/admin/delete/${id}`,
      httpOptions
    );
  }

  affectTeamToOrganizationalUnit(
    organizationalUnitId: number,
    teamName: string
  ): Observable<void> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };

    const params = new HttpParams()
      .set("organizationalUnitId", organizationalUnitId.toString())
      .set("teamName", teamName);

    return this.http.put<void>(
      `${this.apiUrl}/api/v1/organizational-unit/admin/affect-team`,
      {},
      {
        headers: httpOptions.headers,
        params,
      }
    );
  }

  affectMemberToOrganizationalUnit(
    organizationalUnitId: number,
    memberEmail: string
  ): Observable<void> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };

    const params = new HttpParams()
      .set("organizationalUnitId", organizationalUnitId.toString())
      .set("memberEmail", memberEmail);

    return this.http.put<void>(
      `${this.apiUrl}/api/v1/organizational-unit/admin/affect-member`,
      {},
      {
        headers: httpOptions.headers,
        params,
      }
    );
  }

  removeTeamFromOrganizationalUnit(
    organizationalUnitId: number,
    teamName: string
  ): Observable<void> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };

    const params = new HttpParams()
      .set("organizationalUnitId", organizationalUnitId.toString())
      .set("teamName", teamName);

    return this.http.put<void>(
      `${this.apiUrl}/api/v1/organizational-unit/admin/remove-team`,
      {},
      {
        headers: httpOptions.headers,
        params,
      }
    );
  }

  removeMemberFromOrganizationalUnit(
    organizationalUnitId: number,
    memberEmail: string
  ): Observable<void> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };

    const params = new HttpParams()
      .set("organizationalUnitId", organizationalUnitId.toString())
      .set("memberEmail", memberEmail);

    return this.http.put<void>(
      `${this.apiUrl}/api/v1/organizational-unit/admin/remove-member`,
      {},
      {
        headers: httpOptions.headers,
        params,
      }
    );
  }

  affectManagerToOrganizationalUnit(
    organizationalUnitId: number,
    managerEmail: string
  ): Observable<void> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };

    const params = new HttpParams()
      .set("organizationalUnitId", organizationalUnitId.toString())
      .set("managerEmail", managerEmail);

    return this.http.put<void>(
      `${this.apiUrl}/api/v1/organizational-unit/admin/affect-manager`,
      {},
      {
        headers: httpOptions.headers,
        params,
      }
    );
  }

  deleteOrganizationalUnitByName(unitName: string): Observable<void> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };

    return this.http.delete<void>(
      `${this.apiUrl}/api/v1/organizational-unit/admin/delete-by-name/${unitName}`,
      httpOptions
    );
  }

  getTeamsOfOrganizationalUnit(organizationalUnitId: number): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/v1/organizational-unit/admin/teams/${organizationalUnitId}`,
      httpOptions
    );
  }

  updateOrganizationalUnit(
    organizationalUnitId: number,
    organizationalUnitRequest: OrganizationalUnitRequest
  ) {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`, // Add the token as a Bearer token
      }),
    };
    return this.http.put<any>(
      `${this.apiUrl}/api/v1/organizational-unit/admin/update/${organizationalUnitId}`,
      organizationalUnitRequest,
      httpOptions
    );
  }
}
