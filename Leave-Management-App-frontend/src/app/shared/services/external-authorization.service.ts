import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { ExternalAuthorizationRequest } from "../models/external-authorization-request.model";
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class ExternalAuthorizationService {
  private apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  createExternalAuthorization(
    currentUserEmail: string,
    request: ExternalAuthorizationRequest
  ): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      }),
    };

    const params = new HttpParams().set("currentUserEmail", currentUserEmail);

    return this.http.post<any>(
      `${this.apiUrl}/api/external-authorization/create`,
      request,
      {
        headers: httpOptions.headers,
        params,
      }
    );
  }

  getAllExternalAuthorizations(): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/external-authorization/all`,
      httpOptions
    );
  }

  getExternalAuthorizationById(id: number): Observable<any> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/external-authorization/get/${id}`,
      httpOptions
    );
  }

  deleteExternalAuthorization(id: number): Observable<void> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };

    return this.http.delete<void>(
      `${this.apiUrl}/api/external-authorization/delete/${id}`,
      httpOptions
    );
  }

  treatExternalAuthorization(
    id: number,
    status: string,
    currentUserEmail: string
  ): Observable<void> {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };

    const params = new HttpParams()
      .set("status", status)
      .set("currentUserEmail", currentUserEmail);

    return this.http.put<void>(
      `${this.apiUrl}/api/external-authorization/treat/${id}`,
      {},
      {
        headers: httpOptions.headers,
        params,
      }
    );
  }

  getExternalAuthorizationsByUser(currentUserEmail: any) {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/external-authorization/user/get-by-user/${currentUserEmail}`,
      httpOptions
    );
  }

  getExternalAuthorizationsByManager(currentUserEmail: any) {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.get<any>(
      `${this.apiUrl}/api/external-authorization/manager/get-by-manager/${currentUserEmail}`,
      httpOptions
    );
  }

  editExternalAuthorization(
    externalAuthorizationId: number,
    externalAuthorizationRequest: ExternalAuthorizationRequest
  ) {
    const token = localStorage.getItem("token");
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`, // Add the token as a Bearer token
      }),
    };

    return this.http.put<any>(
      `${this.apiUrl}/api/external-authorization/update/${externalAuthorizationId}`,
      externalAuthorizationRequest,
      httpOptions
    );
  }
}
