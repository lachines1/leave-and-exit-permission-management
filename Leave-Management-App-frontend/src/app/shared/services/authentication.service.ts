import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { ToastrService } from "ngx-toastr";
import { User } from "../models/user.model";
import { Role } from "../models/role.enum";
import { AuthenticationRequest } from "../models/authentication-request.model";
import { RegisterRequest } from "../models/register-request.model";
import { AuthenticationResponse } from "../models/authentication-response.model";
import { VerificationRequest } from "../models/verification-request.model";
import { map } from "rxjs/operators";
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class AuthenticationService {
  //public
  public currentUser: Observable<User>;
  private baseUrl: string = `${environment.apiUrl}/api/v1/auth`;
  //private
  private currentUserSubject: BehaviorSubject<User>;

  /**
   *
   * @param {HttpClient} _http
   * @param {ToastrService} _toastrService
   */
  constructor(
    private _http: HttpClient,
    private _toastrService: ToastrService
  ) {
    this.currentUserSubject = new BehaviorSubject<User>(
      JSON.parse(localStorage.getItem("currentUser"))
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }

  // getter: currentUserValue
  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  /**
   *  Confirms if user is admin
   */
  get isAdmin() {
    return (
      this.currentUser && this.currentUserSubject.value.role === Role.Admin
    );
  }

  /**
   *  Confirms if user is client
   */
  get isClient() {
    return this.currentUser && this.currentUserSubject.value.role === Role.User;
  }

  /**
   * User login
   *
   * @returns user
   * @param authRequest
   */
  login(authRequest: AuthenticationRequest) {
    return this._http
      .post<any>(`${this.baseUrl}/authenticate`, authRequest)
      .pipe(
        map((user) => {
          // login successful if there's a jwt token in the response
          if (user && user.token) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem("currentUser", JSON.stringify(user));

            // Display welcome toast!
            setTimeout(() => {
              this._toastrService.success(
                "You have successfully logged in as an " +
                  user.role +
                  " user to Vuexy. Now you can start to explore. Enjoy! 🎉",
                "👋 Welcome, " + user.firstName + "!",
                {
                  toastClass: "toast ngx-toastr",
                  closeButton: true,
                  positionClass: "toast-top-right",
                }
              );
            }, 2500);

            // notify
            this.currentUserSubject.next(user);
          }

          return user;
        })
      );
  }

  /**
   * User logout
   *
   */
  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem("currentUser");
    localStorage.removeItem("token");
    // notify
    this.currentUserSubject.next(null);
  }

  register(registerRequest: RegisterRequest) {
    return this._http.post<AuthenticationResponse>(
      `${this.baseUrl}/register`,
      registerRequest
    );
  }

  verifyCode(verificationRequest: VerificationRequest) {
    return this._http.post<AuthenticationResponse>(
      `${this.baseUrl}/verify`,
      verificationRequest
    );
  }
}
