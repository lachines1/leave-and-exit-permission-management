import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, Observable} from "rxjs";
import {map} from "rxjs/operators";
import {Leave} from "../models/leave.model";

@Injectable({
  providedIn: 'root'
})
export class LeaveCalendarService {
  public events;
  public calendar;
  public currentEvent;
  public tempEvents;

  public onEventChange: BehaviorSubject<any>;
  public onCurrentEventChange: BehaviorSubject<any>;
  public onCalendarChange: BehaviorSubject<any>;

  constructor(private http: HttpClient) {}

  getCalendarEvents(): Observable<any[]> {
    return this.http.get<Leave[]>('/api/leaves').pipe(
        map(leaves => leaves.map(this.convertToFullCalendarEvent))
    );
  }

  public convertToFullCalendarEvent(leave: Leave): any {
    return {
      id: leave.id.toString(),
      title: leave.leaveType + ' : ' +  leave.user.firstName + ' ' + leave.user.lastName ,
      start: leave.startDate,
      end: leave.endDate,
      calendar: leave.status,
      color: this.getEventColor(leave),
      allDay: true // Assuming leave is all day; adjust as necessary
    };
  }

  private getEventColor(leave: Leave): string {
    switch (leave.status) {
      case 'ACCEPTED':
        return 'green';
      case 'PENDING':
        return 'orange';
      case 'REJECTED':
        return 'red';
      default:
        return 'blue'; // Default color if none of the above
    }
  }

  createNewEvent() {
    
  }

  calendarUpdate(calendars) {
    const calendarsChecked = calendars.filter(calendar => {
      return calendar.checked === true;
    });

    let calendarRef = [];
    calendarsChecked.map(res => {
      calendarRef.push(res.filter);
    });

    let filteredCalendar = this.tempEvents.filter(event => calendarRef.includes(event.calendar));
    this.events = filteredCalendar;
    this.onEventChange.next(this.events);
  }
}
