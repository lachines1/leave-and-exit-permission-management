export class OrganizationalUnitRequest {

     name?: string;
     teamNames?: string[];
     memberEmails?: string[];
     managerEmail?: string;
}
