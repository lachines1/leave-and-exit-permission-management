export class TeamExitPermissionRequest {

    leaveDuration?: string;
    date?: Date;
    teamName?: string;
    reason?: string;
}
