import {User} from "./user.model";

export interface ExternalAuthorization {
    id?: number
    leaveDuration?: string
    createdAt?: string
    startDate?: string
    endDate?: string
    status?: string
    user?: User
}

