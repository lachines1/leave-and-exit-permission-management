import {User} from "./user.model";
import {Team} from "./team.model";

export interface OrganizationalUnit {
    id: number
    createdAt: string
    name: string
    teams: Team[]
    members: User[]
    manager: User
}

