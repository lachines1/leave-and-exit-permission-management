export interface RegisterRequest {
    firstName?: string,
    lastName?: string,
    email?: string;
    password?: string;
    phone?: string;
    gender?: string;
    role?: string;
    mfaEnabled?: boolean;
}
