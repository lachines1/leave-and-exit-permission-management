import {Team} from "./team.model";

export interface TeamExitPermission {
    id?: number
    leaveDuration?: string
    createdAt?: string
    startDate?: string
    endDate?: string
    status?: string
    team?: Team
}