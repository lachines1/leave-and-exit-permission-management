export class TeamLeaveRequest {

     name?: string;
     startDate?: Date;
     endDate?: Date;
     reason?: string;
}
