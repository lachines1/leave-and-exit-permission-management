export class ExternalAuthorizationRequest {

     leaveDuration?: string;
     date?: Date;
     userEmail?: string;
     reason?: string;
}
