import {Role} from "./role.enum";
import {Team} from "./team.model";
import {OrganizationalUnit} from "./organizational-unit.model";

export class User {
    id?: number;
    email?: string;
    password?: string;
    firstName?: string;
    phone?: string;
    gender?: string;
    leaveDays?: number;
    onLeave?: boolean;
    externalActivitiesLimit?: number;
    lastName?: string;
    avatar?: string;
    team?: Team;
    organizationalUnit?: OrganizationalUnit;
    role?: Role;
    token?: string;

}
