import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';

import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

import {CoreConfigService} from '@core/services/config.service';
import {UserListService} from "../../../apps/user/user-list/user-list.service";
import {ToastrService} from "ngx-toastr";

@Component({
    selector: 'app-auth-forgot-password-v2',
    templateUrl: './auth-forgot-password-v2.component.html',
    styleUrls: ['./auth-forgot-password-v2.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AuthForgotPasswordV2Component implements OnInit {
    // Public
    public emailVar;
    public coreConfig: any;
    public forgotPasswordForm: UntypedFormGroup;
    public submitted = false;
    public email: string = '';

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {CoreConfigService} _coreConfigService
     * @param {FormBuilder} _formBuilder
     *
     * @param userService
     * @param toastrService
     */
    constructor(private _coreConfigService: CoreConfigService,
                private _formBuilder: UntypedFormBuilder,
                private userService: UserListService,
                private toastrService: ToastrService,) {
        this._unsubscribeAll = new Subject();

        // Configure the layout
        this._coreConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                menu: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                customizer: false,
                enableLocalStorage: false
            }
        };
    }

    // convenience getter for easy access to form fields
    get f() {
        return this.forgotPasswordForm.controls;
    }

    ngOnInit(): void {
        this.forgotPasswordForm = this._formBuilder.group({
            email: ['', [Validators.required, Validators.email]]
        });

        // Subscribe to config changes
        this._coreConfigService.config.pipe(takeUntil(this._unsubscribeAll)).subscribe(config => {
            this.coreConfig = config;
        });
    }

    sendEmail() {
        this.userService.sendResetPasswordEmail(this.email).subscribe(
            (res: any) => {
                this.toastrService.success('Email sent successfully', 'Success', {
                    progressBar: true,
                    progressAnimation: 'increasing',
                    closeButton: true,
                    positionClass: 'toast-top-right',
                    toastClass: 'toast ngx-toastr toast-success',
                    titleClass: 'toast-title',
                    messageClass: 'toast-message',
                    tapToDismiss: true,
                    timeOut: 5000
                });
            },
            (error: any) => {
                this.toastrService.error('Email not sent', 'Error', {
                    closeButton: true,
                    progressBar: true,
                    positionClass: 'toast-top-right',
                    toastClass: 'toast ngx-toastr animated ',
                    timeOut: 3000
                });
            }
        );

    }
}
