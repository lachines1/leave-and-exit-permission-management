import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';

import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

import {CoreConfigService} from '@core/services/config.service';
import {VerificationRequest} from "../../../../shared/models/verification-request.model";
import {Role} from "../../../../shared/models/role.enum";
import {RegisterRequest} from "../../../../shared/models/register-request.model";
import {AuthenticationResponse} from "../../../../shared/models/authentication-response.model";
import {AuthenticationService} from "../../../../auth/service";
import {Router} from "@angular/router";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
    selector: 'app-auth-register-v2',
    templateUrl: './auth-register-v2.component.html',
    styleUrls: ['./auth-register-v2.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AuthRegisterV2Component implements OnInit {
    // Public
    public coreConfig: any;
    public passwordTextType: boolean;
    public registerForm: FormGroup;
    public submitted = false;

    // Private
    private _unsubscribeAll: Subject<any>;
    @ViewChild('content') content: any;
    registerRequest: RegisterRequest = {};
    authResponse: AuthenticationResponse = {};
    message:string ="";
    otpCode: string = '';

    /**
     * Constructor
     *
     * @param {CoreConfigService} _coreConfigService
     * @param {FormBuilder} _formBuilder
     * @param authService
     * @param router
     * @param modalService
     */
    constructor(private _coreConfigService: CoreConfigService, private _formBuilder: FormBuilder, private authService: AuthenticationService, private router: Router,private modalService:NgbModal ) {
        this._unsubscribeAll = new Subject();

        // Configure the layout
        this._coreConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                menu: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                customizer: false,
                enableLocalStorage: false
            }
        };
    }

    // convenience getter for easy access to form fields
    get f() {
        return this.registerForm.controls;
    }
    close(id: string) {
        this.modalService.dismissAll(id);
    }
    openSignUpModal() {
        this.modalService.open(this.content, {
            size: 'lg',
            centered: true,
            windowClass: 'modal-warning'
        });
    }
    togglePasswordTextType() {
        this.passwordTextType = !this.passwordTextType;
    }


    onSubmit() {
        this.submitted = true;
        this.registerUser();
        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }
    }


    ngOnInit(): void {
        this.registerForm = this._formBuilder.group({
            username: ['', [Validators.required]],
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });

        // Subscribe to config changes
        this._coreConfigService.config.pipe(takeUntil(this._unsubscribeAll)).subscribe(config => {
            this.coreConfig = config;
        });
    }


    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    registerUser() {
        this.message = "";
        this.registerRequest.role = Role.Admin;
        this.authService.register(this.registerRequest)
            .subscribe({
                next: (response) => {
                    if (response) {
                        this.authResponse = response
                    } else {
                        // inform the user
                        this.message = 'Account created successfully\n You will be redirected to the login page in 3 seconds'
                        setTimeout(() => {
                            this.router.navigate(['login']);
                        }, 3000)
                    }
                    if(response.mfaEnabled){
                        this.openSignUpModal();
                    }
                },error:(err)=>{

                },
                complete:()=>{
                    if(!this.authResponse.mfaEnabled){
                        this.message = 'Account created successfully\n You will be redirected to the login page in 3 seconds'
                        setTimeout(() => {
                            this.router.navigate(['login']);
                        }, 3000)
                    }

                }
            });
    }

    verifyTfa() {
        this.message = '';
        const verifyRequest : VerificationRequest = {
            email : this.registerRequest.email,
            code : this.otpCode
        };
        this.authService.verifyCode(verifyRequest)
            .subscribe({
                next: (response) => {
                    this.message = 'Account created successfully\n You will be redirected to the welcome page in 3 seconds'
                },
                error: (err) => {
                    this.close('content');
                    window.location.reload();
                },
                complete: () => {
                    this.close('content');
                    this.router.navigate(['/pages/authentication/login-v2']);
                }
            });
    }
}
