import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';

import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

import {CoreConfigService} from '@core/services/config.service';
import {ActivatedRoute, Router} from "@angular/router";
import {UserListService} from "../../../apps/user/user-list/user-list.service";
import {ToastrService} from "ngx-toastr";

@Component({
    selector: 'app-auth-reset-password-v2',
    templateUrl: './auth-reset-password-v2.component.html',
    styleUrls: ['./auth-reset-password-v2.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AuthResetPasswordV2Component implements OnInit {
    // Public
    public coreConfig: any;
    public passwordTextType: boolean;
    public confPasswordTextType: boolean;
    public resetPasswordForm: UntypedFormGroup;
    public submitted = false;
    private resetToken: string = '';
    public confirmPassword: string = '';
    public newPassword: string = '';

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {CoreConfigService} _coreConfigService
     * @param {FormBuilder} _formBuilder
     * @param userService
     * @param toastrService
     * @param router
     * @param route
     */
    constructor(private _coreConfigService: CoreConfigService,
                private _formBuilder: UntypedFormBuilder,
                private userService: UserListService,
                private toastrService: ToastrService,
                private router: Router,
                private route: ActivatedRoute) {
        this._unsubscribeAll = new Subject();
        this.route.queryParams.subscribe(params => {
            this.resetToken = params['token'];
        });
        // Configure the layout
        this._coreConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                menu: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                customizer: false,
                enableLocalStorage: false
            }
        };
    }




    // convenience getter for easy access to form fields
    get f() {
        return this.resetPasswordForm.controls;
    }

    togglePasswordTextType() {
        this.passwordTextType = !this.passwordTextType;
    }

    toggleConfPasswordTextType() {
        this.confPasswordTextType = !this.confPasswordTextType;
    }

    ngOnInit(): void {
        this.resetPasswordForm = this._formBuilder.group({
            newPassword: ['', [Validators.required]],
            confirmPassword: ['', [Validators.required]]
        });

        // Subscribe to config changes
        this._coreConfigService.config.pipe(takeUntil(this._unsubscribeAll)).subscribe(config => {
            this.coreConfig = config;
        });
    }


    resetPassword() {
        if (this.newPassword !== this.confirmPassword) {
            return;
        } else {
            this.userService.resetForgottenPassword(this.resetToken, this.newPassword).subscribe(
                (res: any) => {
                    this.toastrService.success('Password reset successfully', 'Success', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx-toastr animated slideInUp text-white bg-success border-0',
                        positionClass: 'toast-top-right',
                        tapToDismiss: true,
                        titleClass: 'toast-title',
                        messageClass: 'toast-message',

                    })
                    this.router.navigate(['/pages/authentication/login-v2']);
                    console.log(res);
                },
                (error: any) => {
                    this.toastrService.error(error.error.message, 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        positionClass: 'toast-top-right',
                        tapToDismiss: true,
                        titleClass: 'toast-title',
                        messageClass: 'toast-message',
                        toastClass: 'toast ngx-toastr animated slideInUp text-white bg-danger border-0'
                    })
                    console.log(error);
                }
            );
        }
    }
}
