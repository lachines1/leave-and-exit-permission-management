import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ColumnMode} from "@swimlane/ngx-datatable";
import {TeamService} from "../../../../shared/services/team.service";
import {JwtHelperService} from "@auth0/angular-jwt";
import {ToastrService} from "ngx-toastr";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute} from "@angular/router";
import Swal from "sweetalert2";
import {UserListService} from "../../user/user-list/user-list.service";

@Component({
    selector: 'app-team-view',
    templateUrl: './team-view.component.html',
    styleUrls: ['./team-view.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class TeamViewComponent implements OnInit {
    data: any;
    ColumnMode = ColumnMode;
    @ViewChild('content') content: any;
    @ViewChild('tableRowDetails') tableRowDetails: any;
    selectedOption: number = 10;
    rows = [];
    email: string;
    users: any[];
    membersOnLeave: any[] = [];

    constructor(
        private teamService: TeamService,
        private jwtService: JwtHelperService,
        private userService: UserListService,
        private toastrService: ToastrService,
        private modalService: NgbModal,
        private route: ActivatedRoute // Inject ActivatedRoute
    ) {
    }

    ngOnInit(): void {
        this.getUsers();
        this.selectedOption = 10;
        this.email = '';

        // Access the ID from the route parameters
        this.route.params.subscribe(params => {
            const teamId = params['id'];

            // Fetch team based on the ID
            this.getData(teamId);
        });
    }

    rowDetailsToggleExpand(row) {
        this.tableRowDetails.rowDetail.toggleExpandRow(row);
    }

    filterUpdate($event: KeyboardEvent) {

    }

    close(id: string) {
        this.modalService.dismissAll(id);
    }

    openModal() {
        this.modalService.open(this.content, {
            size: 'md',
            centered: true,
            windowClass: 'modal modal-primary'
        });
    }

    affectUserToTeam() {
        this.teamService.addUserToTeam(this.email, this.data.name).subscribe(
            (data) => {
                this.toastrService.success('User added successfully', 'Success', {
                    progressBar: true,
                    progressAnimation: 'increasing',
                    closeButton: true,
                    positionClass: 'toast-top-right',
                    toastClass: 'toast ngx-toastr toast-success',
                    titleClass: 'toast-title',
                    messageClass: 'toast-message',
                    tapToDismiss: true,
                    timeOut: 5000
                });
                this.getData(this.data.id);
                this.close('addUserToTeam');
                this.email = '';
            },
            (err) => {
                if (err.error == "OK") {
                    this.toastrService.success('User added successfully', 'Success', {
                        progressBar: true,
                        progressAnimation: 'increasing',
                        closeButton: true,
                        positionClass: 'toast-top-right',
                        toastClass: 'toast ngx-toastr toast-success',
                        titleClass: 'toast-title',
                        messageClass: 'toast-message',
                        tapToDismiss: true,
                        timeOut: 5000
                    });
                    this.getData(this.data.id);
                    this.close('addUserToTeam');
                    this.email = '';
                } else {
                    this.toastrService.error('Error adding user to team: ' + err.error, 'Error', {
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx-toastr toast-error',
                        titleClass: 'toast-title',
                        messageClass: 'toast-message',
                        tapToDismiss: true,
                        timeOut: 5000,
                        progressAnimation: 'increasing',
                        positionClass: 'toast-top-right'
                    });
                }
            }
        );

    }

    removeFromTeam(email: string) {
        Swal.fire({
            title: 'Confirm',
            text: 'Are you sure you want to delete this item? You will not be able to recover it!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-danger ml-1'
            }
        }).then((result) => {
            if (result.isConfirmed) {
                this.teamService.removeUserFromTeam(email, this.data.name).subscribe(
                    (data) => {
                        this.toastrService.success('User removed successfully', 'Success', {

                            progressBar: true,
                            progressAnimation: 'increasing',
                            closeButton: true,
                            positionClass: 'toast-top-right',
                            toastClass: 'toast ngx-toastr toast-success',
                            titleClass: 'toast-title',
                            messageClass: 'toast-message',
                            tapToDismiss: true,
                            timeOut: 5000
                        });
                        this.getData(this.data.id);
                    },
                    (err) => {
                        if (err.error == "OK") {
                            this.toastrService.success('User removed successfully', 'Success', {
                                progressBar: true,
                                progressAnimation: 'increasing',
                                closeButton: true,
                                positionClass: 'toast-top-right',
                                toastClass: 'toast ngx-toastr toast-success',
                                titleClass: 'toast-title',
                                messageClass: 'toast-message',
                                tapToDismiss: true,
                                timeOut: 5000
                            });
                            this.getData(this.data.id);
                        } else {
                            this.toastrService.error('Failed to remove user from team: ' + err.error, 'Error', {
                                closeButton: true,
                                progressBar: true,
                                toastClass: 'toast ngx-toastr toast-error',
                                titleClass: 'toast-title',
                                messageClass: 'toast-message',
                                tapToDismiss: true,
                                timeOut: 5000,
                                progressAnimation: 'increasing',
                                positionClass: 'toast-top-right'
                            });
                        }
                    }
                )
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal.fire(
                    'Cancelled',
                    'User is safe :)',
                    'error'
                )
            }
        })
    }

    onUserChange(selectedItem: any) {
        this.email = selectedItem.email;
    }

    getUsers() {
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtService.decodeToken(token).email;
        const role = this.jwtService.decodeToken(token).role;
        if (role === 'ADMIN') {
            this.userService.getAllUsers().subscribe(
                (data) => {
                    this.users = data.filter((user) => user.role === 'USER');
                },
                (error) => {
                    if (error.error == "OK") {
                        this.toastrService.success('Data Fetch Successful', 'Success', {
                            progressBar: true,
                            progressAnimation: 'increasing',
                            closeButton: true,
                            positionClass: 'toast-top-right',
                            toastClass: 'toast ngx-toastr toast-success',
                            titleClass: 'toast-title',
                            messageClass: 'toast-message',
                            tapToDismiss: true,
                            timeOut: 5000
                        });
                        this.getData(this.data.id);
                    } else {
                        this.toastrService.error('Error fetching users', 'Error', {
                            progressBar: true,
                            progressAnimation: 'increasing',
                            closeButton: true,
                            positionClass: 'toast-top-right',
                            toastClass: 'toast ngx-toastr toast-error',
                            titleClass: 'toast-title',
                            messageClass: 'toast-message',
                            tapToDismiss: true,
                            timeOut: 5000
                        });
                    }
                });
        } else if (role === 'MANAGER') {
            this.userService.getUsersByManager(currentUserEmail).subscribe(
                (data) => {
                    this.users = data;
                },
                (error) => {
                    this.toastrService.error('Error fetching users', 'Error', {
                        progressBar: true,
                        progressAnimation: 'increasing',
                        closeButton: true,
                        positionClass: 'toast-top-right',
                        toastClass: 'toast ngx-toastr toast-error',
                        titleClass: 'toast-title',
                        messageClass: 'toast-message',
                        tapToDismiss: true,
                        timeOut: 5000
                    });
                });
        } else {
            this.userService.getUserByEmail(currentUserEmail).subscribe(
                (data) => {
                    this.users = [data];
                },
                (error) => {
                    this.toastrService.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx'
                    });
                });
        }
    }

    private getData(teamId) {
        this.teamService.getTeamById(teamId).subscribe((res: any) => {
            this.data = res;
            this.rows = res.members;
            this.membersOnLeave = res.members.filter((member) => member.onLeave === true);
        });
    }

}
