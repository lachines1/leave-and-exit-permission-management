import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";
import {TeamService} from "../../../../shared/services/team.service";
import {TeamRequest} from "../../../../shared/models/team-request.model";
import {OrganizationalUnitService} from "../../../../shared/services/organizational-unit.service";
import {UserListService} from "../../user/user-list/user-list.service";
import {JwtHelperService} from "@auth0/angular-jwt";
import {User} from "../../../../auth/models";
import {OrganizationalUnit} from "../../../../shared/models/organizational-unit.model";

@Component({
    selector: 'app-team-add',
    templateUrl: './team-add.component.html',
    styleUrls: ['./team-add.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class TeamAddComponent implements OnInit {

    teamName: string;
    teamDescription: string;
    teamMembers: any;
    teamLeadEmail: string;
    organizationalUnitName: string;
    contentHeader: object;
    managers: any[];
    departments: OrganizationalUnit[];
    selectUser: User[];

    constructor(private teamService: TeamService,
                private organizationUnitService: OrganizationalUnitService,
                private userService: UserListService,
                private router: Router,
                private jwtService: JwtHelperService,
                private toastr: ToastrService) {
    }

    ngOnInit(): void {
        this.organizationalUnitName = "";
        this.getUsers();
        this.getDepartments();
        this.contentHeader = {
            headerTitle: 'Create Team',
            actionButton: true,
            breadcrumb: {
                type: '',
                links: [
                    {
                        name: 'Home',
                        isLink: true,
                        link: '/'
                    },
                    {
                        name: 'Teams',
                        isLink: true,
                        link: '/apps/teams/list'
                    },
                    {
                        name: 'Create Team',
                        isLink: false,
                        link: '/'
                    }

                ]
            }
        };
    }

    toastrConfig() {
        return {
            timeOut: 3000,          // Display time (in milliseconds)
            closeButton: true,      // Show close button
            progressBar: true,      // Show progress bar
            toastClass: 'toast ngx-toastr',  // Custom class for styling
            positionClass: 'toast-top-right', // Toast position on the screen (you can change it based on your preference)
        };
    }

    addTeam() {
        let teamRequest: TeamRequest = new TeamRequest();
        teamRequest.name = this.teamName;
        teamRequest.description = this.teamDescription;
        teamRequest.teamLeadEmail = this.teamLeadEmail;
        teamRequest.organizationalUnitName = this.organizationalUnitName;

        console.log(teamRequest);
        this.teamService.createTeam(teamRequest).subscribe(
            (data) => {
                this.toastr.success('Team added successfully', 'Success', {
                    timeOut: 3000,
                    closeButton: true,
                    progressBar: true,
                    toastClass: 'toast ngx-toastr',
                });
                this.router.navigate(['/apps/teams/list']);
            },
            (error) => {
                console.log(error);
                if (error.error === "OK" && error.status === 200) {
                    this.toastr.success('Team added successfully', 'Success', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx-toastr',
                    });
                    this.router.navigate(['/apps/teams/list']);
                } else {
                    this.toastr.error('Error adding team. \nPlease make sure you fill all the fields.', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx-toastr',
                    });
                }
            }
        );

    }

    getUsers() {
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtService.decodeToken(token).email;
        const role = this.jwtService.decodeToken(token).role;
        if (role === 'ADMIN') {
            this.userService.getAllUsers().subscribe(
                (data) => {
                    this.selectUser = data.filter(user => user.role === 'MANAGER');
                },
                (error) => {
                    this.toastr.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx'
                    });
                });
        } else if (role === 'MANAGER') {
            this.userService.getUsersByManager(currentUserEmail).subscribe(
                (data) => {
                    this.selectUser = data;
                },
                (error) => {
                    this.toastr.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx'
                    });
                });
        } else {
            this.userService.getUserByEmail(currentUserEmail).subscribe(
                (data) => {
                    this.selectUser = [data];
                },
                (error) => {
                    this.toastr.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx'
                    });
                });
        }
    }

    getDepartments() {
        this.organizationUnitService.getAllOrganizationalUnits().subscribe(
            (data) => {
                this.departments = data;
            },
            (error) => {
                this.toastr.error('Error fetching departments', 'Error', {
                    timeOut: 3000,
                    closeButton: true,
                    progressBar: true,
                    toastClass: 'toast ngx-toastr',
                });
            });
    }

    onUserEmailChange(selectedItem: any) {
        this.teamLeadEmail = selectedItem.email;
    }

    onDepartmentChange(selectedItem: any) {
        this.organizationalUnitName = selectedItem.name;
    }
}
