import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ColumnMode} from "@swimlane/ngx-datatable";
import {TeamService} from "../../../../shared/services/team.service";
import {JwtHelperService} from "@auth0/angular-jwt";
import {Team} from "../../../../shared/models/team.model";
import {ToastrService} from "ngx-toastr";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import Swal from "sweetalert2";

@Component({
    selector: 'app-team-list',
    templateUrl: './team-list.component.html',
    styleUrls: ['./team-list.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class TeamListComponent implements OnInit {
    public contentHeader: object;
    basicSelectedOption: number = 10;
    innerSelectedOption: number = 3;
    public rows: any;
    @ViewChild('content') content: any;
    data: Team[] = [];
    public ColumnMode = ColumnMode;
    private tempFilterData: any;

    constructor(private teamService: TeamService, private jwtService: JwtHelperService, private toastrService: ToastrService, private modalService: NgbModal) {
    }

    ngOnInit(): void {
        this.contentHeader = {
            headerTitle: 'Teams List',
            actionButton: true,
            breadcrumb: {
                type: '',
                links: [
                    {
                        name: 'Home',
                        isLink: true,
                        link: '/'
                    },
                    {
                        name: 'Teams',
                        isLink: true,
                        link: 'apps/teams/list'
                    },
                    {
                        name: 'Teams List',
                        isLink: false
                    }
                ]
            }
        };
        this.getTeams();
    }

    filterUpdate($event: KeyboardEvent) {

    }

    getTeams() {
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtService.decodeToken(token).email;
        const currentUserRole = this.jwtService.decodeToken(token).role;
        if (currentUserRole === 'MANAGER') {
            this.teamService.getTeamsByManager(currentUserEmail).subscribe(
                (response) => {
                    this.data = response;
                    this.rows = this.data;
                },
                (error) => {
                    console.log(error);
                }
            )
        } else if (currentUserRole === 'USER') {
            this.teamService.getTeamByUser(currentUserEmail).subscribe(
                (response) => {
                    this.data = response;
                    this.rows = this.data;
                },
                (error) => {
                    console.log(error);
                }
            )
        } else {
            this.teamService.getAllTeams().subscribe(
                (response) => {
                    this.data = response;
                    this.rows = this.data;
                },
                (error) => {
                    console.log(error);
                }
            )
        }
    }

    deleteTeam(id: number) {
       Swal.fire({
           title: 'Confirm',
           text: 'Are you sure you want to delete this item? You will not be able to recover it!',
           icon: 'warning',
           showCancelButton: true,
           confirmButtonColor: '#3085d6',
           cancelButtonColor: '#d33',
           confirmButtonText: 'Yes, delete it!',
           customClass: {
               confirmButton: 'btn btn-primary',
               cancelButton: 'btn btn-danger ml-1'
           }
        }).then((result) => {
            if (result.value) {
                this.teamService.deleteTeamById(id).subscribe(
                    (response) => {
                        Swal.fire({
                            title: 'Deleted!',
                            text: 'Your file has been deleted.',
                            icon: 'success',
                            customClass: {
                                confirmButton: 'btn btn-success'
                            }
                        });
                        this.getTeams();
                    },
                    (error) => {
                        Swal.fire(
                            'Error!',
                            'Team could not be deleted.',
                            'error',

                        );
                    }
                )
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal.fire({
                    title: 'Cancelled',
                    text: 'The Leave Request was not deleted.',
                    icon: 'error',
                    customClass: {
                        confirmButton: 'btn btn-success'
                    }
                });
            }
        } )

    }

    close(id: string) {
        this.modalService.dismissAll(id);
    }
    openModal() {
        this.modalService.open(this.content, {
            size: 'md',
            centered: true,
            windowClass: 'modal modal-warning'
        });
    }

    // affectUserToTeam() {
    //     const token = localStorage.getItem('token');
    //     const currentUserEmail = this.jwtService.decodeToken(token).email;
    //     const currentUserRole = this.jwtService.decodeToken(token).role;
    //     if (currentUserRole === 'ADMIN') {
    //         this.teamService.(this.teamService.teamId, this.teamService.userId).subscribe(
    //             (response) => {
    //                 this.toastrService.success('User affected to team successfully', 'Success', {
    //                     progressBar: true,
    //                     progressAnimation: 'increasing',
    //                     closeButton: true,
    //                     positionClass: 'toast-top-right'
    //                 });
    //                 this.getTeams();
    //             },
    //             (error) => {
    //                 this.toastrService.error('User could not be affected to team', 'Error', {
    //                     progressBar: true,
    //                     progressAnimation: 'increasing',
    //                     closeButton: true,
    //                     positionClass: 'toast-top-right'
    //                 });
    //                 console.log(error);
    //             }
    //         )
    //     } else {
    //         this.teamService.affectUserToTeam(this.teamService.teamId, this.jwtService.decodeToken(token).id).subscribe(
    //             (response) => {
    //                 this.toastrService.success('User affected to team successfully', 'Success', {
    //                     progressBar: true,
    //                     progressAnimation: 'increasing',
    //                     closeButton: true,
    //                     positionClass: 'toast-top-right'
    //                 });
    //                 this.getTeams();
    //             },
    //             (error) => {
    //                 this.toastrService.error('User could not be affected to team', 'Error', {
    //                     progressBar: true,
    //                     progressAnimation: 'increasing',
    //                     closeButton: true,
    //                     positionClass: 'toast-top-right'
    //                 });
    //                 console.log(error);
    //             }
    //         )
    //     }
    // }
}
