import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TeamListComponent} from './team-list/team-list.component';
import {TeamAddComponent} from './team-add/team-add.component';
import {TeamViewComponent} from './team-view/team-view.component';
import {CardSnippetModule} from "../../../../@core/components/card-snippet/card-snippet.module";
import {ContentHeaderModule} from "../../../layout/components/content-header/content-header.module";
import {FormsModule} from "@angular/forms";
import {NgSelectModule} from "@ng-select/ng-select";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {RouterModule} from "@angular/router";
import {CoreDirectivesModule} from "../../../../@core/directives/directives";
import {CsvModule} from "@ctrl/ngx-csv";
import {NgbDropdownModule} from "@ng-bootstrap/ng-bootstrap";
import {CoreCardModule} from "../../../../@core/components/core-card/core-card.module";
import { TeamEditComponent } from './team-edit/team-edit.component';

const routes = [
    {
        path: 'list',
        component: TeamListComponent
    },
    {
        path: 'add',
        component: TeamAddComponent
    },
    {
        path: 'view/:id',
        component: TeamViewComponent
    },
    {
        path: 'edit/:id',
        component: TeamEditComponent
    },
]

@NgModule({
    declarations: [
        TeamListComponent,
        TeamAddComponent,
        TeamViewComponent,
        TeamEditComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        CardSnippetModule,
        ContentHeaderModule,
        CoreDirectivesModule,
        CsvModule,
        FormsModule,
        NgSelectModule,
        NgxDatatableModule,
        NgbDropdownModule,
        CoreCardModule,
    ]
})
export class TeamsModule {
}
