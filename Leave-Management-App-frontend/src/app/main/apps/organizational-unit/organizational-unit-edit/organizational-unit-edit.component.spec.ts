import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationalUnitEditComponent } from './organizational-unit-edit.component';

describe('OrganizationalUnitEditComponent', () => {
  let component: OrganizationalUnitEditComponent;
  let fixture: ComponentFixture<OrganizationalUnitEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrganizationalUnitEditComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrganizationalUnitEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
