import {Component, OnInit} from '@angular/core';
import {ToastrService} from "ngx-toastr";
import {OrganizationalUnitService} from "../../../../shared/services/organizational-unit.service";
import {ActivatedRoute, Router} from "@angular/router";
import {JwtHelperService} from "@auth0/angular-jwt";
import {OrganizationalUnitRequest} from "../../../../shared/models/organizational-unit-request.model";
import {UserListService} from "../../user/user-list/user-list.service";
import {User} from "../../../../auth/models";

@Component({
    selector: 'app-organizational-unit-edit',
    templateUrl: './organizational-unit-edit.component.html',
    styleUrls: ['./organizational-unit-edit.component.scss']
})
export class OrganizationalUnitEditComponent implements OnInit {
    unitName: string;
    teamNames: string[];
    memberEmails: string[];
    managerEmail: string;
    contentHeader: object;
    public organizationalUnitRequest: OrganizationalUnitRequest = {};
    selectUser: User[];
    private organizationalUnitId: number;

    constructor(
        private toastr: ToastrService,
        private organizationalUnitService: OrganizationalUnitService,
        private userService: UserListService,
        private router: Router,
        private route: ActivatedRoute,
        private jwtService: JwtHelperService) {
        this.route.params.subscribe(params => {
            this.organizationalUnitId = params['id'];
            this.getOrganizationalUnitById(this.organizationalUnitId);
        });
    }

    ngOnInit(): void {
        this.getUsers();
        this.contentHeader = {
            headerTitle: 'Edit Department',
            actionButton: true,
            breadcrumb: {
                type: '',
                links: [
                    {
                        name: 'Home',
                        isLink: true,
                        link: '/'
                    },
                    {
                        name: 'Departments',
                        isLink: true,
                        link: '/apps/organizational-unit/list'
                    },
                    {
                        name: 'Edit Department',
                        isLink: false,
                        link: '/'
                    }

                ]
            }
        };
    }


    editOrganizationalUnit() {
        this.route.params.subscribe(params => {
            this.organizationalUnitId = params['id'];
            this.getOrganizationalUnitById(this.organizationalUnitId);
        });
        this.organizationalUnitRequest.name = this.unitName;
        this.organizationalUnitRequest.teamNames = this.teamNames;
        this.organizationalUnitRequest.memberEmails = this.memberEmails;
        this.organizationalUnitRequest.managerEmail = this.managerEmail;
        this.organizationalUnitService.updateOrganizationalUnit(this.organizationalUnitId, this.organizationalUnitRequest).subscribe(
            res => {
                this.toastr.success('Department updated successfully');
                this.router.navigate(['/apps/organizational-unit/list']);
            },
            err => {
                console.log(err);
            }
        );
    }

    getUsers() {
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtService.decodeToken(token).email;
        const role = this.jwtService.decodeToken(token).role;
        if (role === 'ADMIN') {
            this.userService.getAllUsers().subscribe(
                (data) => {
                    this.selectUser = data.filter(user => user.role === 'ADMIN');
                },
                (error) => {
                    this.toastr.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx'
                    });
                });
        } else if (role === 'MANAGER') {
            this.userService.getUsersByManager(currentUserEmail).subscribe(
                (data) => {
                    this.selectUser = data.filter(user => user.role === 'ADMIN');
                },
                (error) => {
                    this.toastr.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx'
                    });
                });
        } else {
            this.userService.getUserByEmail(currentUserEmail).subscribe(
                (data) => {
                    this.selectUser = [data];
                },
                (error) => {
                    this.toastr.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx'
                    });
                });
        }
    }

    onUserSelect(selectedItem: any) {
        this.managerEmail = selectedItem.email;
    }

    private getOrganizationalUnitById(organizationalUnitId: number) {
        this.organizationalUnitService.getOrganizationalUnitById(organizationalUnitId).subscribe(
            (data) => {
                this.organizationalUnitRequest = data;
                this.unitName = this.organizationalUnitRequest.name;
                this.teamNames = this.organizationalUnitRequest.teamNames;
                this.memberEmails = this.organizationalUnitRequest.memberEmails;
                this.managerEmail = this.organizationalUnitRequest.managerEmail;
            },
            (error) => {
                this.toastr.error('Error fetching department', 'Error', {
                    timeOut: 3000,
                    closeButton: true,
                    progressBar: true,
                    toastClass: 'toast ngx-toastr'
                });
            });
    }
}
