import {ComponentFixture, TestBed} from '@angular/core/testing';

import {OrganizationalUnitListComponent} from './organizational-unit-list.component';

describe('OrganizationalUnitListComponent', () => {
    let component: OrganizationalUnitListComponent;
    let fixture: ComponentFixture<OrganizationalUnitListComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [OrganizationalUnitListComponent]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(OrganizationalUnitListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
