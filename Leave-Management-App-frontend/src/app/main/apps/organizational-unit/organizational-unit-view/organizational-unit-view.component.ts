import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ColumnMode, DatatableComponent} from "@swimlane/ngx-datatable";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {OrganizationalUnitService} from "../../../../shared/services/organizational-unit.service";
import {ActivatedRoute} from "@angular/router";
import Swal from "sweetalert2";
import {OrganizationalUnit} from "../../../../shared/models/organizational-unit.model";
import {UserListService} from "../../user/user-list/user-list.service";
import {JwtHelperService} from "@auth0/angular-jwt";
import {ToastrService} from "ngx-toastr";
import {TeamService} from "../../../../shared/services/team.service";

@Component({
    selector: 'app-organizational-unit-view',
    templateUrl: './organizational-unit-view.component.html',
    styleUrls: ['./organizational-unit-view.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class OrganizationalUnitViewComponent implements OnInit {
    data: OrganizationalUnit;
    ColumnMode = ColumnMode;
    @ViewChild('contentUser') contentUser: any;
    @ViewChild('contentTeam') contentTeam: any;
    @ViewChild('tableRowDetails') tableRowDetails: any;
    selectedOption: number = 10;
    members = [];
    teams: any[] = [];
    teamsSelectedOption: number = 10;
    email: string
    teamName: string
    @ViewChild(DatatableComponent) table: DatatableComponent;
    allTeams: any[];
    users: any[];
    protected organizationalUnitId: number;

    constructor(
        private organizationalUnitService: OrganizationalUnitService,
        private modalService: NgbModal,
        private userService: UserListService,
        private teamService: TeamService,
        private jwtService: JwtHelperService,
        private toastrService: ToastrService,
        private route: ActivatedRoute // Inject ActivatedRoute
    ) {
        this.route.params.subscribe(params => {
            this.organizationalUnitId = params['id'];
            console.log("Department id", this.organizationalUnitId);
        });

    }

    ngOnInit(): void {
        this.teamName = '';
        this.selectedOption = 10;
        this.getDetails(this.organizationalUnitId);
        this.getUsers();
        this.getTeams();
    }

    getDetails(id: number) {
        this.organizationalUnitService.getOrganizationalUnitById(id).subscribe(
            (res: OrganizationalUnit) => {
                this.data = res;
                console.log("Department details", this.data);
                console.log("Department members", this.data.members);
                console.log("Department teams", this.data.teams);
                this.members = this.data.members;
                this.teams = this.data.teams;
            },
            (error) => {
                console.log(error);
            }
        );

    }

    filterUpdate($event: KeyboardEvent) {
        const val = ($event.target as HTMLInputElement).value.toLowerCase();
        this.members = this.data.members.filter(function (d) {
            return d.email.toLowerCase().indexOf(val) !== -1 || !val;
        });
    }

    rowDetailsToggleExpand(row: any) {
        this.tableRowDetails.rowDetail.toggleExpandRow(row);
    }

    close(id: string) {
        this.modalService.dismissAll(id);
    }

    openUserModal() {
        this.modalService.open(this.contentUser, {
            size: 'md',
            centered: true,
            animation: true,
            windowClass: 'modal modal-primary'
        });
    }

    openTeamModal() {
        this.modalService.open(this.contentTeam, {
            size: 'md',
            centered: true,
            animation: true,
            windowClass: 'modal modal-primary'
        });
    }

    removeUserFromDepartment(email: string) {
        Swal.fire({
            title: 'Are you sure?',
            text: 'You will remove this user from the department',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, remove it!',
            cancelButtonText: 'No, keep it',
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-danger ml-1'
            }
        }).then((result) => {
            if (result.value) {
                this.organizationalUnitService.removeMemberFromOrganizationalUnit(this.data.id, email).subscribe(
                    (response) => {
                        Swal.fire(
                            'Removed!',
                            'User has been removed from the department.',
                            'success',
                        );
                        this.getDetails(this.data.id);
                    },
                    (error) => {
                        console.log(error);
                    }
                );
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal.fire(
                    'Cancelled',
                    'User is safe :)',
                    'error'
                );
            }
        });

    }

    removeTeamFromDepartment(teamName: string) {
        Swal.fire({
            title: 'Are you sure?',
            text: 'You will remove this team from the department',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, remove it!',
            cancelButtonText: 'No, keep it',
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-danger ml-1'
            }
        }).then((result) => {
            if (result.value) {
                this.organizationalUnitService.removeTeamFromOrganizationalUnit(this.data.id, teamName).subscribe(
                    (response) => {
                        Swal.fire(
                            'Removed!',
                            'Team has been removed from the department.',
                            'success'
                        );
                        this.getDetails(this.data.id);
                    },
                    (error) => {
                        console.log(error);
                    }
                );
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal.fire(
                    'Cancelled',
                    'Team is safe :)',
                    'error'
                );
            }
        });

    }

    affectUserToDepartment() {
        this.organizationalUnitService.affectMemberToOrganizationalUnit(this.data.id, this.email).subscribe(
            (response) => {
                Swal.fire(
                    'Added!',
                    'User has been added to the department.',
                    'success'
                );
                this.getDetails(this.data.id);
            },
            (error) => {
                console.log(error);
            }
        );
    }

    affectTeamToDepartment(email: string) {
        this.organizationalUnitService.affectTeamToOrganizationalUnit(this.data.id, email).subscribe(
            (response) => {
                Swal.fire(
                    'Added!',
                    'Team has been added to the department.',
                    'success'
                );
                this.getDetails(this.data.id);
            },
            (error) => {
                console.log(error);
            }
        );
    }

    onUserChange(selectedItem: any) {
        this.email = selectedItem.email;
    }

    onTeamChange(selectedItem: any) {
        this.teamName = selectedItem.value;
        console.log('Selected Team:', this.teamName);
    }

    getUsers() {
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtService.decodeToken(token).email;
        const role = this.jwtService.decodeToken(token).role;
        if (role === 'ADMIN') {
            this.userService.getAllUsers().subscribe(
                (data) => {
                    this.users = data;
                },
                (error) => {
                    this.toastrService.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx-toast error'
                    });
                });
        } else if (role === 'MANAGER') {
            this.userService.getUsersByManager(currentUserEmail).subscribe(
                (data) => {
                    this.users = data;
                },
                (error) => {
                    this.toastrService.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx-toast error'
                    });
                });
        } else {
            this.userService.getUserByEmail(currentUserEmail).subscribe(
                (data) => {
                    this.users = [data];
                },
                (error) => {
                    this.toastrService.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx'
                    });
                });
        }
    }

    getTeams() {
        this.teamService.getAllTeams().subscribe(
            (data) => {
                this.allTeams = data;
                this.allTeams = data.map(team => {
                    // Add a new attribute 'value' with the same value as 'name'
                    return {...team, value: team.name};
                });
            },
            (error) => {
                this.toastrService.error('Error fetching teams', 'Error', {
                    timeOut: 3000,
                    closeButton: true,
                    progressBar: true,
                    toastClass: 'toast ngx-toastr',
                });
            });
    }

}
