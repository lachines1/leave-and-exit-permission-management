import {ComponentFixture, TestBed} from '@angular/core/testing';

import {OrganizationalUnitViewComponent} from './organizational-unit-view.component';

describe('OrganizationalUnitViewComponent', () => {
    let component: OrganizationalUnitViewComponent;
    let fixture: ComponentFixture<OrganizationalUnitViewComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [OrganizationalUnitViewComponent]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(OrganizationalUnitViewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
