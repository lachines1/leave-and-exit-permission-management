import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {LeaveService} from "../../../../shared/services/leave.service";
import {JwtHelperService} from "@auth0/angular-jwt";
import {AuthenticationService} from "../../../../auth/service";
import {LeaveRequest} from "../../../../shared/models/leave-request.model";
import {User} from "../../../../auth/models";
import {UserListService} from "../../user/user-list/user-list.service";

@Component({
    selector: 'app-leave-edit',
    templateUrl: './leave-edit.component.html',
    styleUrls: ['./leave-edit.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class LeaveEditComponent implements OnInit {

    public leaveRequest: LeaveRequest = {};
    public contentHeader: object;
    userEmail: string;
    leaveType: string;
    leaveTypeADMIN: string;
    currentUser
    role
    exceptionalLeaveType: string;
    timeOfDay: string;
    startDate: Date;
    endDate: Date;
    public leaveTypes = [
        {name: 'Personal Leave', value: 'PERSONAL_LEAVE'},
        {name: 'Exceptional leave', value: 'EXCEPTIONAL_LEAVE'},
        {name: 'Half Day', value: 'HALF_DAY'},
    ];
    public leaveTypesADMIN = [
        {name: 'Personal Leave', value: 'PERSONAL_LEAVE'},
        {name: 'Exceptional leave', value: 'EXCEPTIONAL_LEAVE'},
        {name: 'Sick Leave', value: 'SICK_LEAVE'},
        {name: 'Half Day', value: 'HALF_DAY'},
    ];

    public timesOfDay = [
        {name: 'Morning', value: 'MORNING'},
        {name: 'Afternoon', value: 'AFTERNOON'},
        {name: 'Inapplicable', value: 'INAPPLICABLE'},
    ];

    public exceptionalLeaveTypes = [
        {name: 'Maternity Leave', value: 'MATERNITY_LEAVE'},
        {name: 'Paternity Leave', value: 'PATERNITY_LEAVE'},
        {name: 'Parent Death Leave', value: 'PARENT_DEATH_LEAVE'},
        {name: 'Marriage Leave', value: 'MARRIAGE_LEAVE'},
        {name: 'Bereavement Leave', value: 'BEREAVEMENT_LEAVE'},
        {name: 'Child Birth Leave', value: 'CHILD_BIRTH_LEAVE'},
        {name: 'Child Marriage Leave', value: 'CHILD_MARRIAGE_LEAVE'},
        {name: 'Child Death Leave', value: 'CHILD_DEATH_LEAVE'},
        {name: 'None', value: 'NONE'}
    ];
    isAdmin: boolean
    isClient: boolean
    selectUser: User[];
    private leaveId: number;

    constructor(private router: Router,
                private toastr: ToastrService,
                private leaveService: LeaveService,
                private jwtService: JwtHelperService,
                private userService: UserListService,
                private _authenticationService: AuthenticationService,
                private route: ActivatedRoute) {

        this._authenticationService.currentUser.subscribe(x => (this.currentUser = x));
        this.isAdmin = this._authenticationService.isAdmin;
        this.isClient = this._authenticationService.isClient;
        this.route.params.subscribe(params => {
            this.leaveId = params['id'];
            this.getLeaveRequestById(this.leaveId);
        });
    }

    public getFilteredLeaveTypes() {
        if (this.role === 'ADMIN') {
            return this.leaveTypes;
        } else {
            return this.leaveTypes.filter(leaveType => leaveType.value !== 'SICK_LEAVE');
        }
    }

    ngOnInit(): void {
        this.getUsers();
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'))
        this.role = this.currentUser.role
        this.userEmail = this.currentUser.email
        console.log("user role", this.role)
        this._authenticationService.currentUser.subscribe(x => (this.currentUser = x));
        this.isAdmin = this._authenticationService.isAdmin;
        this.isClient = this._authenticationService.isClient;
        this.contentHeader = {
            headerTitle: 'Edit Leave Request',
            actionButton: true,
            breadcrumb: {
                type: '',
                links: [
                    {
                        name: 'Home',
                        isLink: true,
                        link: '/'
                    },
                    {
                        name: 'Leave Requests',
                        isLink: true,
                        link: '/apps/leave/list'
                    },
                    {
                        name: 'Edit Leave Request',
                        isLink: false,
                        link: '/'
                    }

                ]
            }
        };

    }

    editLeave() {
        this.route.params.subscribe(params => {
            this.leaveId = params['id'];
            this.getLeaveRequestById(this.leaveId);
        });
        this.leaveRequest.startDate = this.startDate;
        this.leaveRequest.endDate = this.endDate;
        this.leaveRequest.leaveType = this.leaveType;

        if (this.role === "ADMIN") {
            this.leaveRequest.leaveType = this.leaveTypeADMIN;
        } else {
            this.leaveRequest.leaveType = this.leaveType;
        }

        this.leaveRequest.exceptionalLeaveType = this.exceptionalLeaveType;
        this.leaveRequest.timeOfDay = this.timeOfDay;
        this.leaveRequest.userEmail = this.userEmail;

        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtService.decodeToken(token).email;
        this.leaveService.updateLeaveRequest(this.leaveId, this.leaveRequest).subscribe(
            (data) => {
                this.toastr.success('Leave Request added successfully', 'Success', {
                    timeOut: 3000,
                    closeButton: true,
                    progressBar: true,
                    toastClass: 'toast ngx-toastr',
                });
                this.router.navigate(['/apps/leave/list']);
            },
            (error) => {
                this.toastr.warning('You must fill all the fields before submitting', 'Warning', {
                    timeOut: 3000,
                    closeButton: true,
                    progressBar: true,
                    toastClass: 'toast ngx-toastr toast-warning',
                    
                });
            }
        );
    }

    onExceptionalLeaveTypeChange(selectedItem: any) {
        this.exceptionalLeaveType = selectedItem.value;
    }

    onTimeOfDayChange(selectedItem: any) {
        this.timeOfDay = selectedItem.value;
    }

    onLeaveTypeChange(selectedItem: any) {
        if (this.role === "ADMIN") {
            this.leaveTypeADMIN = selectedItem.value;
        } else {
            this.leaveType = selectedItem.value;
        }
        console.log(this.leaveType)
    }

    getUsers() {
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtService.decodeToken(token).email;
        const role = this.jwtService.decodeToken(token).role;
        if (role === 'ADMIN') {
            this.userService.getAllUsers().subscribe(
                (data) => {
                    this.selectUser = data;
                },
                (error) => {
                    this.toastr.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx'
                    });
                });
        } else if (role === 'MANAGER') {
            this.userService.getUsersByManager(currentUserEmail).subscribe(
                (data) => {
                    this.selectUser = data;
                },
                (error) => {
                    this.toastr.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx'
                    });
                });
        } else {
            this.userService.getUserByEmail(currentUserEmail).subscribe(
                (data) => {
                    this.selectUser = [data];
                },
                (error) => {
                    this.toastr.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx'
                    });
                });
        }
    }

    onUserEmailChange(selectedItem: any) {
        this.userEmail = selectedItem.email;

    }

    private getLeaveRequestById(leaveId: number) {
        this.leaveService.getLeaveRequestById(leaveId).subscribe((data: LeaveRequest) => {
            this.leaveRequest = data;
            this.startDate = this.leaveRequest.startDate;
            this.endDate = this.leaveRequest.endDate;
            this.leaveType = this.leaveRequest.leaveType;
            this.exceptionalLeaveType = this.leaveRequest.exceptionalLeaveType;
            this.timeOfDay = this.leaveRequest.timeOfDay;
            this.userEmail = this.leaveRequest.userEmail;
        });
    }
}
