import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ColumnMode, DatatableComponent} from "@swimlane/ngx-datatable";
import {LeaveService} from "../../../../shared/services/leave.service";
import {JwtHelperService} from "@auth0/angular-jwt";
import {ToastrService} from "ngx-toastr";
import {Leave} from "../../../../shared/models/leave.model";
import Swal from "sweetalert2";

@Component({
    selector: 'app-leave-list',
    templateUrl: './leave-list.component.html',
    styleUrls: ['./leave-list.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class LeaveListComponent implements OnInit {
    @ViewChild(DatatableComponent) table: DatatableComponent;
    private previousStatusFilter: string;
    public contentHeader: object;
    basicSelectedOption: number = 10;
    public rows: any;
    data: Leave[];
    public ColumnMode = ColumnMode;
    private tempFilterData: any;
    public currentUser: any;
    public role: string;
    public selectedStatus: string;
    public selectStatus = [
        {name: 'Pending',value: 'PENDING' },
        {name: 'Accepted',value: 'ACCEPTED' },
        {name: 'Rejected',value: 'REJECTED' },
        {name: 'All', value: ''},
    ];
    public selectedLeaveType: string;
    public selectLeaveType = [
        {name: 'Personal Leave', value: 'PERSONAL_LEAVE'},
        {name: 'Exceptional leave', value: 'EXCEPTIONAL_LEAVE'},
        {name: 'Sick Leave', value: 'SICK_LEAVE'},
        {name: 'Half Day', value: 'HALF_DAY'},
        {name: 'All', value: ''},
    ];
    private previousLeaveTypeFilter: string;
    private tempData: Leave[];

    constructor(private leaveService: LeaveService, private jwtService: JwtHelperService, private toastr: ToastrService) {
    }

    ngOnInit(): void {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'))
        this.role = this.currentUser.role
        this.contentHeader = {
            headerTitle: 'Leave Requests',
            actionButton: true,
            breadcrumb: {
                type: '',
                links: [
                    {
                        name: 'Home',
                        isLink: true,
                        link: '/'
                    },
                    {
                        name: 'Leaves',
                        isLink: true,
                        link: '/apps/leave-request'
                    },
                    {
                        name: 'Leave Requests List',
                        isLink: false
                    }
                ]
            }
        };
        this.getLeaves();
    }

    filterUpdate(event) {
        this.tempData = this.data;
        // Reset role and status filters

        // Get the search value
        const searchValue = event.target.value.toLowerCase();

        // Check if the search value is empty
        if (searchValue === '') {
            // If empty, reset to the original data
            this.rows = [...this.tempData];
        } else {
            // Apply the search filter to the original data
            this.rows = this.tempData.filter(row => {
                // Check if the property is not null before calling toLowerCase()
                return row.user.firstName && row.user.firstName.toLowerCase().includes(searchValue);
            });
        }

        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    getLeaves() {
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtService.decodeToken(token).email;
        const currentUserRole = this.jwtService.decodeToken(token).role;
        if (currentUserRole === 'MANAGER') {
            this.leaveService.getLeaveRequestsByManager(currentUserEmail).subscribe(
                (response) => {
                    this.data = response;
                    this.rows = this.data;
                    this.tempFilterData = this.rows;
                },
                (error) => {
                    console.log(error);
                }
            );
        }
        if (currentUserRole === 'ADMIN') {
            this.leaveService.getAllLeaveRequests().subscribe(
                (response) => {
                    this.data = response;
                    this.rows = this.data;
                    this.tempFilterData = this.rows;
                },
                (error) => {
                    console.log(error);
                }
            );
        }
        if (currentUserRole === 'USER') {
            this.leaveService.getLeaveRequestsByUserId(this.jwtService.decodeToken(token).id).subscribe(
                (response) => {
                    this.data = response;
                    this.rows = this.data;
                    this.tempFilterData = this.rows;
                },
                (error) => {
                    console.log(error);
                }
            );
        }
    }



    // deleteLeaveRequest(id:number) {
    //     this.leaveService.deleteLeaveRequest(id).subscribe(
    //         (response) => {
    //             this.toastr.success('Leave request deleted successfully', 'Success',{
    //                 progressBar: true,
    //                 toastClass: 'toast ngx-toastr',
    //                 closeButton: true
    //
    //             });
    //             this.getLeaves();
    //         },
    //         (error) => {
    //             this.toastr.error('Error while deleting leave request', 'Error',{
    //                 progressBar: true,
    //                 toastClass: 'toast ngx-toastr',
    //                 closeButton: true
    //             });
    //             console.log(error);
    //         }
    //     );
    // }

    deleteLeaveRequest(id:number) {
        Swal.fire({
            title: 'Confirm',
            text: 'Are you sure you want to delete this item? You will not be able to recover it!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-danger ml-1'
            }
        }).then((result) => {
            if (result.isConfirmed) {
                this.leaveService.deleteLeaveRequest(id).subscribe(
                    () => {

                        Swal.fire({
                            title: 'Deleted!',
                            text: 'Your file has been deleted.',
                            icon: 'success',
                            customClass: {
                                confirmButton: 'btn btn-success'
                            }
                        });
                        this.getLeaves();
                    },
                    (error) => {
                        // Error
                        // ... your error logic
                        console.log(error);
                        Swal.fire('Error', 'An error occurred while deleting the item.', 'error');
                    }
                );
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal.fire({
                    title: 'Cancelled',
                    text: 'The Leave Request was not deleted.',
                    icon: 'error',
                    customClass: {
                        confirmButton: 'btn btn-success'
                    }
                });
            }
        });
    }

    acceptLeaveRequest(id:number) {
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtService.decodeToken(token).email;
        this.leaveService.treatLeaveRequest(currentUserEmail,id,'ACCEPTED').subscribe(
            (response) => {
                this.toastr.success(`Leave request with id: ${id} accepted`, `Leave request accepted'`, {
                    progressBar: true,
                    progressAnimation: 'increasing',
                    closeButton: true,
                    positionClass: 'toast-top-right',
                    toastClass: 'toast ngx-toastr toast-success',
                    titleClass: 'toast-title',
                    messageClass: 'toast-message',
                    tapToDismiss: true,
                    timeOut: 5000
                });
                this.getLeaves();
            },
            (error) => {
                this.toastr.error(`Error while accepting leave request with id: ${id}`, `Error`, {
                    progressBar: true,
                    progressAnimation: 'increasing',
                    closeButton: true,
                    positionClass: 'toast-top-right',
                    toastClass: 'toast ngx-toastr toast-error',
                    timeOut: 5000
                });
                console.log(error);
            }
        );

    }

    rejectLeaveRequest(id:number) {
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtService.decodeToken(token).email;
        this.leaveService.treatLeaveRequest(currentUserEmail,id,'REJECTED').subscribe(
            (response) => {
                this.toastr.success(`Leave request with id: ${id} rejected`, `Leave request rejected'`, {
                    progressBar: true,
                    progressAnimation: 'increasing',
                    closeButton: true,
                    positionClass: 'toast-top-right',
                    toastClass: 'toast ngx-toastr toast-success',
                    titleClass: 'toast-title',
                    messageClass: 'toast-message',
                    tapToDismiss: true,
                    timeOut: 5000
                });
                this.getLeaves();
            },
            (error) => {
                this.toastr.error(`Error while rejecting leave request with id: ${id}`, `Error`, {
                    progressBar: true,
                    progressAnimation: 'increasing',
                    closeButton: true,
                    positionClass: 'toast-top-right',
                    toastClass: 'toast ngx-toastr toast-error',
                    timeOut: 5000
                });
                console.log(error);
            }
        );

    }

    filterByStatus(event: any) {
        const filter = event ? event.value.toLowerCase() : '';
        this.previousStatusFilter = filter;
        this.tempFilterData = this.data.filter(row => {
            return row.status.toLowerCase().indexOf(filter) !== -1 || !filter;
        });
        this.rows = this.tempFilterData;
        this.table.offset = 0;
    }

    filterByLeaveType(event: any) {
        const filter = event ? event.value.toLowerCase() : '';
        this.previousLeaveTypeFilter = filter;
        this.tempFilterData = this.data.filter(row => {
            return row.leaveType.toLowerCase().indexOf(filter) !== -1 || !filter;
        });
        this.rows = this.tempFilterData;
        this.table.offset = 0;
    }

    modalOpenVC(id) {
        
    }
}
