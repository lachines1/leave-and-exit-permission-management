export class LeaveRef {
    id? = undefined;
    url: string;
    title: string = '';
    start: string;
    end: string;
    allDay = false;
    calendar: '';
    extendedProps = {
        description: '',
    };
}
