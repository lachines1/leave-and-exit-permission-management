import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarViewComponent } from './calendar-view/calendar-view.component';
import {CalendarComponent} from "../calendar/calendar.component";
import {
  CalendarEventSidebarComponent
} from "../calendar/calendar-sidebar/calendar-event-sidebar/calendar-event-sidebar.component";
import {
  CalendarMainSidebarComponent
} from "../calendar/calendar-sidebar/calendar-main-sidebar/calendar-main-sidebar.component";
import {FullCalendarModule} from "@fullcalendar/angular";
import {RouterModule} from "@angular/router";
import {CoreCommonModule} from "../../../../@core/common.module";
import {CoreSidebarModule} from "../../../../@core/components";
import {FormsModule} from "@angular/forms";
import {Ng2FlatpickrModule} from "ng2-flatpickr";
import {NgSelectModule} from "@ng-select/ng-select";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import { FilterCalendarComponent } from './calendar-view/filter-calendar/filter-calendar.component';
import {ContentHeaderModule} from "../../../layout/components/content-header/content-header.module";

const routes = [
    {
        path: '',
        component: CalendarViewComponent
    }
    ];

@NgModule({
  declarations: [CalendarViewComponent, FilterCalendarComponent],
    imports: [
        CommonModule,
        FullCalendarModule,
        RouterModule.forChild(routes),
        CoreCommonModule,
        CoreSidebarModule,
        FormsModule,
        Ng2FlatpickrModule,
        NgSelectModule,
        NgbModule,
        ContentHeaderModule
    ],
})
export class LeaveCalendarModule { }
