import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {CalendarOptions} from "@fullcalendar/angular";
import {JwtHelperService} from "@auth0/angular-jwt";
import {LeaveService} from "../../../../shared/services/leave.service";
import {LeaveCalendarService} from "../../../../shared/services/leave-calendar.service";

@Component({
  selector: 'app-calendar-view',
  templateUrl: './calendar-view.component.html',
  styleUrls: ['./calendar-view.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CalendarViewComponent implements OnInit {

  public contentHeader: object;
  public events = [
    { title: 'All Day Event', start: '2024-06-01' },
    { title: 'Long Event', start: '2021-06-07', end: '2021-06-10' },
    { groupId: '999', title: 'Repeating Event', start: '2021-06-09T16:00:00' },
    { groupId: '999', title: 'Repeating Event', start: '2021-06-16T16:00:00' },
    { title: 'Conference', start: '2021-06-11', end: '2021-06-13' },
    { title: 'Meeting', start: '2021-06-12T10:30:00', end: '2021-06-12T12:30:00' },
    { title: 'Lunch', start: '2021-06-12T12:00:00' },
    { title: 'Meeting', start: '2021-06-12T14:30:00' },
    { title: 'Happy Hour', start: '2021-06-12T17:30:00' },
    { title: 'Dinner', start: '2021-06-12T20:00:00' },
    { title: 'Birthday Party', start: '2021-06-13T07:00:00' },
    { title: 'Click for Google', url: 'http://google.com/', start: '2021-06-28' }
  ];
  public event;
  public leavesData: any[] = [];
  public leaveRows: any;
  public leaveCount: number;


  public calendarOptions: CalendarOptions = {
    headerToolbar: {
      start: 'sidebarToggle, prev,next, title',
      end: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
    },
    initialView: 'dayGridMonth',
    initialEvents: this.events,
    weekends: true,
    editable: true,
    eventResizableFromStart: true,
    selectable: true,
    selectMirror: true,
    dayMaxEvents: 2,
    navLinks: true,
    eventClassNames: this.eventClass.bind(this),

  };

  constructor(private jwtHelperService: JwtHelperService,
              private leaveService: LeaveService,
              private leaveCalendarService: LeaveCalendarService) {
    this.getLeaves();
  }

  getLeaves() {
    const token = localStorage.getItem('token');
    const currentUserEmail = this.jwtHelperService.decodeToken(token).email;
    const currentUserRole = this.jwtHelperService.decodeToken(token).role;
    if (currentUserRole === 'MANAGER') {
      this.leaveService.getLeaveRequestsByManager(currentUserEmail).subscribe(
          (response) => {
            this.leavesData = response;
            this.leaveCount = this.leavesData.length
            this.leaveRows = this.leavesData;
          },
          (error) => {
            console.log(error);
          }
      );
    }
    if (currentUserRole === 'ADMIN') {
      this.leaveService.getAllLeaveRequests().subscribe(
          (response) => {
            this.leavesData = response;
            this.leaveRows = this.leavesData;
          },
          (error) => {
            console.log(error);
          }
      );
    }
    if (currentUserRole === 'USER') {
      this.leaveService.getLeaveRequestsByUserId(this.jwtHelperService.decodeToken(token).id).subscribe(
          (response) => {
            this.leavesData = response;
            this.leaveRows = this.leavesData;
            this.leaveCount = this.leavesData.length
          },
          (error) => {
            console.log(error);
          }
      );
    }

    console.log("these are the leaves: ",this.leavesData)
  }

  eventClass(s) {
    const calendarsColor = {
      ACCEPTED: 'success',
      REJECTED: 'danger',
      PENDING: 'warning',
    };

    const colorName = calendarsColor[s.event._def.extendedProps.calendar];
    return `bg-light-${colorName}`;
  }




  ngOnInit(): void {
    this.contentHeader = {
      headerTitle: 'Leaves Calendar',
      actionButton: true,
      breadcrumb: {
        type: '',
        links: [
          {
            name: 'Home',
            isLink: true,
            link: '/'
          },
          {
            name: 'Calendar',
            isLink: true,
            link: '/apps/calendar-view'
          },
          {
            name: 'Leaves Calendar',
            isLink: false
          }
        ]
      }
    };
    setTimeout(() => {
      this.events = this.leavesData.map(leave => this.leaveCalendarService.convertToFullCalendarEvent(leave));
        this.calendarOptions.events = this.events;
        this.calendarOptions.eventClassNames = this.eventClass.bind(this);
        console.log("these are the events: ",this.calendarOptions.initialEvents)
    }, 2000);

  }

}
