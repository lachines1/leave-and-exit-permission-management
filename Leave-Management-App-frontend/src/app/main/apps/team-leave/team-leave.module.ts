import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TeamLeaveListComponent} from './team-leave-list/team-leave-list.component';
import {TeamLeaveAddComponent} from './team-leave-add/team-leave-add.component';
import {TeamLeaveViewComponent} from './team-leave-view/team-leave-view.component';
import {CardSnippetModule} from "../../../../@core/components/card-snippet/card-snippet.module";
import {ContentHeaderModule} from "../../../layout/components/content-header/content-header.module";
import {FormsModule} from "@angular/forms";
import {NgSelectModule} from "@ng-select/ng-select";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {RouterModule} from "@angular/router";
import {CoreDirectivesModule} from "../../../../@core/directives/directives";
import {CsvModule} from "@ctrl/ngx-csv";
import {NgbDropdownModule} from "@ng-bootstrap/ng-bootstrap";
import {Ng2FlatpickrModule} from "ng2-flatpickr";
import { TeamLeaveEditComponent } from './team-leave-edit/team-leave-edit.component';

const routes = [
    {
        path: 'list',
        component: TeamLeaveListComponent
    },
    {
        path: 'add',
        component: TeamLeaveAddComponent
    },
    {
        path: 'view/:id',
        component: TeamLeaveViewComponent
    },
    {
        path: 'edit/:id',
        component: TeamLeaveEditComponent
    },
]

@NgModule({
    declarations: [
        TeamLeaveListComponent,
        TeamLeaveAddComponent,
        TeamLeaveViewComponent,
        TeamLeaveEditComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        CardSnippetModule,
        ContentHeaderModule,
        CoreDirectivesModule,
        CsvModule,
        FormsModule,
        NgSelectModule,
        NgxDatatableModule,
        NgbDropdownModule,
        Ng2FlatpickrModule,
    ]
})
export class TeamLeaveModule {
}
