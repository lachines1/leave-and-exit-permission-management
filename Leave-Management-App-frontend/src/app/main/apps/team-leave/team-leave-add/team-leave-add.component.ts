import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {JwtHelperService} from "@auth0/angular-jwt";
import {Router} from "@angular/router";
import {TeamLeaveService} from "../../../../shared/services/team-leave.service";
import {ToastrService} from "ngx-toastr";
import {TeamLeaveRequest} from "../../../../shared/models/team-leave-request.model";
import {FlatpickrOptions} from "ng2-flatpickr";
import {UserListService} from "../../user/user-list/user-list.service";
import {User} from "../../../../shared/models/user.model";
import {TeamService} from "../../../../shared/services/team.service";

@Component({
    selector: 'app-team-leave-add',
    templateUrl: './team-leave-add.component.html',
    styleUrls: ['./team-leave-add.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class TeamLeaveAddComponent implements OnInit {

    teamName: string;
    startDate: Date;
    endDate: Date;
    reason: string;
    public dateTimeOptions: FlatpickrOptions = {
        altInput: true,
        enableTime: true
    }
    contentHeader: object;
    allTeams: any[];
    private selectUser: User[];

    constructor(private toastr: ToastrService,
                private userService: UserListService,
                private teamLeaveService: TeamLeaveService,
                private router: Router,
                private teamService: TeamService,
                private jwtService: JwtHelperService) {
        this.getTeams();
    }

    ngOnInit(): void {
        this.contentHeader = {
            headerTitle: 'Create Team Leave Request',
            actionButton: true,
            breadcrumb: {
                type: '',
                links: [
                    {
                        name: 'Home',
                        isLink: true,
                        link: '/'
                    },
                    {
                        name: 'Team Leave Requests',
                        isLink: true,
                        link: '/apps/team-leave/list'
                    },
                    {
                        name: 'Create Team Leave Request',
                        isLink: false,
                        link: '/'
                    }

                ]
            }
        };
    }

    addTeamLeave() {
        const teamLeaveRequest = new TeamLeaveRequest();
        teamLeaveRequest.name = this.teamName;
        teamLeaveRequest.startDate = this.startDate;
        teamLeaveRequest.endDate = this.endDate;
        teamLeaveRequest.reason = this.reason;

        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtService.decodeToken(token).email;

        this.teamLeaveService.createTeamLeave(currentUserEmail, teamLeaveRequest).subscribe
        (
            (data) => {
                this.toastr.success('Team Leave Request added successfully', 'Success', {
                    timeOut: 3000,
                    closeButton: true,
                    progressBar: true,
                    toastClass: 'toast ngx-toastr',
                });
                this.router.navigate(['/apps/team-leave/list']);
            },
            (error) => {
                this.toastr.error('Error adding team leave request', 'Error', {
                    timeOut: 3000,
                    closeButton: true,
                    progressBar: true,
                    toastClass: 'toast ngx-toastr',
                });
            }
        );
    }

    getTeams() {
        const role = this.jwtService.decodeToken(localStorage.getItem('token')).role;
        if (role === "ADMIN") {
            this.teamService.getAllTeams().subscribe(
                (data) => {
                    this.allTeams = data;
                    this.allTeams = data.map(team => {
                        // Add a new attribute 'value' with the same value as 'name'
                        return {...team, value: team.name};
                    });
                },
                (error) => {
                    this.toastr.error('Error fetching teams', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx-toastr',
                    });
                });
        } else if (role === "MANAGER") {
            const currentUserEmail = this.jwtService.decodeToken(localStorage.getItem('token')).email;
            this.teamService.getTeamsByManager(currentUserEmail).subscribe(
                (data) => {
                    this.allTeams = data;
                    this.allTeams = data.map(team => {
                        // Add a new attribute 'value' with the same value as 'name'
                        return {...team, value: team.name};
                    });
                },
                (error) => {
                    this.toastr.error('Error fetching teams', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx-toastr',
                    });
                });
        }

    }

    onTeamNameChange(selectedItem: any) {
        this.teamName = selectedItem.value;
    }
}
