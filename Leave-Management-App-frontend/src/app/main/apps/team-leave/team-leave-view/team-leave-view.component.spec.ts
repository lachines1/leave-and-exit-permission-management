import {ComponentFixture, TestBed} from '@angular/core/testing';

import {TeamLeaveViewComponent} from './team-leave-view.component';

describe('TeamLeaveViewComponent', () => {
    let component: TeamLeaveViewComponent;
    let fixture: ComponentFixture<TeamLeaveViewComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [TeamLeaveViewComponent]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(TeamLeaveViewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
