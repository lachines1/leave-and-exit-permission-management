import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ColumnMode, DatatableComponent} from "@swimlane/ngx-datatable";
import {JwtHelperService} from "@auth0/angular-jwt";
import {TeamLeave} from "../../../../shared/models/team-leave.model";
import {TeamLeaveService} from "../../../../shared/services/team-leave.service";
import {ToastrService} from "ngx-toastr";
import {UserListService} from "../../user/user-list/user-list.service";

@Component({
    selector: 'app-team-leave-list',
    templateUrl: './team-leave-list.component.html',
    styleUrls: ['./team-leave-list.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class TeamLeaveListComponent implements OnInit {
    public currentUser: any
    public contentHeader: object;
    basicSelectedOption: number = 10;
    public rows: any;
    private manager: any;
    data: TeamLeave[];
    public ColumnMode = ColumnMode;
    private tempFilterData: any;
    public role: string;
    public selectedStatus: any;
    public selectStatus = [
        {name: 'Pending', value: 'PENDING'},
        {name: 'Approved', value: 'ACCEPTED'},
        {name: 'Rejected', value: 'REJECTED'},
    ];
    private previousStatusFilter: string;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    private tempData: TeamLeave[];

    constructor(private leaveService: TeamLeaveService, private jwtService: JwtHelperService, private toastrService: ToastrService, private userService: UserListService) {

    }

    ngOnInit(): void {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'))
        this.role = this.currentUser.role
        this.contentHeader = {
            headerTitle: 'Team Leaves List',
            actionButton: true,
            breadcrumb: {
                type: '',
                links: [
                    {
                        name: 'Home',
                        isLink: true,
                        link: '/'
                    },
                    {
                        name: 'Team Leaves',
                        isLink: true,
                        link: '/apps/team-leave/list'
                    },
                    {
                        name: 'Team Leaves List',
                        isLink: false
                    }
                ]
            }
        };
        this.getLeaves();
    }

    filterUpdate(event) {
        this.tempData = this.data;
        // Reset role and status filters

        // Get the search value
        const searchValue = event.target.value.toLowerCase();

        // Check if the search value is empty
        if (searchValue === '') {
            // If empty, reset to the original data
            this.rows = [...this.tempData];
        } else {
            // Apply the search filter to the original data
            this.rows = this.tempData.filter(row => {
                // Check if the property is not null before calling toLowerCase()
                return row.team.name && row.team.name.toLowerCase().includes(searchValue);
            });
        }

        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    getLeaves() {
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtService.decodeToken(token).email;
        const currentUserRole = this.jwtService.decodeToken(token).role;
        const currentUserId = this.jwtService.decodeToken(token).id;
        if (currentUserRole === 'MANAGER') {
            this.userService.getUserByEmail(currentUserEmail).subscribe(
                (response) => {
                    this.manager = response;
                    this.leaveService.getTeamLeavesForTeam(this.manager.team.id).subscribe(
                        (response) => {
                            this.data = response;
                            this.rows = response;
                        },
                        (error) => {
                            console.log(error);
                        }
                    );
                },
                (error) => {
                    console.log(error);
                }
            );
            this.leaveService.getTeamLeavesForTeam(this.manager.team.id).subscribe(
                (response) => {
                    this.data = response;
                    this.rows = response;
                },
                (error) => {
                    console.log(error);
                }
            );
        } else if (currentUserRole === 'USER') {
            this.leaveService.getTeamLeaveById(currentUserId).subscribe(
                (response) => {
                    this.data = response;
                    this.rows = response;
                },
                (error) => {
                    console.log(error);
                }
            );
        } else {
            this.leaveService.getAllTeamLeaves().subscribe(
                (response) => {
                    this.data = response;
                    this.rows = response;
                },
                (error) => {
                    console.log(error);
                }
            );
        }
    }

    protected readonly localStorage = localStorage;

    deleteTeamLeave(id) {
        this.leaveService.deleteTeamLeave(id).subscribe(
            (response) => {
                this.getLeaves();
            },
            (error) => {
                console.log(error);
            }
        );

    }

    acceptTeamLeave(id) {
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtService.decodeToken(token).email;
        this.leaveService.treatTeamLeaveRequest(currentUserEmail,id,'ACCEPTED').subscribe(
            (response) => {
                this.toastrService.success(`Leave request with id: ${id} accepted`, `Leave request accepted'`, {
                    progressBar: true,
                    progressAnimation: 'increasing',
                    closeButton: true,
                    positionClass: 'toast-top-right',
                    toastClass: 'toast ngx-toastr toast-success',
                    timeOut: 5000
                });
                this.getLeaves();
            },
            (error) => {
                this.toastrService.error(`Leave request with id: ${id} could not be accepted \n ${error}`, `Leave request not accepted'`, {
                    progressBar: true,
                    progressAnimation: 'increasing',
                    closeButton: true,
                    positionClass: 'toast-top-right',
                    toastClass: 'toast ngx-toastr toast-danger',
                    timeOut: 5000
                });
                console.log(error);
            }
        );

    }

    rejectTeamLeave(id) {
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtService.decodeToken(token).email;
        this.leaveService.treatTeamLeaveRequest(currentUserEmail,id,'REJECTED').subscribe(
            (response) => {
                this.toastrService.success(`Leave request with id: ${id} rejected`, `Leave request rejected'`, {
                    progressBar: true,
                    progressAnimation: 'increasing',
                    closeButton: true,
                    positionClass: 'toast-top-right',
                    toastClass: 'toast ngx-toastr toast-success',
                    timeOut: 5000
                });
                this.getLeaves();
            },
            (error) => {
                this.toastrService.error(`Leave request with id: ${id} could not be rejected \n ${error}`, `Leave request not rejected'`, {
                    progressBar: true,
                    progressAnimation: 'increasing',
                    closeButton: true,
                    positionClass: 'toast-top-right',
                    toastClass: 'toast ngx-toastr toast-danger',
                    timeOut: 5000
                });
                console.log(error);
            }
        );
    }

    filterByStatus(event: any) {
        const filter = event ? event.value.toLowerCase() : '';
        this.previousStatusFilter = filter;
        this.tempFilterData = this.data.filter(row => {
            return row.status.toLowerCase().indexOf(filter) !== -1 || !filter;
        });
        this.rows = this.tempFilterData;
        this.table.offset = 0;
    }

    modalOpenVC(id) {
        
    }
}
