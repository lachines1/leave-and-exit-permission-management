import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamLeaveEditComponent } from './team-leave-edit.component';

describe('TeamLeaveEditComponent', () => {
  let component: TeamLeaveEditComponent;
  let fixture: ComponentFixture<TeamLeaveEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeamLeaveEditComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TeamLeaveEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
