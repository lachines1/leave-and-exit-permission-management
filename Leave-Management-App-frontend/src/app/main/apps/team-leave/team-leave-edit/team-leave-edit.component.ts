import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FlatpickrOptions} from "ng2-flatpickr";
import {ToastrService} from "ngx-toastr";
import {TeamLeaveService} from "../../../../shared/services/team-leave.service";
import {ActivatedRoute, Router} from "@angular/router";
import {JwtHelperService} from "@auth0/angular-jwt";
import {TeamLeaveRequest} from "../../../../shared/models/team-leave-request.model";
import {TeamService} from "../../../../shared/services/team.service";

@Component({
    selector: 'app-team-leave-edit',
    templateUrl: './team-leave-edit.component.html',
    styleUrls: ['./team-leave-edit.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class TeamLeaveEditComponent implements OnInit {
    public teamLeaveRequest: TeamLeaveRequest = {};
    teamName: string;
    startDate: Date;
    endDate: Date;
    reason: string;
    public dateTimeOptions: FlatpickrOptions = {
        altInput: true,
        enableTime: true
    }
    contentHeader: object;
    allTeams: any;
    private teamLeaveId: number;

    constructor(
        private toastr: ToastrService,
        private teamLeaveService: TeamLeaveService,
        private teamService: TeamService,
        private router: Router,
        private route: ActivatedRoute,
        private jwtService: JwtHelperService) {
        this.route.params.subscribe(params => {
            this.teamLeaveId = params['id'];
            this.getTeamLeaveById(this.teamLeaveId);
        });
    }

    ngOnInit(): void {
        this.getTeams();
        this.contentHeader = {
            headerTitle: 'Edit Team Leave Request',
            actionButton: true,
            breadcrumb: {
                type: '',
                links: [
                    {
                        name: 'Home',
                        isLink: true,
                        link: '/'
                    },
                    {
                        name: 'Team Leave Requests',
                        isLink: true,
                        link: '/apps/team-leave/list'
                    },
                    {
                        name: 'Edit Team Leave Request',
                        isLink: false,
                        link: '/'
                    }

                ]
            }
        };
    }

    editTeamLeave() {
        this.route.params.subscribe(params => {
            this.teamLeaveId = params['id'];
            this.getTeamLeaveById(this.teamLeaveId);
        });
        this.teamLeaveRequest.name = this.teamName;
        this.teamLeaveRequest.startDate = this.startDate;
        this.teamLeaveRequest.endDate = this.endDate;
        this.teamLeaveRequest.reason = this.reason;
        this.teamLeaveService.updateTeamLeave(this.teamLeaveId, this.teamLeaveRequest).subscribe(
            (data) => {
                this.toastr.success('Team Leave Request updated successfully', 'Success',
                    {
                        timeOut: 3000,
                        positionClass: 'toast-top-right',
                        closeButton: true,
                        progressBar: true,
                        progressAnimation: 'increasing',
                    });
                this.router.navigate(['/apps/team-leave/list']);
            },
            (error) => {
                this.toastr.error('Team Leave Request update failed', 'Error', {
                    timeOut: 3000,
                    positionClass: 'toast-top-right',
                    closeButton: true,
                    progressBar: true,
                    progressAnimation: 'increasing',
                });
            }
        );

    }

    getTeams() {
        const role = this.jwtService.decodeToken(localStorage.getItem('token')).role;
        if (role === "ADMIN") {
            this.teamService.getAllTeams().subscribe(
                (data) => {
                    this.allTeams = data;
                    this.allTeams = data.map(team => {
                        // Add a new attribute 'value' with the same value as 'name'
                        return {...team, value: team.name};
                    });
                },
                (error) => {
                    this.toastr.error('Error fetching teams', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx-toastr',
                    });
                });
        } else if (role === "MANAGER") {
            const currentUserEmail = this.jwtService.decodeToken(localStorage.getItem('token')).email;
            this.teamService.getTeamsByManager(currentUserEmail).subscribe(
                (data) => {
                    this.allTeams = data;
                    this.allTeams = data.map(team => {
                        // Add a new attribute 'value' with the same value as 'name'
                        return {...team, value: team.name};
                    });
                },
                (error) => {
                    this.toastr.error('Error fetching teams', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx-toastr',
                    });
                });
        }

    }

    onTeamNameChange(selectedItem: any) {
        this.teamName = selectedItem.value;
    }

    private getTeamLeaveById(teamLeaveId: number) {
        this.teamLeaveService.getTeamLeaveById(teamLeaveId).subscribe((data: TeamLeaveRequest) => {
            this.teamLeaveRequest = data;
            this.teamName = this.teamLeaveRequest.name;
            this.startDate = this.teamLeaveRequest.startDate;
            this.endDate = this.teamLeaveRequest.endDate;
            this.reason = this.teamLeaveRequest.reason;
        });

    }
}
