import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';

import {BehaviorSubject} from 'rxjs';
import {environment} from "../../../../../environments/environment";

@Injectable()
export class UserViewService {
    public rows: any;
    public onUserViewChanged: BehaviorSubject<any>;
    public id;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(private _httpClient: HttpClient) {
        // Set the defaults
        this.onUserViewChanged = new BehaviorSubject({});
    }

    getUserById(id) {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}` // Add the token as a Bearer token
            })
        };

        return this._httpClient.get(`${environment.apiUrl}/api/v1/users/id/${id}`, httpOptions);
    }


    deleteUserById(id: number) {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        }
        return this._httpClient.delete(`${environment.apiUrl}/api/v1/users/admin/id/${id}`, httpOptions);
    }
}
