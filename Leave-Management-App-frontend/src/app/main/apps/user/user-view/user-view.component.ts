import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {Subject} from 'rxjs';

import {UserViewService} from 'app/main/apps/user/user-view/user-view.service';
import {User} from "../../../../shared/models/user.model";
import Swal from "sweetalert2";

@Component({
    selector: 'app-user-view',
    templateUrl: './user-view.component.html',
    styleUrls: ['./user-view.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class UserViewComponent implements OnInit, OnDestroy {
    // public
    public url = this.router.url;
    public lastValue;
    public data: User;

    // private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {Router} router
     * @param userService
     * @param route
     */
    constructor(private router: Router, private userService: UserViewService, private route: ActivatedRoute) {
        this._unsubscribeAll = new Subject();
        this.lastValue = this.url.substr(this.url.lastIndexOf('/') + 1);
    }

    // Lifecycle Hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * On init
     */
    ngOnInit(): void {
        this.route.params.subscribe(params => {
            const userId = params['id'];

            // Fetch team based on the ID
            this.getData(userId);
        });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    private getData(id: number) {
        this.userService.getUserById(id).subscribe((res: any) => {
            this.data = res;
        });
    }

    deleteUser() {
        Swal.fire({
            title: 'Are you sure?',
            text: 'You will not be able to recover this user!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it',
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-danger ml-1'
            },

        }).then((result) => {
            if (result.value) {
                this.userService.deleteUserById(this.data.id).subscribe((res: any) => {
                    Swal.fire({
                        icon: 'success',
                        title: 'Deleted!',
                        text: 'Your user has been deleted.',
                        customClass: {
                            confirmButton: 'btn btn-success'
                        }
                    });
                    this.router.navigate(['/apps/user/user-list']);
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal.fire({
                    title: 'Cancelled',
                    text: 'Your user is safe :)',
                    icon: 'error',
                    customClass: {
                        confirmButton: 'btn btn-success'
                    }
                });
            }
        });

    }
}
