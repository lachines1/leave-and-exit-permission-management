import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserListService} from "../user-list/user-list.service";
import {User} from "../../../../shared/models/user.model";
import {ToastrService} from "ngx-toastr";

@Component({
    selector: 'app-user-edit',
    templateUrl: './user-edit.component.html',
    styleUrls: ['./user-edit.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class UserEditComponent implements OnInit {

    public user: User;
    public contentHeader: object;
    roles: any[] = [
        {name: 'User', value: 'USER'},
        {name: 'Manager', value: 'MANAGER'},
        {name: 'Admin', value: 'ADMIN'}
    ];
    role: string;
    private data: any;
    private targetMail: string = '';
    private currentUser: any;

    constructor(private router: Router,
                private _userListService: UserListService,
                private toastr: ToastrService,
                private route: ActivatedRoute) {
        this.route.params.subscribe(params => {
            const userId = params['id'];

            // Fetch team based on the ID
            this.getUserById(userId);
        });

    }

    ngOnInit(): void {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.role = this.currentUser.role;
        this.route.params.subscribe(params => {
            const userId = params['id'];
            this.getUserById(userId);
        });

        this.contentHeader = {
            headerTitle: 'Edit User',
            actionButton: true,
            breadcrumb: {
                type: '',
                links: [
                    {
                        name: 'Home',
                        isLink: true,
                        link: '/'
                    },
                    {
                        name: 'Users',
                        isLink: true,
                        link: '/apps/user/user-list'
                    },
                    {
                        name: 'Edit User',
                        isLink: false,
                        link: '/'
                    }

                ]
            }
        };

    }

    getUserById(id: number) {
        this._userListService.getUserById(id).subscribe((res: User) => {
            this.data = res;
            this.targetMail = this.data.email;
            console.log(this.targetMail);
            this.user = res;
        });
    }

    updateUser() {
        this._userListService.updateUser(this.targetMail, this.user).subscribe((res: any) => {
                console.log(this.user);
                this.toastr.success('User updated successfully', 'Success', {
                    timeOut: 3000,
                    closeButton: true,
                    progressBar: true,
                    toastClass: 'toast ngx-toastr success'
                });

                if (this.currentUser.role === 'ADMIN') {
                    this.router.navigate(['/apps/user/user-list']);
                } else {
                    this.router.navigate(['/apps/dashboard']);
                }
            },
            (error: any) => {
                this.toastr.error('Error updating user', 'Error', {
                    timeOut: 3000,
                    closeButton: true,
                    progressBar: true,
                    toastClass: 'toast ngx-toastr error'
                });
            });
    }

    onRoleChange(selectedItem: any) {
        this.user.role = selectedItem.value;
    }
}
