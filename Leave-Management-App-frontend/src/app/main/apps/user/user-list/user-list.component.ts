import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ColumnMode, DatatableComponent} from '@swimlane/ngx-datatable';
import {CoreSidebarService} from '@core/components/core-sidebar/core-sidebar.service';

import {UserListService} from 'app/main/apps/user/user-list/user-list.service';
import {User} from "../../../../shared/models/user.model";
import Swal from "sweetalert2";

@Component({
    selector: 'app-user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class UserListComponent implements OnInit {

    // Public
    public sidebarToggleRef = false;
    public data: User[] = [];
    public selectedOption = 10;
    public ColumnMode = ColumnMode;
    public temp = [];
    public previousRoleFilter = '';
    public previousPlanFilter = '';
    public previousStatusFilter = '';

    public selectRole: any = [
        {name: 'All', value: ''},
        {name: 'Admin', value: 'ADMIN'},
        {name: 'Manager', value: 'MANAGER'},
        {name: 'User', value: 'USER'},
    ];

    public selectStatus: any = [
        {name: 'All', value: ''},
        {name: 'On Leave', value: true},
        {name: 'Available', value: false}
    ];

    public selectedRole = [];
    public searchValue = '';
    selectedStatus: any;

    // Decorator
    @ViewChild(DatatableComponent) table: DatatableComponent;

    // Private
    private tempData = [];
    protected rows: any;

    /**
     * Constructor
     *
     * @param {UserListService} _userListService
     * @param {CoreSidebarService} _coreSidebarService
     */
    constructor(
        private _userListService: UserListService,
        private _coreSidebarService: CoreSidebarService,
    ) {
    }


    filterUpdate(event) {
        this.tempData = this.data;
        // Reset role and status filters
        this.previousRoleFilter = '';
        this.previousStatusFilter = '';

        // Get the search value
        const searchValue = event.target.value.toLowerCase();

        // Check if the search value is empty
        if (searchValue === '') {
            // If empty, reset to the original data
            this.rows = [...this.tempData];
        } else {
            // Apply the search filter to the original data
            this.rows = this.tempData.filter(row => {
                // Check if the property is not null before calling toLowerCase()
                return row.firstName && row.firstName.toLowerCase().includes(searchValue);
            });
        }

        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }


    /**
     * Toggle the sidebar
     *
     * @param name
     */
    toggleSidebar(name): void {
        this._coreSidebarService.getSidebarRegistry(name).toggleOpen();
    }

    /**
     * Filter By Roles
     *
     * @param event
     */
    filterByRole(event) {
        this.tempData = this.data;
        const filter = event ? event.value.toLowerCase() : '';

        // Apply the role filter to the original data
        this.temp = this.tempData.filter(row => {
            return row.role.toLowerCase().indexOf(filter) !== -1 || !filter;
        });

        // Update the displayed rows with the filtered data
        this.rows = this.temp;

        // Reset other filters and search
        this.previousPlanFilter = '';
        this.previousStatusFilter = '';
        this.searchValue = '';

        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }


    // ...

    filterByStatus(event) {
        this.tempData = this.data;
        const filter = event ? event.value : '';

        // Check if the filter is "All"
        if (filter === '') {
            // If "All," reset to the original data
            this.rows = [...this.tempData];
        } else {
            // Apply the filter based on the boolean value
            this.temp = this.tempData.filter(row => row.onLeave === filter);

            // Update the displayed rows with the filtered data
            this.rows = this.temp;
        }

        // Reset other filters and search
        this.previousPlanFilter = '';
        this.previousRoleFilter = '';
        this.searchValue = '';

        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

// ...


    ngOnInit(): void {
        this._userListService.getAllUsers().subscribe(response => {
            this.data = response;
            this.rows = this.data;
        });
        this.tempData = this.rows;
    }


    deleteUserDetail(email) {
        Swal.fire({
            title: 'Are you sure?',
            text: 'You will not be able to recover this user!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it',
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-danger ml-1'
            }
        }).then((result) => {
            if (result.value) {
                this._userListService.deleteUser(email).subscribe(
                    () => {
                        Swal.fire(
                            'Deleted!',
                            'Your user has been deleted.',
                            'success'
                        );
                        this._userListService.getAllUsers().subscribe(response => {
                            this.data = response;
                            this.rows = this.data;
                        });
                        this.tempData = this.rows;
                    },
                    (error) => {
                        console.log(error);
                        Swal.fire(
                            'Error!',
                            'Error while deleting. \nYour user is safe though :)',
                            'error'
                        );
                    }
                );

            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal.fire(
                    'Cancelled',
                    'Your user is safe :)',
                    'error'
                );
            }
        });
    }


    @ViewChild('tableRowDetails') tableRowDetails: any;

    /**
     * rowDetailsToggleExpand
     *
     * @param row
     */
    rowDetailsToggleExpand(row: any) {
        this.tableRowDetails.rowDetail.toggleExpandRow(row);
    }

}
