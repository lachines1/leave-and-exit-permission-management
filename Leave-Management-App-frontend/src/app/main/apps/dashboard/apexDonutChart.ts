import { chartColors } from "./chartColors";
  let TotalUser = 0; let TotalTeams = 0; let TotalDepartments = 0;
const labels1 = "Users";
const labels2 = "Departments";
const labels3 = "Teams";
export const apexDonutChart = ( tt?, tu?,td?) => ({
    series: [
        td ? td : TotalDepartments,
        tt ? tt : TotalTeams,
        tu ? tu : TotalUser,
    ],
    chart: {
        height: 350,
        type: "donut",
    },
    colors: [
        chartColors.donut.series1,
        chartColors.donut.series2,
        chartColors.donut.series3,
        chartColors.donut.series5,
    ],
    plotOptions: {
        pie: {
            donut: {
                labels: {
                    show: true,
                    name: {
                        fontSize: "2rem",
                        fontFamily: "Montserrat",
                    },
                    value: {
                        fontSize: "1rem",
                        fontFamily: "Montserrat",
                        formatter: function (val) {
                            return parseInt (val) +"";
                        },
                    },
                },
            },
        },
    },
    legend: {
        show: true,
        position: "bottom",
    },
    labels: [labels3, labels1, labels2],
    responsive: [
        {
            breakpoint: 480,
            options: {
                chart: {
                    height: 300,
                },
                legend: {
                    position: "bottom",
                },
            },
        },
    ],
});
