export const exitStatusBarGraphConfig = (exitData: number[]) => ({
    series: [{
        name: 'Exit Status',
        data: exitData
    }],
    chart: {
        height: 250,
        type: 'bar',
        toolbar: {
            show: false
        }
    },
    colors: ['#C70039'],

    plotOptions: {
        bar: {
            horizontal: false,
            columnWidth: '50%',
            endingShape: 'rounded'
        },
    },
    xaxis: {
        categories: ['Pending', 'Accepted', 'Rejected']
    },
    yaxis: {
        title: {
            text: 'Number of Leave Status'
        }
    },
    legend: {
        show: true,
        position: 'bottom',
        horizontalAlign: 'center',
        fontSize: '14px',
        markers: {
            width: 16,
            height: 16,
            strokeWidth: 0
        },
        itemMargin: {
            horizontal: 8,
            vertical: 8
        }
    },
    responsive: [{
        breakpoint: 480,
        options: {
            chart: {
                height: 300
            },
            legend: {
                position: 'bottom'
            }
        }
    }]
});
