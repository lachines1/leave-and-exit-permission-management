import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardComponent} from './dashboard/dashboard.component';
import {RouterModule} from "@angular/router";
import {CardSnippetModule} from "../../../../@core/components/card-snippet/card-snippet.module";
import {ContentHeaderModule} from "../../../layout/components/content-header/content-header.module";
import {CoreDirectivesModule} from "../../../../@core/directives/directives";
import {CsvModule} from "@ctrl/ngx-csv";
import {FormsModule} from "@angular/forms";
import {NgSelectModule} from "@ng-select/ng-select";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {NgbDropdownModule} from "@ng-bootstrap/ng-bootstrap";
import {LeaveModule} from "../leave/leave.module";
import {NgApexchartsModule} from "ng-apexcharts";

const routes = [
    {
        path: '',
        component: DashboardComponent
    }
]


@NgModule({
    declarations: [
        DashboardComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        CardSnippetModule,
        ContentHeaderModule,
        CoreDirectivesModule,
        CsvModule,
        FormsModule,
        NgSelectModule,
        NgxDatatableModule,
        NgbDropdownModule,
        LeaveModule,
        NgApexchartsModule,
    ]
})
export class DashboardModule {
}
