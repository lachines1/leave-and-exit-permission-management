export const externalActivitiesChart = (data: any) => {
    const seriesData = [data.usedExternalActivities, data.remainingExternalActivities];

    return {
        series: seriesData,
        chart: {
            type: 'donut',
        },
        plotOptions: {
            pie: {
                startAngle: -90,
                endAngle: 90,
                offsetY: 10
            }
        },
        grid: {
            padding: {
                bottom: -80
            }
        },
        labels: ["Used External Activities", "Remaining External Activities"],
        responsive: [{
            breakpoint: 480,
            options: {
                chart: {
                    width: 200
                },
                legend: {
                    position: 'bottom'
                }
            }
        }]
    };
};
