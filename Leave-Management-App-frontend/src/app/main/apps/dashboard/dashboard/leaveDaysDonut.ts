export const leaveDaysChart = (data: any) => {
    const seriesData = [data.usedLeaveDays, data.remainingLeaveDays];

    return {
        series: seriesData,
        chart: {
            type: 'donut',
        },
        plotOptions: {
            pie: {
                startAngle: -90,
                endAngle: 90,
                offsetY: 10
            }
        },
        grid: {
            padding: {
                bottom: -80
            }
        },
        labels: ["Used Leave Days", "Remaining Leave Days"],
        responsive: [{
            breakpoint: 480,
            options: {
                chart: {
                    width: 200
                },
                legend: {
                    position: 'bottom'
                }
            }
        }]
    };
};
