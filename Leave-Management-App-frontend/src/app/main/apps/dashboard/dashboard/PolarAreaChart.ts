export const PolarAreaChartConfig = (permissionData: any) => {

    const formattedData = [];

    for (const [permissionType, permissions] of Object.entries(permissionData)) {
        const formattedPermissionType = toTitleCase(permissionType); // Format permission type
        for (const [permission, value] of Object.entries(permissions)) {
            formattedData.push({ name: toTitleCase(permission), y: value });
        }
    }

    return {
        series: formattedData.map(item => Math.round(item.y)),
        chart: {
            height: 350,
            type: 'polarArea',
        },
        labels: formattedData.map(item => item.name),
        legend: {
            show: true,
            position: 'bottom',
            horizontalAlign: 'center',
            fontSize: '14px',
            markers: {
                width: 16,
                height: 16,
                strokeWidth: 0
            },
            itemMargin: {
                horizontal: 8,
                vertical: 8
            }
        },
    };
};

function toTitleCase(str) {
    return str.split('_').map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()).join(' ');
}