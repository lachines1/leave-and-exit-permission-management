export const leaveTypeBarGraphConfig = (leaveData: number[]) => ({
    series: [{
        name: 'Leave Status',
        data: leaveData
    }],
    chart: {
        height: 250,
        type: 'bar',
        toolbar: {
            show: false
        }
    },
    colors: ['#00E396'],
    plotOptions: {
        bar: {
            horizontal: false,
            columnWidth: '50%',
            endingShape: 'rounded'
        },
    },
    xaxis: {
        categories: ['Personal Leave', 'Exceptional Leave', 'Sick Leave', 'Half Day Leave']
    },
    yaxis: {
        title: {
            text: 'Number of Leave Status'
        }
    },
    legend: {
        show: true,
        position: 'bottom',
        horizontalAlign: 'center',
        fontSize: '14px',
        markers: {
            width: 16,
            height: 16,
            strokeWidth: 0
        },
        itemMargin: {
            horizontal: 8,
            vertical: 8
        }
    },
    responsive: [{
        breakpoint: 480,
        options: {
            chart: {
                height: 300
            },
            legend: {
                position: 'bottom'
            }
        }
    }]
});
