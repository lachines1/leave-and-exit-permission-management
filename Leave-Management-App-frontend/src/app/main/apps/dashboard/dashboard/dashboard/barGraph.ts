export const leaveStatusBarGraphConfig = (pending?, approved?, rejected?) => ({
    series: [pending, approved, rejected],
    chart: {
        height: 350,
        type: 'bar',
    },
    colors: ['#FEB019', '#00E396', '#FF4560'],
    plotOptions: {
        bar: {
            horizontal: false,
            columnWidth: '55%',
            endingShape: 'rounded'
        },
    },
    dataLabels: {
        enabled: false
    },
    xaxis: {
        categories: ['Pending', 'Approved', 'Rejected'],
    },
    yaxis: {
        title: {
            text: 'Number of Leave Requests'
        }
    },
    legend: {
        show: true,
        position: 'bottom',
    },
    responsive: [
        {
            breakpoint: 480,
            options: {
                chart: {
                    height: 300,
                },
                legend: {
                    position: 'bottom',
                },
            },
        },
    ],
});
