import {
  AfterViewInit,
  Component,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from "@angular/core";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { LeaveService } from "../../../../shared/services/leave.service";
import { ExternalAuthorization } from "../../../../shared/models/external-authorization.model";
import { ColumnMode, DatatableComponent } from "@swimlane/ngx-datatable";
import { ExternalAuthorizationService } from "../../../../shared/services/external-authorization.service";
import { JwtHelperService } from "@auth0/angular-jwt";
import { TeamLeaveService } from "../../../../shared/services/team-leave.service";
import { UserListService } from "../../user/user-list/user-list.service";
import { User } from "../../../../shared/models/user.model";
import { TeamService } from "../../../../shared/services/team.service";
import { OrganizationalUnitService } from "../../../../shared/services/organizational-unit.service";
import { apexDonutChart } from "../apexDonutChart";
import { CoreConfigService } from "../../../../../@core/services/config.service";
import { apexDonutChart2 } from "./apexDonutChart2";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import Swal from "sweetalert2";
import { leaveStatusBarGraphConfig } from "./barGraph";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class DashboardComponent implements OnInit, AfterViewInit {
  @ViewChild("apexDonutChartRef") apexDonutChartRef: any;
  @ViewChild("apexDonutChartRef2") apexDonutChartRef2: any;
  basicSelectedOption: number = 10;
  public rows: any;
  data: ExternalAuthorization[];
  public ColumnMode = ColumnMode;
  userChart;
  depChart;
  teamChart;
  public currentUser: any;
  public apexDonutChart;
  public apexDonutChart2;
  apexDonutChart2Done = false;
  public leaveStatusBarGraphConfig;
  public role: string;
  public leavesData: any[] = [];
  public leavesRequest: any[] = [];
  public leaveRows: any;
  public user: User;
  public usersOnLave: User[] = [];
  public managerCount: number;
  public adminCount: number;
  public isMenuToggled = false;
  @ViewChild(DatatableComponent) table: DatatableComponent;
  public leaveCount: number = 0;
  public leaveChart: number;
  public teamLeaveChart: number;
  public externalAuthorizationDataChart: number;
  public usersOnLaveChart: number;
  selectedOption: number = 10;
  @ViewChild("content") content: any;
  @ViewChild("tableRowDetails") tableRowDetails: any;
  public allUsers: User[];
  membersOnLeave: any[] = [];
  protected usersData: User[];
  protected teamData: any;
  protected departmentData: any;
  protected teamLeaveData: any;
  protected externalAuthorizationData: any[] = [];
  protected externalAuthorizationRows: any;
  protected email: string;
  private tempFilterData: any;
  private manager: any;
  private teamRows: any;
  private departmentRows: any;
  private teamLeaveRows: any;
  private teamId: number;
  private tempUserData: User[];
  private previousRoleFilter: string;
  private previousStatusFilter: string;

  constructor(
    private externalAuthorizationService: ExternalAuthorizationService,
    private teamLeaveService: TeamLeaveService,
    private userService: UserListService,
    private jwtHelperService: JwtHelperService,
    private leaveService: LeaveService,
    private teamService: TeamService,
    private departmentService: OrganizationalUnitService,
    private toastr: ToastrService,
    private _coreConfigService: CoreConfigService,
    private modalService: NgbModal,
    private router: Router
  ) {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
    this.userService.getUserByEmail(this.currentUser.email).subscribe(
      (response) => {
        this.user = response;
        this.teamId = this.user.team.id;
      },
      (error) => {
        console.log(error);
      }
    );
    this.apexDonutChart = apexDonutChart();
    this.apexDonutChart2 = apexDonutChart2();

    this.leaveStatusBarGraphConfig = leaveStatusBarGraphConfig();
  }

  ngOnInit(): void {
    this.role = this.currentUser.role;
    this.getUsers();
    this.getAllUsers();
    this.getExternalAuthorizations();
    this.getTeamLeaves();
    this.getLeaves();
    this.getTeams();
    this.getDepartments();
  }

  getExternalAuthorizations() {
    const token = localStorage.getItem("token");
    const currentUserEmail = this.jwtHelperService.decodeToken(token).email;
    const currentUserRole = this.jwtHelperService.decodeToken(token).role;
    if (currentUserRole === "USER") {
      this.externalAuthorizationService
        .getExternalAuthorizationsByUser(currentUserEmail)
        .subscribe(
          (response) => {
            this.externalAuthorizationData = response;
            this.externalAuthorizationRows = response;
            this.leaveChartData();
          },
          (error) => {
            console.log(error);
          }
        );
    } else if (currentUserRole === "MANAGER") {
      this.externalAuthorizationService
        .getExternalAuthorizationsByManager(currentUserEmail)
        .subscribe(
          (response) => {
            this.externalAuthorizationData = response;
            this.externalAuthorizationRows = response;
            this.leaveChartData();
          },
          (error) => {
            console.log(error);
          }
        );
    } else {
      this.externalAuthorizationService
        .getAllExternalAuthorizations()
        .subscribe(
          (response) => {
            this.externalAuthorizationData = response;
            this.externalAuthorizationRows = response;
            this.leaveChartData();
          },
          (error) => {
            console.log(error);
          }
        );
    }
  }

  getTeamLeaves() {
    const token = localStorage.getItem("token");
    const currentUserEmail = this.jwtHelperService.decodeToken(token).email;
    const currentUserRole = this.jwtHelperService.decodeToken(token).role;
    const currentUserId = this.jwtHelperService.decodeToken(token).id;
    if (currentUserRole === "MANAGER") {
      this.userService.getUserByEmail(currentUserEmail).subscribe(
        (response) => {
          this.manager = response;

          this.teamLeaveService
            .getTeamLeavesForTeam(this.manager.team.id)
            .subscribe(
              (response) => {
                this.teamLeaveData = response;
                this.teamLeaveRows = response;
                this.leaveChartData();
              },
              (error) => {
                console.log(error);
              }
            );
        },
        (error) => {
          console.log(error);
        }
      );
      this.teamLeaveService
        .getTeamLeavesForTeam(this.manager.team.id)
        .subscribe(
          (response) => {
            this.teamLeaveData = response;
            this.teamLeaveRows = response;
            this.leaveChartData();
          },
          (error) => {
            console.log(error);
          }
        );
    } else if (currentUserRole === "USER") {
      this.teamLeaveService.getTeamLeaveById(currentUserId).subscribe(
        (response) => {
          this.teamLeaveData = response;
          this.teamLeaveRows = response;
          this.leaveChartData();
        },
        (error) => {
          console.log(error);
        }
      );
    } else {
      this.teamLeaveService.getAllTeamLeaves().subscribe(
        (response) => {
          this.teamLeaveData = response;
          this.teamLeaveRows = response;
          this.leaveChartData();
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }

  getLeaves() {
    const token = localStorage.getItem("token");
    const currentUserEmail = this.jwtHelperService.decodeToken(token).email;
    const currentUserRole = this.jwtHelperService.decodeToken(token).role;
    if (currentUserRole === "MANAGER") {
      this.leaveService.getLeaveRequestsByManager(currentUserEmail).subscribe(
        (response) => {
          this.leavesData = response;
          this.leaveCount = this.leavesData.length;
          this.leaveRows = this.leavesData;
          this.leaveChartData();
        },
        (error) => {
          console.log(error);
        }
      );
    }
    if (currentUserRole === "ADMIN") {
      this.leaveService.getAllLeaveRequests().subscribe(
        (response) => {
          this.leavesData = response;
          this.leaveRows = this.leavesData;
          this.leaveChartData();
        },
        (error) => {
          console.log(error);
        }
      );
    }
    if (currentUserRole === "USER") {
      this.leaveService
        .getLeaveRequestsByUserId(this.jwtHelperService.decodeToken(token).id)
        .subscribe(
          (response) => {
            this.leavesData = response;
            this.leaveRows = this.leavesData;
            this.leaveCount = this.leavesData.length;
            this.leaveChartData();
          },
          (error) => {
            console.log(error);
          }
        );
    }
  }

  getUsers() {
    if (this.role === "ADMIN") {
      this.userService.getAllUsers().subscribe(
        (response) => {
          this.usersData = response;
          this.saveChartData();
          this.usersOnLave = this.usersData.filter((row) => {
            return row.onLeave;
          });

          console.log("adminCount", this.adminCount);
        },
        (error) => {
          console.log(error);
        }
      );
    } else {
      let currentUser: User = {};
      let idOfTeam: number;
      this.userService.getUserByEmail(this.currentUser.email).subscribe(
        (response) => {
          currentUser = response;
          idOfTeam = currentUser.team.id;
          console.log("teamId", this.teamId);
          this.membersOnLeave = response.team.members.filter(
            (member) => member.onLeave === true
          );
          this.teamService.getMembersOfTeam(idOfTeam).subscribe(
            (response) => {
              this.usersData = response;
              this.usersOnLave = this.usersData.filter((row) => {
                return row.onLeave;
              });
            },
            (error) => {
              console.log(error);
            }
          );
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }

  getTeams() {
    if (this.role === "ADMIN") {
      this.teamService.getAllTeams().subscribe(
        (response) => {
          this.teamData = response;
          this.teamRows = response;
          this.saveChartData();
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }

  getDepartments() {
    if (this.role === "ADMIN") {
      this.departmentService.getAllOrganizationalUnits().subscribe(
        (response) => {
          this.departmentData = response;
          this.departmentRows = response;
          this.saveChartData();
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }

  saveChartData() {
    if (this.role === "ADMIN") {
      console.log("my data", this.leavesRequest);
      this.teamChart = this.teamData.length;
      console.log("team", this.teamChart);
      this.depChart = this.departmentData.length;
      console.log("departmentData", this.depChart);
      this.userChart = this.usersData.length;
      console.log("zzzzzzz", this.userChart);
      this.managerCount = this.usersData.filter((row) => {
        return row.role === "MANAGER";
      }).length;

      this.adminCount = this.usersData.filter((row) => {
        return row.role === "ADMIN";
      }).length;
      this.apexDonutChart = apexDonutChart(
          this.teamChart,
          this.depChart,
          this.userChart,
      );
    }
  }

  leaveChartData() {
    if (this.role === "ADMIN") {
      this.leaveChart = this.leavesData.length;
      console.log("test", this.leaveChart);
      this.teamLeaveChart = this.teamLeaveData.length;
      console.log("test", this.teamLeaveChart);
      this.externalAuthorizationDataChart =
        this.externalAuthorizationData.length;
      console.log("test", this.externalAuthorizationDataChart);
      this.usersOnLave = this.usersData.filter((row) => {
        return row.onLeave;
      });
      this.usersOnLaveChart = this.usersOnLave.length;
      console.log("on leave", this.usersOnLave);
      this.apexDonutChart2Done = true;
      this.apexDonutChart2 = apexDonutChart2(
        this.leaveChart,
        this.teamLeaveChart,
        this.externalAuthorizationDataChart,
        this.usersOnLaveChart
      );
    }
  }

  ngAfterViewInit() {
    // Subscribe to core config changes
    this._coreConfigService.getConfig().subscribe((config) => {
      // If Menu Collapsed Changes
      if (
        (config.layout.menu.collapsed === true ||
          config.layout.menu.collapsed === false) &&
        localStorage.getItem("currentUser")
      ) {
        setTimeout(() => {
          if (this.currentUser.role == "Admin") {
            // Get Dynamic Width for Charts
            this.isMenuToggled = true;
            this.apexDonutChart.chart.width =
              this.apexDonutChartRef?.nativeElement.offsetWidth;
            this.apexDonutChart2.chart.width =
              this.apexDonutChartRef2?.nativeElement.offsetWidth;
          }
        }, 2000);
      }
    });
  }

  filterUpdate(event) {
    this.tempUserData = this.usersData;
    // Reset role and status filters
    this.previousRoleFilter = "";
    this.previousStatusFilter = "";

    // Get the search value
    const searchValue = event.target.value.toLowerCase();

    // Check if the search value is empty
    if (searchValue === "") {
      // If empty, reset to the original data
      this.rows = [...this.tempUserData];
    } else {
      // Apply the search filter to the original data
      this.rows = this.tempUserData.filter((row) => {
        // Check if the property is not null before calling toLowerCase()
        return (
          row.firstName && row.firstName.toLowerCase().includes(searchValue)
        );
      });
    }

    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  rowDetailsToggleExpand(row: any) {
    this.tableRowDetails.rowDetail.toggleExpandRow(row);
  }

  openModal() {
    this.modalService.open(this.content, {
      size: "md",
      centered: true,
      windowClass: "modal modal-primary",
    });
  }

  close(id: string) {
    this.modalService.dismissAll(id);
  }

  affectUserToTeam() {
    this.teamService.addUserToTeam(this.email, this.user.team.name).subscribe(
      (data) => {
        this.toastr.success("User added successfully", "Success", {
          timeOut: 3000,
          closeButton: true,
          progressBar: true,
          toastClass: "toast ngx-toastr toast-success",
          positionClass: "toast-top-right",
          titleClass: "toast-title",
          messageClass: "toast-message",
          tapToDismiss: true,
        });
        this.getUsers();
        this.close("addUserToTeam");
        this.email = "";
      },
      (err) => {
        if (err.error === "OK") {
          this.toastr.success("User added successfully", "Success", {
            timeOut: 3000,
            closeButton: true,
            progressBar: true,
            toastClass: "toast ngx-toastr toast-success",
            positionClass: "toast-top-right",
            titleClass: "toast-title",
            messageClass: "toast-message",
            tapToDismiss: true,
          });
          this.getUsers();
          this.close("addUserToTeam");
          this.email = "";
        }
        this.toastr.error(err.error.message, "Error", {
          timeOut: 3000,
          closeButton: true,
          progressBar: true,
          toastClass: "toast ngx-toastr toast-error",
          positionClass: "toast-top-right",
          titleClass: "toast-title",
          messageClass: "toast-message",
        });
      }
    );
  }

  removeFromTeam(email: string) {
    Swal.fire({
      title: "Confirm",
      text: "Are you sure you want to delete this item? You will not be able to recover it!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
      customClass: {
        confirmButton: "btn btn-primary",
        cancelButton: "btn btn-danger ml-1",
      },
    }).then((result) => {
      if (result.isConfirmed) {
        this.teamService
          .removeUserFromTeam(email, this.user.team.name)
          .subscribe(
            (data) => {
              this.toastr.success("User removed successfully", "Success", {
                timeOut: 3000,
                closeButton: true,
                progressBar: true,
                toastClass: "toast ngx-toastr toast-success",
                positionClass: "toast-top-right",
                titleClass: "toast-title",
                messageClass: "toast-message",
                tapToDismiss: true,
              });
              this.getUsers();
            },
            (err) => {
              this.toastr.error(err.error.message, "Error", {
                timeOut: 3000,
                closeButton: true,
                progressBar: true,
              });
            }
          );
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire("Cancelled", "User is safe :)", "error");
      }
    });
  }

  getAllUsers() {
    this.userService.getAllUsers().subscribe(
      (response) => {
        this.allUsers = response;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  onUserEmailChange(selectedItem: any) {
    this.email = selectedItem.email;
  }

  getLeaveRequests() {
    if (this.role === "ADMIN") {
      this.teamService.leaveRequests().subscribe(
        (response) => {
          this.leavesRequest = response;

          this.saveChartData();
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }
}
