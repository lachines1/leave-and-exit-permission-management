export const leaveTrendLineChartConfig = (seriesData: number[]) => {
    return {
        series: [{
            name: 'Monthly Leaves',
            data: seriesData
        }],
        chart: {
            height: 250,
            type: 'line',
            toolbar: {
                show: false
            }
        },
        colors: ['#FF9999'],
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: '50%',
                endingShape: 'rounded'
            },
        },
        xaxis: {
            categories: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

        },
        yaxis: {
            title: {
                text: 'Number of Leave Status'
            }
        },
        legend: {
            show: true,
            position: 'bottom',
            horizontalAlign: 'center',
            fontSize: '14px',
            markers: {
                width: 16,
                height: 16,
                strokeWidth: 0
            },
            itemMargin: {
                horizontal: 8,
                vertical: 8
            }
        },
        responsive: [{
            breakpoint: 480,
            options: {
                chart: {
                    height: 300
                },
                legend: {
                    position: 'bottom'
                }
            }
        }]
    };
};
