export const employeeLeaveChart = (data: any) => {
    const employeeOnLeave = [(data.totalEmployeesOnLeave / data.totalEmployees) * 100];
    //(0 / 8) * 100

    return {
        chart: {
            height: 280,
            type: "radialBar"
        },

        series: employeeOnLeave,

        plotOptions: {
            radialBar: {
                hollow: {
                    margin: 0,
                    size: "70%"
                },

                dataLabels: {
                    showOn: "always",
                    name: {
                        offsetY: 0,
                        show: true,
                        color: "#888",
                        fontSize: "13px"
                    },
                    value: {
                        color: "#111",
                        fontSize: "30px",
                        show: true
                    }
                }
            }
        },

        stroke: {
            lineCap: "round",
        },
        labels: [""]
    };
};
