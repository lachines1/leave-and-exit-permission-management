import { chartColors } from "./chartColors";

const LeaveChart = 0;
const TeamLeaveChart = 0;
const ExternalAuthorizationDataChart = 0;
const UsersOnLaveChart = 0;
const labels1 = "Leave Requests";
const labels2 = "Team Leave Requests";
const labels3 = "External Authorization Requests";
export const apexDonutChart2 = ( leaveChart?, teamLeaveChart?, externalAuthorizationDataChart?) => ({
    series: [
        leaveChart ? leaveChart: LeaveChart,
        teamLeaveChart ? teamLeaveChart: TeamLeaveChart,
        externalAuthorizationDataChart ? externalAuthorizationDataChart: ExternalAuthorizationDataChart,


    ],
    chart: {
        height: 350,
        type: "donut",
    },
    colors: [

        chartColors.donut.series5,
        '#C70039' ,
        '#FF5733',
        "#6495ED",
    ],
    plotOptions: {
        pie: {
            donut: {
                labels: {
                    show: true,
                    name: {
                        fontSize: "2rem",
                        fontFamily: "Montserrat",
                    },
                    value: {
                        fontSize: "1rem",
                        fontFamily: "Montserrat",
                        formatter: function (val) {
                            return parseInt(val) + "";
                        },
                    },
                },
            },
        },
    },
    legend: {
        show: true,
        position: "bottom",
    },
    labels: [labels1,labels2,labels3],
    responsive: [
        {
            breakpoint: 480,
            options: {
                chart: {
                    height: 300,
                },
                legend: {
                    position: "bottom",
                },
            },
        },
    ],
});
