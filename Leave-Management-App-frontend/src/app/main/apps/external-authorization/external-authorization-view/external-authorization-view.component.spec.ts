import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ExternalAuthorizationViewComponent} from './external-authorization-view.component';

describe('ExternalAuthorizationViewComponent', () => {
    let component: ExternalAuthorizationViewComponent;
    let fixture: ComponentFixture<ExternalAuthorizationViewComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ExternalAuthorizationViewComponent]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(ExternalAuthorizationViewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
