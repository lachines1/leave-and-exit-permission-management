import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ExternalAuthorizationService} from "../../../../shared/services/external-authorization.service";
import {ToastrService} from "ngx-toastr";
import {JwtHelperService} from "@auth0/angular-jwt";
import {ActivatedRoute, Router} from "@angular/router";
import {ExternalAuthorizationRequest} from "../../../../shared/models/external-authorization-request.model";
import {UserListService} from "../../user/user-list/user-list.service";

@Component({
    selector: 'app-external-authorization-edit',
    templateUrl: './external-authorization-edit.component.html',
    styleUrls: ['./external-authorization-edit.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ExternalAuthorizationEditComponent implements OnInit {

    public durations = [
        {name: '30Mn', value: 'ThirtyMinutes'},
        {name: '1H', value: 'OneHour'},
        {name: '1H30', value: 'NinetyMinutes'},
        {name: '2H', value: 'TwoHours'},
    ]
    contentHeader: object;
    public externalAuthorizationRequest: ExternalAuthorizationRequest = {};
    users: any[];
    reason: any;
    protected date: Date;
    protected userEmail: string;
    protected leaveDuration: any;
    private externalAuthorizationId: number;

    constructor(
        private externalAuthorizationService: ExternalAuthorizationService,
        private toastr: ToastrService,
        private jwtService: JwtHelperService,
        private userService: UserListService,
        private route: ActivatedRoute,
        private router: Router) {
        this.route.params.subscribe(params => {
            this.externalAuthorizationId = params['id'];
            this.getExternalAuthorizationById(this.externalAuthorizationId);
        });
    }

    ngOnInit(): void {
        this.getUsers();
        this.contentHeader = {
            headerTitle: 'Edit Exit Permission Request',
            actionButton: true,
            breadcrumb: {
                type: '',
                links: [
                    {
                        name: 'Home',
                        isLink: true,
                        link: '/'
                    },
                    {
                        name: 'Exit Permission Requests',
                        isLink: true,
                        link: '/apps/external-authorization/list'
                    },
                    {
                        name: 'Edit Exit Permission Request',
                        isLink: false,
                        link: '/'
                    }

                ]
            }
        };
    }

    onDurationChange(selectedItem: any) {
        this.leaveDuration = selectedItem.value;
    }

    editExternalAuthorization() {
        this.route.params.subscribe(params => {
            this.externalAuthorizationId = params['id'];
            this.getExternalAuthorizationById(this.externalAuthorizationId);
        });
        this.externalAuthorizationRequest.date = this.date;
        this.externalAuthorizationRequest.leaveDuration = this.leaveDuration;
        this.externalAuthorizationRequest.userEmail = this.userEmail;
        this.externalAuthorizationRequest.reason = this.reason;
        console.log(this.externalAuthorizationRequest);
        this.externalAuthorizationService.editExternalAuthorization(this.externalAuthorizationId, this.externalAuthorizationRequest).subscribe(
            res => {
                this.toastr.success('External Authorization Request updated successfully', 'Success', {
                    timeOut: 3000,
                    closeButton: true,
                    progressBar: true,
                    toastClass: 'toast ngx-toastr success'

                });
                this.router.navigate(['/apps/external-authorization/list']);
            },
            error => {
                this.toastr.error(error.error.message, 'Error', {
                    timeOut: 3000,
                    closeButton: true,
                    progressBar: true,
                    toastClass: 'toast ngx-toastr error'

                });
            }
        );

    }

    onEmailChange(selectedItem: any) {
        this.userEmail = selectedItem.email;
    }

    getUsers() {
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtService.decodeToken(token).email;
        const role = this.jwtService.decodeToken(token).role;
        if (role === 'ADMIN') {
            this.userService.getAllUsers().subscribe(
                (data) => {
                    this.users = data;
                },
                (error) => {
                    this.toastr.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx-toastr'
                    });
                });
        } else if (role === 'MANAGER') {
            this.userService.getUsersByManager(currentUserEmail).subscribe(
                (data) => {
                    this.users = data;
                },
                (error) => {
                    this.toastr.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx-toastr'
                    });
                });
        } else {
            this.userService.getUserByEmail(currentUserEmail).subscribe(
                (data) => {
                    this.users = [data];
                },
                (error) => {
                    this.toastr.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx-toastr'
                    });
                });
        }
    }

    private getExternalAuthorizationById(externalAuthorizationId: number) {
        this.externalAuthorizationService.getExternalAuthorizationById(externalAuthorizationId).subscribe((res: any) => {
            this.externalAuthorizationRequest = res;
            console.log(this.externalAuthorizationRequest);
            this.date = new Date(res.date);
            this.userEmail = res.userEmail;
            this.leaveDuration = res.leaveDuration;
            this.reason = res.reason;
        });
    }
}
