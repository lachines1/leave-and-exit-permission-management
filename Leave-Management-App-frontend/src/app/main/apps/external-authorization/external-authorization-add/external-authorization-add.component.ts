import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ExternalAuthorizationService} from "../../../../shared/services/external-authorization.service";
import {ToastrService} from "ngx-toastr";
import {ExternalAuthorizationRequest} from "../../../../shared/models/external-authorization-request.model";
import {JwtHelperService} from "@auth0/angular-jwt";
import {Router} from "@angular/router";
import {UserListService} from "../../user/user-list/user-list.service";


@Component({
    selector: 'app-external-authorization-add',
    templateUrl: './external-authorization-add.component.html',
    styleUrls: ['./external-authorization-add.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ExternalAuthorizationAddComponent implements OnInit {
    public durations = [
        {name: '30Mn', value: 'ThirtyMinutes'},
        {name: '1H', value: 'OneHour'},
        {name: '1H30', value: 'NinetyMinutes'},
        {name: '2H', value: 'TwoHours'},
    ]
    contentHeader: object;
    users: any[];
    reason: any;
    protected date: Date;
    protected userEmail: string;
    protected leaveDuration: any;
    private currentUser: any;

    constructor(private externalAuthorizationService: ExternalAuthorizationService, private userService: UserListService, private toastr: ToastrService, private jwtService: JwtHelperService, private router: Router) {
    }

    ngOnInit(): void {
        this.getUsers();
        const token = localStorage.getItem('token');
        this.currentUser = this.jwtService.decodeToken(token);
        this.userEmail = this.currentUser.email;
        this.contentHeader = {
            headerTitle: 'Create Exit Permission Request',
            actionButton: true,
            breadcrumb: {
                type: '',
                links: [
                    {
                        name: 'Home',
                        isLink: true,
                        link: '/'
                    },
                    {
                        name: 'Exit Permissions Requests',
                        isLink: true,
                        link: '/apps/external-authorization/list'
                    },
                    {
                        name: 'Create Exit Permission Request',
                        isLink: false,
                        link: '/'
                    }

                ]
            }
        };

    }

    addExternalAuthorization() {
        const externalAuthorizationRequest = new ExternalAuthorizationRequest();
        externalAuthorizationRequest.date = this.date;
        externalAuthorizationRequest.leaveDuration = this.leaveDuration;
        externalAuthorizationRequest.reason = this.reason;
        externalAuthorizationRequest.userEmail = this.userEmail;
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtService.decodeToken(token).email;

        console.log(externalAuthorizationRequest);
        this.externalAuthorizationService.createExternalAuthorization(currentUserEmail, externalAuthorizationRequest).subscribe(
            (data) => {
                this.toastr.success('External Authorization Request added successfully', 'Success', {
                    timeOut: 3000,
                    closeButton: true,
                    progressBar: true,
                    toastClass: 'toast ngx-toastr',
                });
                this.router.navigate(['/apps/external-authorization/list']);
            },
            (error) => {
                this.toastr.error('Error adding exit permission request', 'Error', {
                    timeOut: 3000,
                    closeButton: true,
                    progressBar: true,
                    toastClass: 'toast ngx-toastr',
                });
            }
        );
    }

    onDurationChange(selectedItem: any) {
        this.leaveDuration = selectedItem.value;
    }

    getUsers() {
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtService.decodeToken(token).email;
        const role = this.jwtService.decodeToken(token).role;
        if (role === 'ADMIN') {
            this.userService.getAllUsers().subscribe(
                (data) => {
                    this.users = data;
                },
                (error) => {
                    this.toastr.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx-toastr'
                    });
                });
        } else if (role === 'MANAGER') {
            this.userService.getUsersByManager(currentUserEmail).subscribe(
                (data) => {
                    this.users = data;
                },
                (error) => {
                    this.toastr.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx-toastr'
                    });
                });
        } else {
            this.userService.getUserByEmail(currentUserEmail).subscribe(
                (data) => {
                    this.users = [data];
                },
                (error) => {
                    this.toastr.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx-toastr'
                    });
                });
        }
    }

    onEmailChange(selectedItem: any) {
        this.userEmail = selectedItem.email;
    }

}
