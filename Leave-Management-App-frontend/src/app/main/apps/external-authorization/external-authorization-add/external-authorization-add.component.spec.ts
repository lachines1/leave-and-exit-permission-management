import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ExternalAuthorizationAddComponent} from './external-authorization-add.component';

describe('ExternalAuthorizationAddComponent', () => {
    let component: ExternalAuthorizationAddComponent;
    let fixture: ComponentFixture<ExternalAuthorizationAddComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ExternalAuthorizationAddComponent]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(ExternalAuthorizationAddComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
