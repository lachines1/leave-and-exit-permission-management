import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ExternalAuthorizationListComponent} from './external-authorization-list.component';

describe('ExternalAuthorizationListComponent', () => {
    let component: ExternalAuthorizationListComponent;
    let fixture: ComponentFixture<ExternalAuthorizationListComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ExternalAuthorizationListComponent]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(ExternalAuthorizationListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
