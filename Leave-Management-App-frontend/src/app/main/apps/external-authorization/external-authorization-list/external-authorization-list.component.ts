import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ColumnMode, DatatableComponent} from "@swimlane/ngx-datatable";
import {ExternalAuthorizationService} from "../../../../shared/services/external-authorization.service";
import {JwtHelperService} from "@auth0/angular-jwt";
import {ExternalAuthorization} from "../../../../shared/models/external-authorization.model";
import {ToastrService} from "ngx-toastr";
import Swal from "sweetalert2";

@Component({
    selector: 'app-external-authorization-list',
    templateUrl: './external-authorization-list.component.html',
    styleUrls: ['./external-authorization-list.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ExternalAuthorizationListComponent implements OnInit {
    public contentHeader: object;
    basicSelectedOption: number = 10;
    public rows: any;
    data: ExternalAuthorization[];
    public ColumnMode = ColumnMode;
    public currentUser: any;
    public currentUserEmail: any;
    public role: string;
    selectDuration = [
        {name: '30Mn', value: 'ThirtyMinutes'},
        {name: '1H', value: 'OneHour'},
        {name: '1H30', value: 'NinetyMinutes'},
        {name: '2H', value: 'TwoHours'},
    ];
    public duration: any;
    public selectedDuration: any;
    public selectedStatus: any;
    public selectStatus = [
        {name: 'Accepted', value: 'ACCEPTED'},
        {name: 'Rejected', value: 'REJECTED'},
        {name: 'Pending', value: 'PENDING'},
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;
    private tempFilterData: any;
    private previousStatusFilter: string;
    private previousDurationFilter: string;
    private tempData: ExternalAuthorization[];


    constructor(private externalAuthorizationService: ExternalAuthorizationService, private jwtHelperService: JwtHelperService, private toastr: ToastrService) {

    }

    ngOnInit(): void {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'))
        this.currentUserEmail = this.jwtHelperService.decodeToken(localStorage.getItem('token')).email;
        console.log('hello ' + this.currentUserEmail)
        this.role = this.currentUser.role
        this.contentHeader = {
            headerTitle: 'Exit Permissions',
            actionButton: true,
            breadcrumb: {
                type: '',
                links: [
                    {
                        name: 'Home',
                        isLink: true,
                        link: '/'
                    },
                    {
                        name: 'Exit Permissions',
                        isLink: true,
                        link: '/apps/external-authorization/list'
                    },
                    {
                        name: 'Exit Permissions Lists',
                        isLink: false
                    }
                ]
            }
        };
        this.getExternalAuthorizations();
    }

    filterUpdate(event) {
        this.tempData = this.data;
        // Reset role and status filters

        // Get the search value
        const searchValue = event.target.value.toLowerCase();

        // Check if the search value is empty
        if (searchValue === '') {
            // If empty, reset to the original data
            this.rows = [...this.tempData];
        } else {
            // Apply the search filter to the original data
            this.rows = this.tempData.filter(row => {
                // Check if the property is not null before calling toLowerCase()
                return row.user.firstName && row.user.firstName.toLowerCase().includes(searchValue);
            });
        }

        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    getExternalAuthorizations() {
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtHelperService.decodeToken(token).email;
        const currentUserRole = this.jwtHelperService.decodeToken(token).role;
        if (currentUserRole === 'USER') {
            this.externalAuthorizationService.getExternalAuthorizationsByUser(currentUserEmail).subscribe(
                (response) => {
                    this.data = response;
                    this.rows = response;
                    console.log(this.data);
                },
                (error) => {
                    console.log(error);
                }
            );
        } else if (currentUserRole === 'MANAGER') {
            this.externalAuthorizationService.getExternalAuthorizationsByManager(currentUserEmail).subscribe(
                (response) => {
                    this.data = response;
                    this.rows = response;
                    console.log(this.data);

                },
                (error) => {
                    console.log(error);
                }
            );
        } else {
            this.externalAuthorizationService.getAllExternalAuthorizations().subscribe(
                (response) => {
                    this.data = response;
                    this.rows = response;
                    console.log(this.data);

                },
                (error) => {
                    console.log(error);
                }
            );
        }
    }

    // deleteExternalAuthorization(id: number) {
    //     this.externalAuthorizationService.deleteExternalAuthorization(id).subscribe(
    //         (response) => {
    //             this.toastr.success('External Authorization deleted successfully', 'Success', {
    //                 progressBar: true,
    //                 toastClass: 'toast ngx-toastr',
    //                 closeButton: true
    //
    //             });
    //             this.getExternalAuthorizations();
    //         },
    //         (error) => {
    //             this.toastr.error('External Authorization could not be deleted', 'Error', {
    //                 progressBar: true,
    //                 toastClass: 'toast ngx-toastr',
    //                 closeButton: true
    //             });
    //             console.log(error);
    //         }
    //     );
    // }

    deleteExternalAuthorization(id: number): void {
        Swal.fire({
            title: 'Confirm',
            text: 'Are you sure you want to delete this item? You will not be able to recover it!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-danger ml-1'
            }
        }).then((result) => {
            if (result.isConfirmed) {
                this.externalAuthorizationService.deleteExternalAuthorization(id).subscribe(
                    () => {

                        Swal.fire({
                            title: 'Deleted!',
                            text: 'Your file has been deleted.',
                            icon: 'success',
                            customClass: {
                                confirmButton: 'btn btn-success'
                            }
                        });
                        this.getExternalAuthorizations();
                    },
                    (error) => {
                        // Error
                        // ... your error logic
                        console.log(error);
                        Swal.fire('Error', 'An error occurred while deleting the item.', 'error');
                    }
                );
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal.fire({
                    title: 'Cancelled',
                    text: 'The external authorization was not deleted.',
                    icon: 'error',
                    customClass: {
                        confirmButton: 'btn btn-success'
                    }
                });
            }
        });
    }


    acceptRequest(id: number) {
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtHelperService.decodeToken(token).email;
        this.externalAuthorizationService.treatExternalAuthorization(id, 'ACCEPTED', currentUserEmail)
            .subscribe((response) => {
                    this.toastr.success(`External Authorization with id: ${id} accepted`, `External Authorization accepted'`, {
                        positionClass: 'toast-top-right',
                        progressBar: true,
                        progressAnimation: 'increasing',
                        closeButton: true,
                        toastClass: 'toast ngx-toastr',
                    });
                    this.getExternalAuthorizations();
                }, (error) => {
                    this.toastr.error(`External Authorization with id: ${id} could not be accepted`, `Error`, {
                        positionClass: 'toast-top-right',
                        progressBar: true,
                        progressAnimation: 'increasing',
                        closeButton: true,
                        toastClass: 'toast ngx-toastr',
                    });
                }
            );
    }

    rejectRequest(id: number) {
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtHelperService.decodeToken(token).email;
        this.externalAuthorizationService.treatExternalAuthorization(id, 'REJECTED', currentUserEmail)
            .subscribe((response) => {
                    this.toastr.success(`External Authorization with id: ${id} rejected`, `External Authorization rejected'`, {
                        positionClass: 'toast-top-right',
                        progressBar: true,
                        progressAnimation: 'increasing',
                        closeButton: true,
                        toastClass: 'toast ngx-toastr',
                    });
                    this.getExternalAuthorizations();
                }, (error) => {
                    this.toastr.error(`External Authorization with id: ${id} could not be rejected`, `Error`, {
                        positionClass: 'toast-top-right',
                        progressBar: true,
                        progressAnimation: 'increasing',
                        closeButton: true,
                        toastClass: 'toast ngx-toastr',
                    });
                    this.getExternalAuthorizations();
                }
            );
    }

    filterByDuration(event: any) {
        const filter = event ? event.value.toLowerCase() : '';
        this.previousDurationFilter = filter;
        this.tempFilterData = this.data.filter(row => {
            return row.leaveDuration.toLowerCase().indexOf(filter) !== -1 || !filter;
        });
        this.rows = this.tempFilterData;
        this.table.offset = 0;
    }

    filterByStatus(event: any) {
        const filter = event ? event.value.toLowerCase() : '';
        this.previousStatusFilter = filter;
        this.tempFilterData = this.data.filter(row => {
            return row.status.toLowerCase().indexOf(filter) !== -1 || !filter;
        });
        this.rows = this.tempFilterData;
        this.table.offset = 0;
    }

    modalOpenVC(id) {

    }
}
