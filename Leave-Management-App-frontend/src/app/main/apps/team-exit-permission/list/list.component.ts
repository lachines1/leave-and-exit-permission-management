import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ColumnMode, DatatableComponent} from "@swimlane/ngx-datatable";
import {JwtHelperService} from "@auth0/angular-jwt";
import {ToastrService} from "ngx-toastr";
import Swal from "sweetalert2";
import {TeamExitPermissionService} from "../../../../shared/services/team-exit-permission.service";
import {UserListService} from "../../user/user-list/user-list.service";
import {TeamExitPermission} from "../../../../shared/models/team-exit-permission.model";

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ListComponent implements OnInit {

    public contentHeader: object;
    basicSelectedOption: number = 10;
    public rows: any;
    data: any[];
    public ColumnMode = ColumnMode;
    public currentUser: any;
    public role: string;
    selectDuration = [
        {name: '30Mn', value: 'ThirtyMinutes'},
        {name: '1H', value: 'OneHour'},
        {name: '1H30', value: 'NinetyMinutes'},
        {name: '2H', value: 'TwoHours'},
    ];
    public duration: any;
    public selectedDuration: any;
    public selectedStatus: any;
    public selectStatus = [
        {name: 'Accepted', value: 'ACCEPTED'},
        {name: 'Rejected', value: 'REJECTED'},
        {name: 'Pending', value: 'PENDING'},
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;
    private tempFilterData: any;
    private previousStatusFilter: string;
    private previousDurationFilter: string;
    private tempData: TeamExitPermission[];


    constructor(private teamExitPermissionService: TeamExitPermissionService, private jwtHelperService: JwtHelperService, private toastr: ToastrService, private userService: UserListService) {
    }

    ngOnInit(): void {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'))
        this.role = this.currentUser.role
        this.contentHeader = {
            headerTitle: 'Team Exit Permissions',
            actionButton: true,
            breadcrumb: {
                type: '',
                links: [
                    {
                        name: 'Home',
                        isLink: true,
                        link: '/'
                    },
                    {
                        name: 'TeamExit Permissions',
                        isLink: true,
                        link: '/apps/team-exit-permission/list'
                    },
                    {
                        name: 'Team Exit Permissions List',
                        isLink: false
                    }
                ]
            }
        };
        this.getTeamExitPermissions();
    }

    filterUpdate(event) {
        this.tempData = this.data;
        // Reset role and status filters

        // Get the search value
        const searchValue = event.target.value.toLowerCase();

        // Check if the search value is empty
        if (searchValue === '') {
            // If empty, reset to the original data
            this.rows = [...this.tempData];
        } else {
            // Apply the search filter to the original data
            this.rows = this.tempData.filter(row => {
                // Check if the property is not null before calling toLowerCase()
                return row.team.name && row.team.name.toLowerCase().includes(searchValue);
            });
        }

        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    getTeamExitPermissions() {
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtHelperService.decodeToken(token).email;
        const currentUserRole = this.jwtHelperService.decodeToken(token).role;
        if (currentUserRole === 'USER') {
            const teamName = this.currentUser.team.name;
            this.teamExitPermissionService.getTeamExitPermissionsByUser(currentUserEmail).subscribe(
                (response) => {
                    this.data = response;
                    this.rows = response;
                },
                (error) => {
                    console.log(error);
                }
            );
        } else if (currentUserRole === 'MANAGER') {
            this.teamExitPermissionService.getTeamExitPermissionsByManager(currentUserEmail).subscribe(
                (response) => {
                    this.data = response;
                    this.rows = response;
                },
                (error) => {
                    console.log(error);
                }
            );
        } else {
            this.teamExitPermissionService.getAllTeamExitPermissions().subscribe(
                (response) => {
                    this.data = response;
                    this.rows = response;
                },
                (error) => {
                    console.log(error);
                }
            );
        }
    }

    deleteExternalAuthorization(id: number): void {
        Swal.fire({
            title: 'Confirm',
            text: 'Are you sure you want to delete this item? You will not be able to recover it!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-danger ml-1'
            }
        }).then((result) => {
            if (result.isConfirmed) {
                this.teamExitPermissionService.deleteTeamExitPermissionById(id).subscribe(
                    () => {

                        Swal.fire({
                            title: 'Deleted!',
                            text: 'Your Team Exit Permission has been deleted.',
                            icon: 'success',
                            customClass: {
                                confirmButton: 'btn btn-success'
                            }
                        });
                        this.getTeamExitPermissions();
                    },
                    (error) => {
                        // Error
                        // ... your error logic
                        console.log(error);
                        Swal.fire('Error', 'An error occurred while deleting the item.', 'error');
                    }
                );
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal.fire({
                    title: 'Cancelled',
                    text: 'The Team Exit Permission was not deleted.',
                    icon: 'error',
                    customClass: {
                        confirmButton: 'btn btn-success'
                    }
                });
            }
        });
    }


    acceptRequest(id: number) {
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtHelperService.decodeToken(token).email;
        this.teamExitPermissionService.treatTeamExitPermission(id, 'ACCEPTED', currentUserEmail)
            .subscribe((response) => {
                    this.toastr.success(`Team Exit Permission with id: ${id} accepted`, `Team Exit Permission accepted'`, {
                        positionClass: 'toast-top-right',
                        progressBar: true,
                        progressAnimation: 'increasing',
                        closeButton: true,
                        toastClass: 'toast ngx-toastr',
                    });
                    this.getTeamExitPermissions();
                }, (error) => {
                    this.toastr.error(`Team Exit Permission with id: ${id} could not be accepted`, `Error`, {
                        positionClass: 'toast-top-right',
                        progressBar: true,
                        progressAnimation: 'increasing',
                        closeButton: true,
                        toastClass: 'toast ngx-toastr',
                    });
                }
            );
    }

    rejectRequest(id: number) {
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtHelperService.decodeToken(token).email;
        this.teamExitPermissionService.treatTeamExitPermission(id, 'REJECTED', currentUserEmail)
            .subscribe((response) => {
                    this.toastr.success(`Team Exit Permission with id: ${id} rejected`, `Team Exit Permission rejected'`, {
                        positionClass: 'toast-top-right',
                        progressBar: true,
                        progressAnimation: 'increasing',
                        closeButton: true,
                        toastClass: 'toast ngx-toastr',
                    });
                    this.getTeamExitPermissions();
                }, (error) => {
                    this.toastr.error(`Team Exit Permission with id: ${id} could not be rejected`, `Error`, {
                        positionClass: 'toast-top-right',
                        progressBar: true,
                        progressAnimation: 'increasing',
                        closeButton: true,
                        toastClass: 'toast ngx-toastr',
                    });
                    this.getTeamExitPermissions();
                }
            );
    }

    filterByDuration(event: any) {
        const filter = event ? event.value.toLowerCase() : '';
        this.previousDurationFilter = filter;
        this.tempFilterData = this.data.filter(row => {
            return row.leaveDuration.toLowerCase().indexOf(filter) !== -1 || !filter;
        });
        this.rows = this.tempFilterData;
        this.table.offset = 0;
    }

    filterByStatus(event: any) {
        const filter = event ? event.value.toLowerCase() : '';
        this.previousStatusFilter = filter;
        this.tempFilterData = this.data.filter(row => {
            return row.status.toLowerCase().indexOf(filter) !== -1 || !filter;
        });
        this.rows = this.tempFilterData;
        this.table.offset = 0;
    }


}
