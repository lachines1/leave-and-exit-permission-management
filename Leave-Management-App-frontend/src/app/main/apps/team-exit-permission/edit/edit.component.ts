import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ToastrService} from "ngx-toastr";
import {JwtHelperService} from "@auth0/angular-jwt";
import {UserListService} from "../../user/user-list/user-list.service";
import {ActivatedRoute, Router} from "@angular/router";
import {TeamExitPermissionService} from "../../../../shared/services/team-exit-permission.service";
import {TeamService} from "../../../../shared/services/team.service";
import {TeamExitPermissionRequest} from "../../../../shared/models/team-exit-permission-request.model";

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class EditComponent implements OnInit {
    teamExitPermissionId: number;

    public durations = [
        {name: '30Mn', value: 'ThirtyMinutes'},
        {name: '1H', value: 'OneHour'},
        {name: '1H30', value: 'NinetyMinutes'},
        {name: '2H', value: 'TwoHours'},
    ]
    contentHeader: object;
    users: any[];
    reason: any;
    allTeams: any;
    teamName: string;
    public teamExitPermissionRequest: TeamExitPermissionRequest = {};
    protected date: Date;
    protected leaveDuration: any;
    private currentUser: any;

    constructor(private teamExitPermissionService: TeamExitPermissionService,
                private teamService: TeamService,
                private userService: UserListService,
                private toastr: ToastrService,
                private jwtService: JwtHelperService,
                private route: ActivatedRoute,
                private router: Router) {
        this.route.params.subscribe(params => {
            this.teamExitPermissionId = params['id'];
            this.getTeamExitPermissionById(this.teamExitPermissionId);
        });
    }

    ngOnInit(): void {
        this.getTeams();
        const token = localStorage.getItem('token');
        this.currentUser = this.jwtService.decodeToken(token);
        this.contentHeader = {
            headerTitle: 'Update Team Exit Permission Request',
            actionButton: true,
            breadcrumb: {
                type: '',
                links: [
                    {
                        name: 'Home',
                        isLink: true,
                        link: '/'
                    },
                    {
                        name: 'Team Exit Permissions Requests',
                        isLink: true,
                        link: '/apps/team-exit-permission/list'
                    },
                    {
                        name: 'Update Team Exit Permission Request',
                        isLink: false,
                        link: '/'
                    }

                ]
            }
        };

    }

    editTeamExitPermission() {
        this.route.params.subscribe(params => {
            this.teamExitPermissionId = params['id'];
            this.getTeamExitPermissionById(this.teamExitPermissionId);
        });
        this.teamExitPermissionRequest.date = this.date;
        this.teamExitPermissionRequest.leaveDuration = this.leaveDuration;
        this.teamExitPermissionRequest.teamName = this.teamName;
        this.teamExitPermissionRequest.reason = this.reason;
        console.log(this.teamExitPermissionRequest);
        this.teamExitPermissionService.editExternalAuthorization(this.teamExitPermissionId, this.teamExitPermissionRequest).subscribe(
            res => {
                this.toastr.success('Team Exit Permission Request updated successfully', 'Success', {
                    timeOut: 3000,
                    closeButton: true,
                    progressBar: true,
                    toastClass: 'toast ngx-toastr success'

                });
                this.router.navigate(['/apps/team-exit-permission/list']);
            },
            error => {
                this.toastr.error(error.error.message, 'Error', {
                    timeOut: 3000,
                    closeButton: true,
                    progressBar: true,
                    toastClass: 'toast ngx-toastr error'

                });
            }
        );

    }

    onDurationChange(selectedItem: any) {
        this.leaveDuration = selectedItem.value;
    }

    onTeamChange(selectedItem: any) {
        this.teamName = selectedItem.value;
        console.log('Selected Team:', this.teamName);
    }

    getTeams() {
        const role = this.jwtService.decodeToken(localStorage.getItem('token')).role;
        if (role === "ADMIN") {
            this.teamService.getAllTeams().subscribe(
                (data) => {
                    this.allTeams = data;
                    this.allTeams = data.map(team => {
                        // Add a new attribute 'value' with the same value as 'name'
                        return {...team, value: team.name};
                    });
                },
                (error) => {
                    this.toastr.error('Error fetching teams', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx-toastr',
                    });
                });
        } else if (role === "MANAGER") {
            const currentUserEmail = this.jwtService.decodeToken(localStorage.getItem('token')).email;
            this.teamService.getTeamsByManager(currentUserEmail).subscribe(
                (data) => {
                    this.allTeams = data;
                    this.allTeams = data.map(team => {
                        // Add a new attribute 'value' with the same value as 'name'
                        return {...team, value: team.name};
                    });
                },
                (error) => {
                    this.toastr.error('Error fetching teams', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx-toastr',
                    });
                });
        }

    }

    private getTeamExitPermissionById(teamExitPermissionId: number) {
        this.teamExitPermissionService.getTeamExitPermissionById(teamExitPermissionId).subscribe(
            (data) => {
                this.teamExitPermissionRequest = data;
                this.date = new Date(this.teamExitPermissionRequest.date);
                this.leaveDuration = this.teamExitPermissionRequest.leaveDuration;
                this.teamName = this.teamExitPermissionRequest.teamName;
                this.reason = this.teamExitPermissionRequest.reason;
            },
            (error) => {
                this.toastr.error('Error fetching team exit permission', 'Error', {
                    timeOut: 3000,
                    closeButton: true,
                    progressBar: true,
                    toastClass: 'toast ngx-toastr',
                });
            });

    }
}