import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { AddComponent } from './add/add.component';
import { ViewComponent } from './view/view.component';
import { EditComponent } from './edit/edit.component';
import {CardSnippetModule} from "../../../../@core/components/card-snippet/card-snippet.module";
import {ContentHeaderModule} from "../../../layout/components/content-header/content-header.module";
import {CoreDirectivesModule} from "../../../../@core/directives/directives";
import {CsvModule} from "@ctrl/ngx-csv";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgSelectModule} from "@ng-select/ng-select";
import {NgbDropdownModule} from "@ng-bootstrap/ng-bootstrap";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {RouterLinkWithHref, RouterModule} from "@angular/router";

const routes = [
    {
        path: 'list',
        component: ListComponent
    },
    {
        path: 'add',
        component: AddComponent
    },
    {
        path: 'view/:id',
        component: ViewComponent
    },
    {
        path: 'edit/:id',
        component: EditComponent
    },
    ]

@NgModule({
  declarations: [
    ListComponent,
    AddComponent,
    ViewComponent,
    EditComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CardSnippetModule,
    ContentHeaderModule,
    CoreDirectivesModule,
    CsvModule,
    FormsModule,
    NgSelectModule,
    NgbDropdownModule,
    NgxDatatableModule,
    ReactiveFormsModule,
    RouterLinkWithHref
  ]
})
export class TeamExitPermissionModule { }
