provider "aws" {
  region = "eu-west-3"
}

# Create a security group for the backend
resource "aws_security_group" "backend_sg" {
  name        = "backend-sg"
  description = "Security group for Spring Boot backend"

  ingress {
    from_port   = 8081
    to_port     = 8081
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# EC2 instance for the backend
resource "aws_instance" "backend" {
  ami           = "ami-0cff7528ff583bf9a"  # Amazon Linux 2 AMI (HVM), SSD Volume Type
  instance_type = "t2.micro"
  key_name      = "your-key-pair-name"
  security_groups = [aws_security_group.backend_sg.name]

  user_data = <<-EOF
              #!/bin/bash
              yum update -y
              amazon-linux-extras install -y java-openjdk17
              # Additional dependencies can be installed here
              EOF

  tags = {
    Name = "Spring Boot Backend"
  }
}

# Create a security group for the frontend
resource "aws_security_group" "frontend_sg" {
  name        = "frontend-sg"
  description = "Security group for Angular frontend"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# EC2 instance for the frontend
resource "aws_instance" "frontend" {
  ami           = "ami-0cff7528ff583bf9a"  # Same AMI as backend
  instance_type = "t2.micro"
  key_name      = "your-key-pair-name"
  security_groups = [aws_security_group.frontend_sg.name]

  user_data = <<-EOF
              #!/bin/bash
              yum update -y
              curl --silent --location https://rpm.nodesource.com/setup_14.x | bash -
              yum install -y nodejs
              npm install -g @angular/cli
              # Additional dependencies can be installed here
              EOF

  tags = {
    Name = "Angular Frontend"
  }
}

# RDS instance for MySQL database
resource "aws_db_instance" "mysql_db" {
  identifier             = "mysqldbinstance"
  allocated_storage      = 20
  storage_type           = "gp2"
  engine                 = "mysql"
  engine_version         = "8.0.20"
  instance_class         = "db.t2.micro"
  name                   = "mydatabase"
  username               = "dbadmin"
  password               = "securepassword"
  skip_final_snapshot    = true
  vpc_security_group_ids = [aws_security_group.backend_sg.id]

  tags = {
    Name = "My MySQL Database"
  }
}

# Output variables
output "backend_ip" {
  value = aws_instance.backend.public_ip
}

output "frontend_ip" {
  value = aws_instance.frontend.public_ip
}

output "db_endpoint" {
  value = aws_db_instance.mysql_db.address
}
